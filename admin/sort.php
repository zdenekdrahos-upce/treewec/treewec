<?php
header("Content-type: text/plain");
// TODO: refactoring with save.php
require_once("../treewec/admin/init.php");

session_start();
if (Treewec\Admin\Authentication::isAuthenticated()) {
    $_POST['submit'] = 'submit';
    $pageInfo = new \Treewec\PageInfo();
    $pageInfo->currentPath = $_POST['fileUrl'];
    $pageInfo->pageType = \Treewec\FileSystem\FileType::getType($_POST['fileUrl']);
    $fileOrder = new \Treewec\Admin\FileOrderForm('submit', true);
    $fileOrder->setSuccessAction(\Treewec\Admin\FormSuccessAction::PRINT_MESSAGE);
    $fileOrder->setFilePath($pageInfo->getFilePath(TREEWEC_VIEW_DIR));
    $fileOrder->process();

    $errors = $fileOrder->getFormErrors();
    foreach ($errors as $error) {
        echo $error;
        echo $error != end($errors) ? ', ' : ' !!!';
    }
} else {
    echo \Translator::get('file', 'edit', 'save-session-expire');
}
?>