/**
 * Full HTML5 compatibility rule set
 * These rules define which tags and css classes are supported and which tags should be specially treated.
 */
var wysihtml5ParserRules = {
    /**
     * CSS Class white-list
     * Following css classes won't be removed when parsed by the wysihtml5 html parser
     */
    "classes": {
        "left": 1,
        "right": 1,
        "center": 1,
        "main": 1,
        "no-border": 1
    },
    /**
     * Tag list
     *
     * Following options are available:
     *
     *    - add_class:        converts and deletes the given HTML4 attribute (align, clear, ...) via the given method to a css class
     *                        The following methods are implemented in wysihtml5.dom.parse:
     *                          - align_text:  converts align attribute values (right/left/center/justify) to their corresponding css class "wysiwyg-text-align-*")
                                  <p align="center">foo</p> ... becomes ... <p> class="wysiwyg-text-align-center">foo</p>
     *                          - clear_br:    converts clear attribute values left/right/all/both to their corresponding css class "wysiwyg-clear-*"
     *                            <br clear="all"> ... becomes ... <br class="wysiwyg-clear-both">
     *                          - align_img:    converts align attribute values (right/left) on <img> to their corresponding css class "wysiwyg-float-*"
     *                          
     *    - remove:             removes the element and it's content
     *
     *    - rename_tag:         renames the element to the given tag
     *
     *    - set_class:          adds the given class to the element (note: make sure that the class is in the "classes" white list above)
     *
     *    - set_attributes:     sets/overrides the given attributes
     *
     *    - check_attributes:   checks the given HTML attribute via the given method
     *                            - url:      checks whether the given string is an url, deletes the attribute if not
     *                            - alt:      strips unwanted characters. if the attribute is not set, then it gets set (to ensure valid and compatible HTML)
     *                            - numbers:  ensures that the attribute only contains numeric characters
     */
    "tags": {
        "tr": {},
        "strike": {},
        "form": {},
        "rt": {},
        "code": {},
        "acronym": {},
        "br": {},
        "details": {},
        "h4": {},
        "em": {},
        "title": {},
        "multicol": {},
        "figure": {},
        "xmp": {},
        "small": {},
        "area": {},
        "time": {},
        "dir": {},
        "bdi": {},
        "command": {},
        "ul": {},
        "progress": {},
        "dfn": {},
        "iframe": {
            "check_attributes": {
                "src":"url",
                "width":"numbers",
                "height":"numbers"                
            }
        },
        "figcaption": {},
        "a": {
            "check_attributes": {
                "href": "url",
                "target": "alt"
            },
            "set_attributes": {
                //"rel": "nofollow",
                //"target": "_blank"
            }
        },
        "img": {
            "check_attributes": {
                "width": "numbers",
                "alt": "alt",
                "src": "url",
                "height": "numbers"
            }
        },
        "rb": {},
        "footer": {},
        "noframes": {},
        "abbr": {},
        "u": {},
        "bgsound": {},
        "sup": {},
        "address": {},
        "basefont": {},
        "nav": {},
        "h1": {},
        "head": {},
        "tbody": {},
        "dd": {},
        "s": {},
        "li": {},
        "td": {
            "check_attributes": {
                "rowspan": "numbers",
                "colspan": "numbers"
            }
        },
        "object": {},
        "div": {},
        "option": {},
        "select": {},
        "i": {
            "rename_tag": "em"
        },
        "track": {},
        "wbr": {},
        "fieldset": {},
        "big": {},
        "button": {},
        "noscript": {},
        "svg": {},
        "input": {},
        "table": {},
        "keygen": {},
        "h5": {},
        "meta": {},
        "map": {},
        "isindex": {},
        "mark": {},
        "caption": {},
        "tfoot": {},
        "base": {},
        "video": {},
        "strong": {},
        "canvas": {},
        "output": {},
        "marquee": {},
        "b": {
            "rename_tag": "strong"
        },
        "q": {
            "check_attributes": {
                "cite": "url"
            }
        },
        "applet": {},
        "span": {},
        "rp": {},
        "spacer": {},
        "source": {},
        "aside": {},
        "frame": {},
        "section": {},
        "body": {},
        "ol": {},
        "nobr": {},
        "html": {},
        "summary": {},
        "var": {},
        "del": {},
        "blockquote": {
            "check_attributes": {
                "cite": "url"
            }
        },
        "style": {},
        "device": {},
        "meter": {},
        "h3": {},
        "textarea": {},
        "embed": {},
        "hgroup": {},
        "font": {},
        "tt": {},
        "noembed": {},
        "thead": {},
        "blink": {},
        "plaintext": {},
        "xml": {},
        "h6": {},
        "param": {},
        "th": {
            "check_attributes": {
                "rowspan": "numbers",
                "colspan": "numbers"
            }
        },
        "legend": {},
        "hr": {},
        "label": {},
        "dl": {},
        "kbd": {},
        "listing": {},
        "dt": {},
        "nextid": {},
        "pre": {},
        "center": {},
        "audio": {},
        "datalist": {},
        "samp": {},
        "col": {},
        "article": {},
        "cite": {},
        "link": {},
        "script": {},
        "bdo": {},
        "menu": {},
        "colgroup": {},
        "ruby": {},
        "h2": {},
        "ins": {},
        "p": {},
        "sub": {},
        "comment": {},
        "frameset": {},
        "optgroup": {},
        "header": {}
    }
};