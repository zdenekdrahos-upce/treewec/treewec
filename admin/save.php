<?php
header("Content-type: text/plain");
require_once("../treewec/admin/init.php");

session_start();
if (Treewec\Admin\Authentication::isAuthenticated()) {
    $_POST['submit'] = 'submit';
    $pageInfo = new Treewec\PageInfo();
    $pageInfo->currentPath = $_POST['fileUrl'];
    $pageInfo->pageType = Treewec\FileSystem\FileType::getType($_POST['fileUrl']);
    $fileEdit = new \Treewec\Admin\FileEditForm('submit', false);
    $fileEdit->setSuccessAction(Treewec\Admin\FormSuccessAction::PRINT_MESSAGE);
    $fileEdit->setFilePath($pageInfo->getFilePath(TREEWEC_VIEW_DIR));
    $fileEdit->process();
    
    $errors = $fileEdit->getFormErrors();
    foreach ($errors as $error) {
        echo $error;
        echo $error != end($errors) ? ', ' : ' !!!';
    }
} else {
    echo \Translator::get('file', 'edit', 'save-session-expire');
}
?>