<?php
require_once("../treewec/admin/init.php");
$array = \Treewec\Utils\Functions::getUrlArray();
$arrayHolder = new Treewec\Holders\ArrayHolder($array);
$controller = new Treewec\Admin\AdminAreaController($arrayHolder);
$controller->loadView();
$controller->displayView();
?>