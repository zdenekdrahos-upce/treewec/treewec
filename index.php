<?php
require_once("./treewec/init.php");
$array = \Treewec\Utils\Functions::getUrlArray();
$arrayHolder = new Treewec\Holders\ArrayHolder($array);
$controller = new Treewec\PublicAreaController($arrayHolder);
$controller->loadView();
$controller->displayView();
?>