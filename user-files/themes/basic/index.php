<?php PageInfoUpdater::updateHeaders($pageInfo); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <base href="<?php echo TREEWEC_WEB_ROOT; ?>" />
        <title>TREEWEC | <?php echo \Treewec\Utils\Arrays::convertArrayToString($pageInfo->pathInfo, ' &raquo; '); ?></title>
        <meta name="robots" content="index, follow">
        <link href="public/themes/basic/screen.css" type="text/css" rel="stylesheet" media="screen" />
        <script src="public/themes/basic/jquery.js" type="text/javascript"></script>        
    </head>
    <body>     

        <?php 
        PageInfoPrinter::printNeighbourNavigation($pageInfo, false, true);  
        echo '<br />';
        PageInfoPrinter::printNeighbourNavigation($pageInfo, true, true);
        ?>
        <hr />
        <h1 style="margin-bottom: 0"><?php echo $pageInfo->header; ?></h1>        
        <small>
            <?php PageInfoPrinter::printBreadcrumbNavigation($pageInfo, ' &raquo; ', true); ?>
            | <?php echo $pageInfo->pageType; ?>
            <br />
            Permanent links:
            <ul>
                <li>Header as text: <?php echo $pageInfo->header->getLink(); ?></li>
                <li>Your text: <?php echo $pageInfo->header->getLink('my link'); ?></li>
            </ul>
        </small>
        <hr />

        <?php \Treewec\ViewDisplayer::display($view); ?>        
        <hr />

        <?php if ($pageInfo->pageType == Treewec\FileSystem\FileType::FOLDER && $pageInfo->header != TREEWEC_DEFAULT_ERROR): ?>
        <a href="javascript:toggleOneDiv('tree');">Content/obsah</a><br />
        <div class="toogle" title="tree">
        <?php
        $printer = Treewec\PrintoutLoader::getHTMLListPrinter(true, false, true);
        $pl = Treewec\PrintoutLoader::get($printer, TREEWEC_VIEW_DIR . Treewec\FileSystem\Path::toFileSystem($pageInfo->currentPath), 100, false, false, \Treewec\DirectoryIterators\SearchType::DEPTH_FIRST);
        $pl->display();
        ?>
        </div>
        <hr />
        <?php endif; ?>

        Admin area: <a href="./admin/<?php echo $pageInfo->currentPath; ?>">EDIT PAGE</a>, <a href="./admin/&admin=home">ADMIN HOME</a>
    </body>
</html>