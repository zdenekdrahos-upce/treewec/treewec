<?php
/*
 * $exception 
 * - instanceof \Treewec\Exceptions\ClientRequestException
 * - code: 400 (invalid URL address), 404 (file not found)
 */
$errorCode = in_array($exception->getCode(), array(400, 403, 404), true) ? $exception->getCode() : 'default';
$errors = array(    
    'default' => array(
        'name' => 'Unknown Error',
        'what' => 'Nobody knows. Report problem with detailed description what happened'
    ),
    '400' => array(
        'name' => 'Invalid URL Address',        
        'what' => 'Adresa contains illegal characters or invalid values in parameters. Illegal characters are: ?, ", <, >, :, *, |, ^, ;, \, ../, .. and space',
    ),
    '403' => array(
        'name' => 'Access Forbidden',
        'what' => 'The requested page cannot be accessed from public area.'
    ),
    '404' => array(
        'name' => 'Page Was Not Found',
        'what' => 'The requested page was not found. The page either does not exist, or if it is a directory listing then the file associated with the contents of the folder was not found. Return to <a href="./">main page<a> or choose another page in the sitemap'
    ),
);
?>

        <h2><?php echo $errors[$errorCode]['name']; ?></h2>
        <ul class="language-chooser">                
            <li><a href="javascript:toggleOneDiv('what');">What's the problem?</a></li>
            <li><a href="javascript:toggleOneDiv('map');">Map of the web</a></li>
            <li><a href="javascript:toggleOneDiv('report');">Report problem</a></li>
        </ul>

        <section>
            <div class="toogle" title="what">
                <h3>What's the problem?</h3>
                <?php echo $errors[$errorCode]['what']; ?>
            </div>
            <div class="toogle" title="map">
                <h3>Map of the web</h3>
                <?php
                $printer = Treewec\PrintoutLoader::getHTMLListPrinter(true, true, true);
                $pl = Treewec\PrintoutLoader::get($printer, TREEWEC_VIEW_DIR, 3, false);
                $pl->display();   
                ?>
            </div>
            <div class="toogle" title="report">
                <h3>Report problem</h3>
                <ul>
                    <li>If you are visitor - contact administrator of the website</li>
                    <li>If you are administrator of the application and the page should work - <a href="https://bitbucket.org/zdenekdrahos/treewec/issues/new" target="_blank">send issue to Bitbucket</a></li>
                </ul>
            </div>
        </section>
        