<?php

final class PageInfoUpdater {

    public static function updateHeaders(&$pageInfo) {
        if ($pageInfo instanceof \Treewec\PageInfo) {
            self::updatePage($pageInfo->header);
            self::updatePage($pageInfo->homePage);
            self::updatePage($pageInfo->previousPage);
            self::updatePage($pageInfo->nextPage);
            self::updatePage($pageInfo->parentFolder);
            foreach ($pageInfo->pathInfo as $page) {
                self::updatePage($page);
            }
        }
    }

    private static function updatePage(&$page) {
        if ($page instanceof \Treewec\OnePage && !in_array($page->header, array(TREEWEC_MAINPAGE, TREEWEC_DEFAULT_ERROR), true)) {
            $page->header = self::getModifiedHeader($page->header);
        }
    }

    private static function getModifiedHeader($header) {
        return \Treewec\Utils\Strings::uppercaseFirstLetterAndReplaceDashes($header, ' ');
    }

}

?>