<?php

final class PageInfoPrinter {

    public static function printBreadcrumbNavigation($pageInfo, $separator = ' ', $printLinks = true) {
        if ($pageInfo instanceof \Treewec\PageInfo) {
            self::printPage($pageInfo->homePage, $printLinks);
            foreach ($pageInfo->pathInfo as $page) {
                echo $separator;
                self::printPage($page, $printLinks);
            }
        }
    }

    public static function printNeighbourNavigation($pageInfo, $printDefaultNames = false, $printLinks = true) {
        if ($pageInfo instanceof \Treewec\PageInfo) {
            self::printPage($pageInfo->previousPage, $printLinks, $printDefaultNames ? TREEWEC_NAV_PREVIOUS : '');
            echo ' | ';
            self::printPage($pageInfo->parentFolder, $printLinks, $printDefaultNames ? TREEWEC_NAV_PARENT : '');
            echo ' | ';
            self::printPage($pageInfo->homePage, $printLinks, $printDefaultNames ? TREEWEC_NAV_CONTENT : '');
            echo ' | ';
            self::printPage($pageInfo->nextPage, $printLinks, $printDefaultNames ? TREEWEC_NAV_NEXT : '');
        }
    }

    private static function printPage($page, $printLinks, $text = '') {
        if ($page instanceof \Treewec\OnePage) {
            if ($printLinks) {
                echo $page->getLink($text);
            } else {
                echo $page->header;
            }
        } else {
            echo '-';
        }
    }

}

?>