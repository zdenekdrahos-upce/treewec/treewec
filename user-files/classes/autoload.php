<?php
/*
 * Autoload function for your classes
 */

function treewecAutoloadUser($className) {
    $directories = array('', 'test', 'pageinfo');
    foreach ($directories as $directory) {      
        $path = TREEWEC_USER_FILES . "classes/{$directory}/{$className}.php";
        if (file_exists($path)) {
            require_once($path);
            return;
        }
    }
}

?>