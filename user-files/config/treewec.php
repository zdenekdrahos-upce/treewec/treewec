<?php
# Treewec settings
define('TREEWEC_URL_REWRITE', true);
define('TREEWEC_WEB_ROOT', 'http://localhost/_my_classes/TreeWeC/');
define('TREEWEC_DEFAULT_THEME', 'basic');
define('TREEWEC_DEFAULT_ADMIN_EDITOR', 'ckeditor');
define('TREEWEC_ADMIN_AUTHENTICATION', '\Treewec\Admin\Authentication\FreeAccess');
define('TREEWEC_DEFAULT_LANGUAGE', 'english');
# Default settings
define('TREEWEC_DEFAULT_COMPARATOR', Treewec\DirectoryIterators\ComparatorType::USER);
define('TREEWEC_DEFAULT_ORDER', Treewec\DirectoryIterators\OrderType::ASCENDING);
define('TREEWEC_DEFAULT_DEPTH_IN_PAGES', 2);
# Files
define('TREEWEC_PREFIX_OF_HIDDEN_FILES', '.');
define('TREEWEC_USER_HIDDEN_FILES', 'Morašice.php');
define('TREEWEC_DEFAULT_FILE_EXTENSION', '.php');
define('TREEWEC_NAME_OF_INDEX_FILE', 'index');
?>