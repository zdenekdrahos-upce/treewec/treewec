<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryPrinter;

class HTMLListPrinter extends Printer {

    /** @var \Treewec\HTML\Listing */
    protected $listing;

    public function __construct() {
        parent::__construct();
        $this->listing = new \Treewec\HTML\Listing(\Treewec\HTML\Listing::BULLET);
    }

    public function setHtmlList($listType) {
        $this->listing = new \Treewec\HTML\Listing($listType);
    }

    public function process() {
        $this->listing->printListHeader();
        parent::process();
        $this->listing->printListFooter();
    }

    protected function processElement($iteratorElement) {
        $this->listing->printItemHeader();
        echo $this->printFileInfo($iteratorElement);
        $this->listing->printItemFooter();
    }

}

?>