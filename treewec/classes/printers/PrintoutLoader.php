<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class PrintoutLoader {

    private $pathToRoot;
    private $searchType;
    /** @var Treewec\DirectoryIterators\IteratorFilters */
    private $filter;
    /** @var Treewec\DirectoryIterators\IAbort */
    private $abortCondition;
    /** @var Treewec\DirectoryIterators\IComparator */
    private $comparator;
    /** @var Treewec\DirectoryIterators\IteratorProcessor */
    private $processor;

    /**
     * @param Treewec\DirectoryIterators\IteratorProcessor $printer or null
     * @param string $rootDir
     * @param int $maxDepth
     * @param boolean $displayHidden 
     * @param string $fileType - constant from Treewec\FileSystem\FileType
     * @param string $searchType - constant from Treewec\DirectoryIterators\SearchType
     * @return \Treewec\PrintoutLoader
     */
    public static function get($printer, $rootDir, $maxDepth, $displayHidden, $fileType = false, $searchType = DirectoryIterators\SearchType::DEPTH_FIRST) {
        $loader = new self();
        $loader->setDirectoryPath($rootDir);
        $loader->setSearchType($searchType);
        $loader->setMaxDepth($maxDepth);
        $loader->setDisplayHiddenFiles($displayHidden);
        if ($fileType) {
            $fileType = constant('\Treewec\FileSystem\FileType::' . strtoupper($fileType));
            $loader->setFileTypeFilter($fileType);
        }
        $loader->setPrinter($printer);
        return $loader;
    }

    /**
     * @param boolean $printLinks
     * @param boolean $processRoot
     * @param boolean $hierarchy
     * @param boolean $loadListFromURL
     * @return \DirectoryPrinter\HTMLListPrinter 
     */
    public static function getHTMLListPrinter($printLinks, $processRoot, $hierarchy = true) {
        if ($hierarchy) {
            $listPrinter = new DirectoryPrinter\HTMLHierarchyListPrinter();
        } else {
            $listPrinter = new DirectoryPrinter\HTMLListPrinter();
        }
        $listPrinter->setPrintLinks($printLinks);
        $listPrinter->setProcessRoot($processRoot);
        return $listPrinter;
    }

    private function __construct() {
        $this->pathToRoot = TREEWEC_VIEW_DIR;
        $this->searchType = DirectoryIterators\SearchType::DEPTH_FIRST;        
        $this->processor = new DirectoryPrinter\HTMLHierarchyListPrinter();
        $this->comparator = DirectoryIterators\ComparatorFactory::get();
        $this->filter = DirectoryIterators\IteratorFilters::getInstanceWithStandardFilters();
    }

    public function setDirectoryPath($path) {
        $this->pathToRoot = $path ? $path : null;
    }

    public function setSearchType($searchType) {
        if (\Treewec\Utils\Instances::isClassConstant('\Treewec\DirectoryIterators\SearchType', $searchType)) {
            $this->searchType = $searchType;
        }
    }

    public function setMaxDepth($depth) {
        $this->abortCondition = new DirectoryIterators\MaxDepthAbortion();
        $this->abortCondition->setMaxDepth($depth);
    }

    public function setDisplayHiddenFiles($displayHidden) {
        $filter = new DirectoryIterators\FilterHiddenFiles();
        $filter->setDisplayHiddenItems($displayHidden);
        $this->filter->addFilter($filter);
    }

    public function setFileTypeFilter($fileType) {
        $fileTypeFilter = new DirectoryIterators\FilterFileType();
        $fileTypeFilter->setFileTypeFilter($fileType);
        $this->filter->addFilter($fileTypeFilter);
    }

    public function addNewFilter($filter) {
        $this->filter->addFilter($filter);
    }

    public function resetFilterAndAddNewFilter($filter) {
        $this->filter = new DirectoryIterators\IteratorFilters();        
        $this->filter->addFilter($filter);
    }

    public function setComparator($comparator) {
        $this->comparator = $comparator;
    }

    public function setPrinter($printer, $processRoot = null) {
        if (is_object($printer) && is_subclass_of($printer, '\Treewec\DirectoryIterators\IteratorProcessor')) {
            $this->processor = $printer;
            if (!is_null($processRoot)) {
                $this->processor->setProcessRoot($processRoot);
            }
        }
    }

    public function display() {
        $iterator = $this->getIterator();
        $iterator->setFilter($this->filter);
        $iterator->setComparator($this->comparator);
        if ($this->abortCondition) {
            $iterator->setAbortCondition($this->abortCondition);
        }
        $this->processor->setDirectoryIterator($iterator);
        $this->processor->process();
    }

    private function getIterator() {
        $classes = array(
            DirectoryIterators\SearchType::BASIC => 'DirectoryIterator',
            DirectoryIterators\SearchType::DEPTH_FIRST => 'DepthFirstSearch',
            DirectoryIterators\SearchType::BREADTH_FIRST => 'BreadthFirstSearch',
        );
        $className = '\Treewec\DirectoryIterators\\' . $classes[$this->searchType];
        return new $className($this->pathToRoot);
    }

}

?>