<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryPrinter;

abstract class Printer extends \Treewec\DirectoryIterators\IteratorProcessor {

    private $printLinks;
    /** @var \Treewec\URLBuilder */
    protected $urlBuilder;
    /** @var \Treewec\DirectoryPrinter\OutputFileHelper */
    protected $outputter;

    public function __construct() {
        parent::__construct();
        $this->printLinks = true;
        $this->urlBuilder = \Treewec\URLBuilder::getEmptyURLBuilder();
        $this->setRootDirectory(TREEWEC_VIEW_DIR);
    }

    public function setPrintLinks($printLinks) {
        $this->printLinks = is_bool($printLinks) ? $printLinks : $this->printLinks;
    }

    public final function setRootDirectory($rootDirectory, $rootName = TREEWEC_MAINPAGE) {
        $this->outputter = new OutputFileHelper($rootDirectory, $rootName);
    }

    protected function printFileInfo($iteratorElement) {        
        $splFileInfo = $iteratorElement->splFileInfo;
        $filename = $this->getFilename($splFileInfo);
        if ($this->printLinks) {            
            return \Treewec\HTML\Anchor::create($this->getURLLink($splFileInfo), $filename);
        } else {
            return $filename;
        }
    }

    protected function getFilename($splFileInfo) {
        return $this->outputter->getFilename($splFileInfo, ' ');
    }

    protected function getURLLink($splFileInfo) {
        $this->urlBuilder->addOrModifyParameter('path', $this->outputter->getBaseLinkForURL($splFileInfo));
        return $this->urlBuilder->build();
    }

}

?>