<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryPrinter;

// TODO: refactoring
class HTMLHierarchyListPrinter extends HTMLListPrinter {

    /** @var Treewec\DirectoryIterators\IteratorElement */
    private $firstElement;
    /** @var Treewec\DirectoryIterators\IteratorElement */
    private $secondElement;
    /** @var Treewec\DirectoryIterators\IteratorElement */
    private $thirdElement;

    public function __construct() {
        parent::__construct();
        $this->setPrintLinks(true);
    }

    public function process() {
        $this->listing->printListHeader();
        Printer::process();        

        if (!is_null($this->thirdElement)) {
            $this->listing->printItemHeader();
            echo $this->printFileInfo($this->thirdElement);        
            for ($i = 0; $i < $this->thirdElement->depth; $i++) {
                $this->listing->printItemFooter();
                $this->listing->printListFooter();
            }
        } else {
            $this->listing->printListFooter();
        }
    }

    protected function processElement($iteratorElement) {
        $this->firstElement = $this->secondElement;
        $this->secondElement = $this->thirdElement;
        $this->thirdElement = $iteratorElement;
        if (!is_null($this->secondElement)) {
            $this->printHierarchy();
        }
    }

    private function printHierarchy() {
        $this->listing->printItemHeader();
        echo $this->printFileInfo($this->secondElement);

        if ($this->secondElement->depth == $this->thirdElement->depth) {
            $this->listing->printItemFooter();
        } else if ($this->secondElement->depth > $this->thirdElement->depth) {
            $this->listing->printItemFooter();
            for ($i = 0; $i < $this->secondElement->depth - $this->thirdElement->depth; $i++) {
                $this->listing->printListFooter();
            }
        } else if ($this->secondElement->depth < $this->thirdElement->depth) {
            $this->listing->printListHeader();
        }
    }

}

?>