<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class URLBuilder {

    private $array;

    public function __construct($arrayHolder) {
        if (Utils\Instances::isInstanceOf($arrayHolder, '\Treewec\Holders\ArrayHolder')) {
            $this->array = $arrayHolder->get();
        }
    }

    /** @return \Treewec\URLBuilder  */
    public static function getEmptyURLBuilder() {
        $arrayHolder = new Holders\ArrayHolder(array());
        return new self($arrayHolder);
    }

    public function addOrModifyParameter($key, $value) {
        $this->array[$key] = $value;
    }

    public function removeParameter($parameterName) {
        if (array_key_exists($parameterName, $this->array)) {
            unset($this->array[$parameterName]);
        }
    }

    public function build() {
        $this->removeInvalidItemsFromArray();
        if (TREEWEC_URL_REWRITE === true) {
            return $this->buildPrettyLink();
        } else {
            return $this->buildStandardLink();
        }
    }

    private function removeInvalidItemsFromArray() {
        foreach ($this->array as $parameter => $value) {
            if (!is_string($parameter) || !is_scalar($value)) {
                unset($this->array[$parameter]);
            }
        }
    }

    private function buildStandardLink() {
        return $_SERVER['PHP_SELF'] . $this->buildQueryString($this->array, '?');
    }

    private function buildPrettyLink() {
        $webAddress = $this->getAddress();
        $path = $this->getPath();
        $queryString = $this->buildQueryString($this->getArrayWithoutPath(), '&');
        return $webAddress . $path . $queryString;
    }

    private function getAddress() {
        return str_replace('index.php', '', $_SERVER['PHP_SELF']);
    }

    private function getPath() {
        return isset($this->array['path']) ? $this->array['path'] : '';
    }

    private function getArrayWithoutPath() {
        $array = $this->array;
        unset($array['path']);
        return $array;
    }

    private function buildQueryString($array, $startLetter) {
        $queryString = implode('&', Utils\Arrays::convertArrayToStringPairs($array, '='));
        $queryStringWithStartLetter = (empty($queryString) ? '' : $startLetter) . $queryString;
        return $queryStringWithStartLetter;
    }

}

?>