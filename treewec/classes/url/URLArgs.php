<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class URLArgs {

    // GENERAL VARIABLES (used in PUBLIC AREA)
    public $path;
    // Extra variables used in ADMIN AREA
    public $admin;
    public $action;
    public $editor;

    public static function createFromArrayHolder($arrayHolder) {
        if (Utils\Instances::isInstanceOf($arrayHolder, '\Treewec\Holders\ArrayHolder')) {
            return new self($arrayHolder->get());
        }
    }

    private function __construct($array) {
        foreach (array_keys(get_object_vars($this)) as $key) {
            $this->$key = array_key_exists($key, $array) ? $array[$key] : null;
        }
    }
}

?>