<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

class URLArgsInspector {

    /** @var Treewec\URLArgs */
    protected $urlArguments;
    /** @var Treewec\Inspector */
    protected $inspector;

    public function __construct() {
        $this->inspector = new Inspector();
    }

    /**
     * @param Treewec\URLArgs $urlArgs 
     * @throws \Exception - if $urlArgs is invalid argument
     */
    public function addInspectedObject($urlArgs) {
        $this->urlArguments = $urlArgs;
        $this->checkInstance();
    }

    public function check() {
        if (!is_null($this->urlArguments->path)) {
            $this->checkPath();
        }
        $this->checkConditions();
    }

    protected function checkConditions() {
        try {
            $this->inspector->checkNecessaryConditions();
        } catch (\Exception $e) {
            Exceptions\ClientRequestException::throwsBadRequest();
        }
    }

    protected function checkOptionalConstant($constant, $class, $configConstant) {
        if (!is_null($constant)) {
            $this->inspector->addCondition($configConstant === true);
            $this->inspector->addCondition(Utils\Instances::isClassConstant($class, strtoupper($constant)));
        }
    }

    private function checkInstance() {
        $this->inspector->addCondition($this->urlArguments instanceof URLArgs);
        $this->inspector->checkNecessaryConditions();
    }

    protected function checkPath() {
        $this->checkValidFirstLetter();
        $this->checkValidPathName();
        $this->checkEmptyValuesInPath();
        $this->checkUserIndexFile();
        $this->checkValidRewritePath();
    }

    private function checkValidFirstLetter() {
        $this->inspector->addCondition(substr($this->urlArguments->path, 0, 1) != '/');
    }

    private function checkValidPathName() {
        $this->inspector->addCondition(FileSystem\Path::isValid($this->urlArguments->path));
    }

    private function checkEmptyValuesInPath() {
        $pathArray = Utils\Arrays::convertStringToArray($this->urlArguments->path, '/');
        $this->inspector->addCondition(Utils\Arrays::getNumberOfEmptyValues($pathArray) <= 1);
    }

    private function checkUserIndexFile() {
        $indexLength = mb_strlen(TREEWEC_NAME_OF_INDEX_FILE, 'UTF-8');
        $lastWord = mb_substr($this->urlArguments->path, -$indexLength, $indexLength, 'UTF-8');
        $this->inspector->addCondition(TREEWEC_NAME_OF_INDEX_FILE != $lastWord);
    }

    private function checkValidRewritePath() {
        if (TREEWEC_URL_REWRITE && isset($_SERVER['REQUEST_URI'])) {
            $this->inspector->addCondition(is_bool(strpos($_SERVER['REQUEST_URI'], 'index.php')));
        }
    }

}

?>