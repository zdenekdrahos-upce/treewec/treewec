<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class AdminURLInspector extends URLArgsInspector {

    public function check() {        
        $this->checkAdmin();
        $this->checkAction();
        $this->checkEditor();
        $this->checkExtraArguments();
        parent::check();
    }

    private function checkAdmin() {
        $validValues = array(null, 'audit', 'help', 'home', 'pages', 'plugins', 'settings', 'themes', 'login', 'logout');
        $this->inspector->addCondition(in_array($this->urlArguments->admin, $validValues, true));
        if ($this->urlArguments->path && !in_array($this->urlArguments->admin, array('themes', 'plugins', 'pages'), true)) {
            $this->inspector->addCondition($this->urlArguments->admin == '');
        }
    }

    private function checkAction() {
        if (!is_null($this->urlArguments->action)) {
            $validValues = array('rename', 'delete', 'hidden', 'move', 'copy', 'add', 'order', 'info');
            $this->inspector->addCondition(is_null($this->urlArguments->admin) && in_array($this->urlArguments->action, $validValues, true));
            $this->inspector->addCondition(is_null($this->urlArguments->editor));
        }
    }

    private function checkEditor() {
        if (!is_null($this->urlArguments->editor)) {
            $editors = Admin\Editors\AvailableEditors::getAvailableEditors();
            $this->inspector->addCondition(is_null($this->urlArguments->admin) && in_array($this->urlArguments->editor, $editors, true));
            $this->inspector->addCondition(is_null($this->urlArguments->action));
        }
    }

    private function checkExtraArguments() {
        $validArgs = array($this->urlArguments->path, $this->urlArguments->admin, $this->urlArguments->action, $this->urlArguments->editor);
        $argsCount = count(array_filter($validArgs, 'strlen'));
        $getCount = $this->urlArguments->path ? count($_GET) : (count($_GET) - 1);
        $this->inspector->addCondition($argsCount == $getCount);
    }

}

?>