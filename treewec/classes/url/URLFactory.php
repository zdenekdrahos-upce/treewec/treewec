<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class URLFactory {

    /** @var \Treewec\URLBuilder */
    private static $urlBuilder;

    public static function initUrlBuilder() {
        self::$urlBuilder = URLBuilder::getEmptyURLBuilder();
    }

    public static function getLink($path) {
        if (is_string($path) && $path !== '') {
            self::$urlBuilder->addOrModifyParameter('path', $path);
        } else {
            self::$urlBuilder->removeParameter('path');
        }
        return self::$urlBuilder->build();
    }

}

URLFactory::initUrlBuilder();
?>