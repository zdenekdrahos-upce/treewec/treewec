<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class Inspector {

    private $necessaryConditions;

    public function __construct() {
        $this->necessaryConditions = array();
    }

    /** @throws \Exception */
    public function checkNecessaryConditions() {
        if ($this->isInvalid()) {
            throw new \Exception();
        }
    }

    public function addCondition($condition) {
        $this->necessaryConditions[] = $condition;
    }

    private function isInvalid() {
        return in_array(false, $this->necessaryConditions, true);
    }

}

?>