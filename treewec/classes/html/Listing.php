<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\HTML;

final class Listing {

    const BULLET = 'BULLET';
    const NUMBER = 'NUMBER';

    private $htmlTag;

    public function __construct($listType = self::BULLET) {
        $this->htmlTag = $this->getHtmlTag($listType);
    }

    public function printListHeader() {
        echo "<{$this->htmlTag}>";
    }

    public function printListFooter() {
        echo "</{$this->htmlTag}>";
    }

    public function printItemHeader() {
        echo '<li>';
    }

    public function printItemFooter() {
        echo '</li>';
    }

    private function getHtmlTag($listType) {
        $tag = 'ul';
        if (\Treewec\Utils\Instances::isClassConstant('\Treewec\HTML\Listing', $listType)) {
            switch ($listType) {
                case self::BULLET:
                    $tag = 'ul';
                    break;
                case self::NUMBER:
                    $tag = 'ol';
                    break;
            }
        }
        return $tag;
    }

}

?>