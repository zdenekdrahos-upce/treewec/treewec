<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\HTML;

final class Attribute {

    private $name;
    private $value;

    public function __construct($name, $value) {
        $this->name = is_string($name) ? $name : '';
        $this->value = is_string($value) ? $value : '';
    }

    public function isValid() {
        return $this->name !== '' && $this->value !== '';
    }

    public function __toString() {
        if ($this->isValid()) {
            return "{$this->name}=\"{$this->value}\"";
        }
        return '';
    }

}

?>