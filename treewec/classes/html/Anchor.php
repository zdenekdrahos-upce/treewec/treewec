<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\HTML;

final class Anchor {

    public static function create($url, $textOfTheLink, $attributes = array()) {
        if (is_string($url) && is_string($textOfTheLink)) {
            $attributes = AttributeFactory::transformAttributesToString($attributes);
            return "<a href=\"{$url}\"{$attributes}>{$textOfTheLink}</a>";
        }
        return '';
    }

}

?>