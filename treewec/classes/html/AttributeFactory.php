<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\HTML;

final class AttributeFactory {

    public static function getAttributeClass($value, $condition = true) {
        if ($condition === true) {
            return new Attribute('class', $value);
        }
        return false;
    }

    public static function getAttributeTitle($value, $condition = true) {
        if ($condition === true) {
            return new Attribute('title', $value);
        }
        return false;
    }

    public static function transformAttributesToString($attributes) {
        $resultText = '';
        if (is_array($attributes)) {
            foreach ($attributes as $attribute) {
                if ($attribute instanceof Attribute && $attribute->isValid()) {
                    $resultText .= " {$attribute->__toString()}";
                }
            }
        }
        return $resultText;
    }

}

?>