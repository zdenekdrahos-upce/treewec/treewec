<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\FileSystem;

final class FileFactory {

    public static function build($path, $rootDirectory = TREEWEC_VIEW_DIR) {
        try {
            $path = $rootDirectory . Path::toFileSystem($path);
            $fileType = FileType::getType($path);
            if ($fileType == FileType::FOLDER) {
                return new Directory($path);
            } else {
                return new File($path . TREEWEC_DEFAULT_FILE_EXTENSION);
            }
        } catch (\Treewec\Exceptions\FileSystemException $e) {
            \Treewec\Exceptions\ClientRequestException::throwsFileNotFound();
        }
    }

}

?>