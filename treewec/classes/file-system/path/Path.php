<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\FileSystem;

final class Path {

    private static $invalidCharacters = array(
        '?', '"', '<', '>', ':', '*', '|',
        '^', ';', ' ', '\\', '../', '..'
    );

    public static function isValid($path) {
        if (is_string($path)) {
            $test = str_replace(self::$invalidCharacters, '', $path);
            return strlen($path) == strlen($test);
        }
        return false;
    }

    public static function isHidden($path) {
        if (is_string($path)) {
            $prefixOnStart = Filename::isHidden($path);
            $prefixInFolder = is_int(strpos($path, '/' . TREEWEC_PREFIX_OF_HIDDEN_FILES));
            if ($prefixOnStart || $prefixInFolder) {
                return true;
            } else {
                return Filename::isHidden(self::getFullFilename($path));
            }
        }
        return false;
    }

    public static function getFullFilename($path) {
        return self::getFilenameFromPath($path) . self::getFilenameExtension($path);
    }

    public static function getFilenameFromPath($path) {
        if (is_string($path)) {
            $explodedPath = explode('/', $path);
            $files = array_filter($explodedPath, 'strlen');
            if ($files) {
                return array_pop($files);
            }
        }
        return '';
    }

    public static function getFilenameExtension($path) {
        if (is_string($path)) {
            return FileType::getType($path) == FileType::FOLDER ? '' : TREEWEC_DEFAULT_FILE_EXTENSION;
        }
        return '';
    }

    public static function toScreen($path) {
        if (is_string($path)) {
            return self::convert($path) ? iconv('cp1252', 'UTF-8', $path) : $path;
        }
        return '';
    }

    public static function toFileSystem($path) {
        if (is_string($path)) {
            return self::convert($path) ? iconv('UTF-8', 'cp1252//IGNORE', $path) : $path;
        }
        return '';
    }

    private static function convert($path) {
        return is_int(strpos(PHP_OS, 'WIN')) && mb_detect_encoding($path) != 'ASCII';
    }
    
}

?>