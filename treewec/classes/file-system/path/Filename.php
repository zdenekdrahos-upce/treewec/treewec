<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\FileSystem;

final class Filename {

    public static function isValid($filename) {
        if (Path::isValid($filename)) {
            return is_bool(strpos($filename, '/'));
        }
        return false;
    }

    public static function isHidden($filename) {
        if (is_string($filename)) {
            $hiddenPrefix = substr($filename, 0, strlen(TREEWEC_PREFIX_OF_HIDDEN_FILES)) == TREEWEC_PREFIX_OF_HIDDEN_FILES;
            $hiddenFilename = HiddenFilenames::isHiddenFilename($filename);
            return $hiddenPrefix || $hiddenFilename;
        }
        return false;
    }

}

?>