<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\FileSystem;

final class HiddenFilenames {
    
    private static $defaultHiddenFilenames;
    private static $userHiddenFilenames;

    public static function isHiddenFilename($filename) {
        return self::isDefaultHiddenFilename($filename) || self::isUserHiddenFilename($filename);
    }

    public static function isDefaultHiddenFilename($filename) {
        return self::isHidden($filename, self::$defaultHiddenFilenames);
    }

    public static function isUserHiddenFilename($filename) {
        return self::isHidden($filename, self::$userHiddenFilenames);
    }

    private static function isHidden($filename, $hiddenFilenames) {
        if (is_string($filename)) {
            return in_array($filename, $hiddenFilenames, true);
        }
        return false;
    }

    public static function loadHiddenFilenames() {
        self::loadDefaultHiddenFilenames();
        self::loadUserHiddenFilenames();
    }

    private static function loadDefaultHiddenFilenames() {
        self::$defaultHiddenFilenames = explode(';', TREEWEC_DEFAULT_HIDDEN_FILES);
    }

    private static function loadUserHiddenFilenames() {
        self::$userHiddenFilenames = array();
        if (TREEWEC_USER_HIDDEN_FILES) {
            $filenames =  explode(';', TREEWEC_USER_HIDDEN_FILES);
            foreach ($filenames as $filename) {
                self::$userHiddenFilenames[] = Path::toFileSystem($filename);// if name from splFileInfo
                self::$userHiddenFilenames[] = $filename;// if name from URL path
            }
            self::$userHiddenFilenames = array_unique(self::$userHiddenFilenames);
        }
    }

}

HiddenFilenames::loadHiddenFilenames();
?>