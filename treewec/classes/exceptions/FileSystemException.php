<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Exceptions;

final class FileSystemException extends \Exception {

    public static function throwsNonreadablePath() {
        throw new self('Path not exists or not readable');
    }

    public static function throwsInvalidFile() {
        throw new self('Invalid file');
    }

    public static function throwsInvalidDirectory() {
        throw new self('Invalid directory');
    }

    public function __construct($message) {
        parent::__construct($message);
    }

}

?>