<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Exceptions;

final class ClientRequestException extends \Exception {

    /** @throws Treewec\Exceptions\ClientRequestException */
    public static function throwsFileNotFound() {
        throw new self('File Not Found', 404);
    }

    /**
     * Throws if bad request, i.e.
     * - empty values in address
     * - invalid character(s) in address
     * - invalid key(s) in adress
     * @throws Treewec\Exceptions\ClientRequestException 
     */
    public static function throwsBadRequest() {
        throw new self('Bad Request - invalid URL address', 400);
    }

    /**
     * Throws if hidden file is accessed in public area
     * @throws Treewec\Exceptions\ClientRequestException 
     */
    public static function throwsForbiddenAccess() {
        throw new self('Access Forbidden', 403);
    }

    public function __construct($message, $code) {
        parent::__construct($message, $code);
    }

    public function setHttpResponse() {
        if (!headers_sent()) {
            header($this->message, true, $this->code);
        }
    }

}

?>