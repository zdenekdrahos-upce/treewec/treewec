<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class DepthFirstSearch extends BaseDirectoryIterator {

    protected function getHelperStructure() {
        return new \Treewec\DataStructures\LIFO();
    }

    protected function sortChildren(&$children) {
        parent::sortChildren($children);
        $children = array_reverse($children);
    }

}

?>