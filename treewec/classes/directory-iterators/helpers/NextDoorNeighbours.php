<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class NextDoorNeighbours extends IteratorProcessor {

    /** @var \SplFileInfo */
    private $currentFile;
    /** @var array */
    private $nextDoorNeighbours;
    /** @var boolean */
    private $isSetPrevious;
    /** @var boolean */
    private $areNeighboursLoaded;

    /**
     * @param string $filePath 
     * @throws \Treewec\Exceptions\FileSystemException
     */
    public function __construct($filePath) {        
        $this->nextDoorNeighbours = array('P' => null, 'N' => null);
        $this->isSetPrevious = false;
        $this->areNeighboursLoaded = false;
        $this->currentFile = new \SplFileInfo($filePath);
        $this->loadParentProcessor();
        parent::process();
    }

    public function getNextDoorNeighbours() {
        return $this->nextDoorNeighbours;
    }

    protected function processElement($iteratorElement) {
        if ($iteratorElement->splFileInfo->getRealPath() == $this->currentFile->getRealPath()) {
            $this->isSetPrevious = true;
        } else {
            $basename = \Treewec\Utils\Files::getBasename($iteratorElement->splFileInfo, TREEWEC_DEFAULT_FILE_EXTENSION);
            $basename .= $iteratorElement->splFileInfo->isDir() ? '/' : '';
            if (!$this->isSetPrevious) {
                $this->nextDoorNeighbours['P'] = \Treewec\FileSystem\Path::toScreen($basename);
            } else {
                $this->nextDoorNeighbours['N'] = \Treewec\FileSystem\Path::toScreen($basename);
                $this->areNeighboursLoaded = true;
            }
        }
    }

    protected function continueIterating() {
        return !$this->areNeighboursLoaded && parent::continueIterating();
    }

    private function loadParentProcessor() {
        $directoryIterator = new DirectoryIterator($this->currentFile->getPath());
        $directoryIterator->setFilter(IteratorFilters::getInstanceWithStandardFilters());
        $directoryIterator->setComparator(ComparatorFactory::get());
        parent::setDirectoryIterator($directoryIterator);
        parent::setProcessRoot(false);
    }

}

?>