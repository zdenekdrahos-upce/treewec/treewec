<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class SearchType {

    /** Basic - only files from folder, not from subfolders */
    const BASIC = 'BASIC';
    /** Depth first search */
    const DEPTH_FIRST = 'DEPTH_FIRST';
    /** Breadth first search */
    const BREADTH_FIRST = 'BREADTH_FIRST';

    private function __construct() {}

}

?>