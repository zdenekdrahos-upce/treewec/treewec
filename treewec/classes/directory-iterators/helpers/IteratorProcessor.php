<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

abstract class IteratorProcessor {

    /** @var \Treewec\DirectoryIterator\IIterator */
    private $iterator;
    /** @var boolean */
    private $processRoot;

    public function __construct() {        
        $this->iterator = new DirectoryIterator(TREEWEC_VIEW_DIR);
        $this->processRoot = false;
    }
    
    public function setDirectoryIterator($directoryIterator) {
        \Treewec\Utils\Instances::isInstanceOf($directoryIterator, '\Treewec\DirectoryIterators\IIterator');
        $this->iterator = $directoryIterator;
    }

    public function setProcessRoot($processRoot) {
        $this->processRoot = is_bool($processRoot) ? $processRoot : $this->processRoot;        
    }

    public function process() {
        $this->checkRootProcessing();
        while ($this->continueIterating()) {
            $this->processElement($this->iterator->next());
        }
    }

    private function checkRootProcessing() {
        if (!$this->processRoot && $this->iterator->hasNext()) {
            $this->iterator->next();
        }
    }

    protected function continueIterating() {
        return $this->iterator->hasNext();
    }
    
    /** @param Treewec\DirectoryIterators\IteratorElement $iteratorElement */
    protected abstract function processElement($iteratorElement);
}

?>