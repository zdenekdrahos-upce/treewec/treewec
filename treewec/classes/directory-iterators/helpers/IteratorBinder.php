<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class IteratorBinder implements \Iterator {

    /** @var Treewec\DirectoryIterators\IIterator */
    private $iterator;
    /** @var Treewec\DirectoryIterators\IteratorElement */
    private $currentElement;
    /** @var int */
    private $processedElements;

    public function __construct($directoryIterator) {
        \Treewec\Utils\Instances::isInstanceOf($directoryIterator, '\Treewec\DirectoryIterators\IIterator');
        $this->iterator = $directoryIterator;        
        $this->rewind();
    }

    public function current() {
        return $this->currentElement;
    }

    public function key() {
        return $this->processedElements;
    }

    public function next() {
        if ($this->iterator->hasNext()) {
            $this->currentElement = $this->iterator->next();
            $this->processedElements++;
        } else {
            $this->currentElement = null;
        }
    }

    public function rewind() {
        $this->processedElements = 0;
        $this->iterator->rewind();
        $this->next();
    }

    public function valid() {
        return $this->currentElement ? true : false;
    }

}

/* http://www.php.net/manual/en/class.iterator.php#96691
    1. Before the first iteration of the loop, Iterator::rewind() is called.
    2. Before each iteration of the loop, Iterator::valid() is called.
    3a. It Iterator::valid() returns false, the loop is terminated.
    3b. If Iterator::valid() returns true, Iterator::current() and Iterator::key() are called.
    4. The loop body is evaluated.
    5. After each iteration of the loop, Iterator::next() is called and we repeat from step 2 above.
 */
?>