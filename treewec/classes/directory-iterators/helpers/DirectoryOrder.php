<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class DirectoryOrder {

    private static $orderFilename = 'dir.order';

    private $directoryPath;
    private $orderArray;

    public function __construct() {
        $this->directoryPath = '';
        $this->orderArray = array();
    }

    /** @return array (keys = filenames and values = index from 0 to ...) */
    public function getOrderArray() {
        return $this->orderArray;
    }

    public function changeDirectory($directoryPath) {
        if (is_dir($directoryPath) && $this->directoryPath != ($directoryPath)) {
            $this->directoryPath = $directoryPath . '/';
            $this->updateOrderArray();
            return true;
        }
        return false;
    }

    public function changeOrderArray($filenamesArray) {
        $this->orderArray = array();
        if (is_array($filenamesArray)) {
            $count = 0;
            foreach ($filenamesArray as $filename) {
                if (!\Treewec\Utils\Variables::isEmpty($filename)) {
                    $key = trim($filename);
                    $this->orderArray[$key] = $count++;
                }
            }
        }
    }

    /** @param \SplFileInfo $fileFromDirectory */
    public function addNewAtTop($filename) {
        if (is_string($filename)) {
            $currentFilenames = array_keys($this->orderArray);
            array_unshift($currentFilenames, $filename);
            $this->changeOrderArray($currentFilenames);
        }
    }

    /** @param \SplFileInfo $fileFromDirectory */
    public function addNewAtBottom($filename) {
        if (is_string($filename)) {
            $this->orderArray[$filename] = count($this->orderArray);
        }
    }

    public function saveOrder() {
        if (!file_exists($this->directoryPath . self::$orderFilename)) {
            file_put_contents($this->directoryPath . self::$orderFilename, '');
        }
        $newData = implode("\n", array_keys($this->orderArray));
        return is_int(file_put_contents($this->directoryPath . self::$orderFilename, $newData));
    }

    private function updateOrderArray() {
        $this->orderArray = array();
        $orderFile = $this->directoryPath . self::$orderFilename;
        if (file_exists($orderFile)) {
            $filenames = file($orderFile);
            $this->changeOrderArray($filenames);
        }
    }

}

?>