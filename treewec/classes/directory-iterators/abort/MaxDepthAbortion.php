<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class MaxDepthAbortion implements IAbort {

    private $maxDepth;
    
    public function __construct() {
        $this->maxDepth = 5;
    }

    public function abortChildLoading($iteratorElement) {        
        return $iteratorElement->depth > $this->maxDepth;
    }

    public function setMaxDepth($depth) {
        if ($this->isDepthValidNumber($depth)) {
            $this->maxDepth = $depth;
        }
    }

    private function isDepthValidNumber($depth) {
        return is_int($depth) && $depth >= 0;
    }

}

?>