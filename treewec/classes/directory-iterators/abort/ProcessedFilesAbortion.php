<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class ProcessedFilesAbortion implements IAbort {

    private $processedFiles;
    private $maxProcessedFiles;
    
    public function __construct() {
        $this->processedFiles = 0;
        $this->maxProcessedFiles = 0;
    }

    public function abortChildLoading($iteratorElement) {        
        return ++$this->processedFiles > $this->maxProcessedFiles;
    }

    public function setMax($max) {
        if ($this->isMaxValidNumber($max)) {
            $this->maxProcessedFiles = $max;
        }
    }

    private function isMaxValidNumber($max) {
        return is_int($max) && $max >= 0;
    }

}

?>