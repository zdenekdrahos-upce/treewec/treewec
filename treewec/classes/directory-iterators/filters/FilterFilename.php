<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class FilterFilename implements IFilter {

    private $displayFolderIndex;
    private $displayInvalidNames;
    private $displayDefaultHiddenFiles;
    private $displayUserHiddenFiles;

    public function __construct() {
        $this->displayInvalidNames = false;
        $this->displayFolderIndex = false;
        $this->displayDefaultHiddenFiles = false;
        $this->displayUserHiddenFiles = false;
    }

    public function setDisplayInvalidNames($displayInvalidNames) {
        $this->displayInvalidNames = is_bool($displayInvalidNames) ? $displayInvalidNames : $this->displayInvalidNames;
    }

    public function setDisplayFolderIndex($displayFolderIndex) {
        $this->displayFolderIndex = is_bool($displayFolderIndex) ? $displayFolderIndex : $this->displayFolderIndex;
    }

    public function setDisplayDefaultHiddenFiles($displayDefaultHidden) {
        $this->displayDefaultHiddenFiles = is_bool($displayDefaultHidden) ? $displayDefaultHidden : $this->displayDefaultHiddenFiles;
    }

    public function setDisplayUserHiddenFiles($displayUserHidden) {
        $this->displayUserHiddenFiles = is_bool($displayUserHidden) ? $displayUserHidden : $this->displayUserHiddenFiles;
    }

    public function canBeProcessed($iteratorElement) {
        $file = $iteratorElement->splFileInfo;
        return $this->isValidFilename($file) && $this->isNotFolderIndex($file) && 
               $this->isNotDefaultHiddenFile($file) && $this->isNotUserHiddenFile($file);
    }

    private function isValidFilename($file) {
        return $this->displayInvalidNames ? true : \Treewec\FileSystem\Filename::isValid($file->getFilename());
    }

    private function isNotFolderIndex($file) {
        $basename = \Treewec\FileSystem\Path::toScreen(\Treewec\Utils\Files::getBasename($file));
        return $this->displayFolderIndex ? true : TREEWEC_NAME_OF_INDEX_FILE != $basename;
    }

    private function isNotDefaultHiddenFile($file) {
        return $this->displayDefaultHiddenFiles ? true : !\Treewec\FileSystem\HiddenFilenames::isDefaultHiddenFilename($file->getFilename());
    }

    private function isNotUserHiddenFile($file) {
        return $this->displayUserHiddenFiles ? true : !\Treewec\FileSystem\HiddenFilenames::isUserHiddenFilename($file->getFilename());
    }

}

?>