<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class FilterHiddenFiles implements IFilter {

    private $displayHiddenItems;

    public function __construct() {
        $this->displayHiddenItems = false;
    }

    public function setDisplayHiddenItems($printHiddenItems) {
        $this->displayHiddenItems = is_bool($printHiddenItems) ? $printHiddenItems : $this->displayHiddenItems;
    }
    
    public function canBeProcessed($iteratorElement) {
        $file = $iteratorElement->splFileInfo;
        return $this->displayHiddenItems ? true : !\Treewec\FileSystem\Filename::isHidden($file->getFilename());
    }

}

?>