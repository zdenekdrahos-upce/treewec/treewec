<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class FilterFileType implements IFilter {

    private $fileTypeFilter;

    public function __construct() {
        $this->fileTypeFilter = \Treewec\FileSystem\FileType::FOLDER;
    }

    public function setFileTypeFilter($fileType) {
        if (\Treewec\Utils\Instances::isClassConstant('\Treewec\FileSystem\FileType', $fileType)) {
            $this->fileTypeFilter = $fileType;
        }
    }

    public function canBeProcessed($iteratorElement) {
        $file = $iteratorElement->splFileInfo;
        $isFile = $this->fileTypeFilter == \Treewec\FileSystem\FileType::FILE && $file->isFile();
        $isDir = $this->fileTypeFilter == \Treewec\FileSystem\FileType::FOLDER && $file->isDir();
        return $isFile || $isDir;
    }

}

?>