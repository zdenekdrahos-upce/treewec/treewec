<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class IteratorFilters implements IFilter {

    private $filters;

    public function __construct() {
        $this->filters = array();
    }

    public function addFilter($filter) {
        if (\Treewec\Utils\Instances::isInstanceOf($filter, '\Treewec\DirectoryIterators\IFilter')) {
            $class = get_class($filter);
            $this->filters[$class] = $filter;
        }
    }

    public function canBeProcessed($iteratorElement) {
        foreach ($this->filters as $filter) {            
            if (!$filter->canBeProcessed($iteratorElement)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return \Treewec\DirectoryIterators\IteratorFilters
     * Returns filter for only valid filenames. And filter refuse files that 
     * are hidden, Apache files, Treewec files or folder index.
     */
    public static function getInstanceWithStandardFilters() {
        $filter = new self();
        $filter->addFilter(new FilterFilename());
        $filter->addFilter(new FilterHiddenFiles());
        return $filter;
    }

}

?>