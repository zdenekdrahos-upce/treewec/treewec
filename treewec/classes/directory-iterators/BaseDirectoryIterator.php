<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

abstract class BaseDirectoryIterator implements IIterator {

    /** @var string */
    private $pathToRoot;
    /** @var Treewec\DataStructures\ILinkedList */
    private $helperStructure;
    /** @var Treewec\DirectoryIterators\IComparator */
    private $comparator;
    /** @var Treewec\DirectoryIterators\IFilter */
    private $filter;
    /** @var Treewec\DirectoryIterators\IAbort */
    private $abortCondition;

    public function __construct($path) {
        if (!is_string($path) || !is_readable($path)) {
            \Treewec\Exceptions\FileSystemException::throwsNonreadablePath();
        }
        $this->pathToRoot = $path;
        $this->comparator = false;
        $this->filter = false;
        $this->abortCondition = false;
        $this->setHelperStructure();
        $this->addRootToStructure();
    }

    /** @return \Treewec\DataStructures\ILinkedList */
    protected abstract function getHelperStructure();

    public function setComparator($comparator) {
        if ($comparator) {
            \Treewec\Utils\Instances::isInstanceOf($comparator, '\Treewec\DirectoryIterators\IComparator');
            $this->comparator = $comparator;
        }
    }

    public function setFilter($filter) {
        \Treewec\Utils\Instances::isInstanceOf($filter, '\Treewec\DirectoryIterators\IFilter');
        $this->filter = $filter;
    }

    public function setAbortCondition($abortCondition) {
        \Treewec\Utils\Instances::isInstanceOf($abortCondition, '\Treewec\DirectoryIterators\IAbort');
        $this->abortCondition = $abortCondition;
    }

    public function hasNext() {
        return !$this->helperStructure->isEmpty();
    }

    public function next() {
        $removedElement = $this->helperStructure->removeElement();
        $this->loadChildren($removedElement);
        return $removedElement;
    }

    public function rewind() {
        $this->helperStructure->clear();
        $this->addRootToStructure();
    }

    protected function loadChildren($iteratorElement) {
        if ($iteratorElement->splFileInfo->isDir()) {
            $children = $this->getChildren($iteratorElement);
            if ($children) {
                $this->sortChildren($children);
                foreach ($children as $child) {
                    $this->addChild($child);
                }
            }
        }
    }

    private function getChildren($iteratorElement) {
        $children = array();
        $parentDir = $iteratorElement->splFileInfo->getPathname();
        $fsIterator = new \FilesystemIterator($parentDir, \FilesystemIterator::SKIP_DOTS);
        foreach ($fsIterator as $file) {
            $child = new IteratorElement($iteratorElement->depth + 1, $file);
            if ($this->abortCondition && $this->abortCondition->abortChildLoading($child)) {
                return $children;
            } else if ($this->canBeElementProcessed($child)) {
                $children[] = $child;
            }
        }
        return $children;
    }

    private function canBeElementProcessed($iteratorElement) {
        return $this->filter ? $this->filter->canBeProcessed($iteratorElement) : true;
    }

    protected function sortChildren(&$children) {
        if ($this->comparator) {            
            uasort($children, array($this->comparator, 'compare'));
        }
    }

    private function addRootToStructure() {
        $root = new IteratorElement(0, new \SplFileInfo($this->pathToRoot));
        $this->addChild($root);
    }

    private function addChild($iteratorElement) {
        $this->helperStructure->insertElement($iteratorElement);
    }

    private function setHelperStructure() {
        $this->helperStructure = $this->getHelperStructure();
        \Treewec\Utils\Instances::isInstanceOf($this->helperStructure, '\Treewec\DataStructures\ILinkedList');
    }

}

?>