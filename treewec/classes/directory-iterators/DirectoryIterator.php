<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class DirectoryIterator extends BaseDirectoryIterator {

    /** @var boolean */
    private $firstRun;

    public function __construct($path) {
        parent::__construct($path);
        $this->firstRun = true;
    }

    public function hasNext() {        
        return $this->firstRun ? true : parent::hasNext();
    }

    public function next() {                
        $removed = parent::next();
        $this->firstRun = false;
        return $removed;
    }

    protected function loadChildren($iteratorElement) {        
        if ($this->firstRun) {
            parent::loadChildren($iteratorElement);
        }
    }

    protected function getHelperStructure() {
        return new \Treewec\DataStructures\FIFO();
    }
}

?>