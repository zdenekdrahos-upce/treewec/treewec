<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class ComparatorFactory {

    /**
     * @param \Treewec\DirectoryIterators\ComparatorType $comparatorType
     * @param \Treewec\DirectoryIterators\OrderType $orderType
     * @return \Treewec\DirectoryIterators\BinderComparator
     * Returns false if ComparatorType::NONE is selected 
     */
    public static function get($comparatorType = TREEWEC_DEFAULT_COMPARATOR, $orderType = TREEWEC_DEFAULT_ORDER) {
        self::checkOrderType($orderType);
        self::checkComparatorType($comparatorType);
        if ($comparatorType != ComparatorType::NONE) {
            $comparator = new BinderComparator();
            $comparator->setPrimaryComparator(self::getComparator($comparatorType));
            $comparator->setOrderType($orderType);
            return $comparator;
        } else {
            return false;
        }
    }

    private static function checkOrderType(&$orderType) {
        if (!\Treewec\Utils\Instances::isClassConstant('\Treewec\DirectoryIterators\OrderType', $orderType)) {
            $orderType = TREEWEC_DEFAULT_ORDER;
        }
    }

    private static function checkComparatorType(&$comparatorType) {
        if (!\Treewec\Utils\Instances::isClassConstant('\Treewec\DirectoryIterators\ComparatorType', $comparatorType)) {
            $comparatorType = TREEWEC_DEFAULT_COMPARATOR;
        }
    }

    private static function getComparator($comparatorType) {
        switch ($comparatorType) {
            case ComparatorType::FILENAME:
                return new FilenameComparator();
            case ComparatorType::USER:
                return new UserComparator();
            case ComparatorType::RANDOM:
                return new RandomComparator();
            case ComparatorType::FILE_TYPE:
                return new FileTypeComparator();
            default:
                $comparator = new BasicFileComparator();
                $comparator->setComparableAttribute($comparatorType);
                return $comparator;
        }
    }

}

?>