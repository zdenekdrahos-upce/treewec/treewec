<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class BinderComparator implements IComparator {

    /** @var Treewec\DirectoryIterators\OrderType */
    private $orderType;
    /** @var Treewec\DirectoryIterators\IComparator */
    private $primaryComparator;
    /** @var Treewec\DirectoryIterators\IComparator */
    private $secondaryComparator;

    public function __construct() {
        $this->orderType = TREEWEC_DEFAULT_ORDER;
        $this->primaryComparator = TREEWEC_DEFAULT_COMPARATOR;
        $this->secondaryComparator = false;        
    }

    public function setPrimaryComparator($primaryComparator) {
        $this->setComparator('primaryComparator', $primaryComparator);
    }

    public function setSecondaryComparator($secondaryComparator) {
        $this->setComparator('secondaryComparator', $secondaryComparator);
    }

    public function setOrderType($orderType) {
        $isClassConstant = \Treewec\Utils\Instances::isClassConstant('\Treewec\DirectoryIterators\OrderType', $orderType);
        $this->orderType = $isClassConstant ? $orderType : $this->orderType;
    }

    public function compare($a, $b) {
        $result = $this->compareInComparator('primaryComparator', $a, $b);
        return $this->useSecondComparator($result) ? $this->compareInComparator('secondaryComparator', $a, $b) : $result;
    }

    private function compareInComparator($comparatorName, $a, $b) {
        $result = $this->$comparatorName->compare($a, $b);
        return $this->orderType == OrderType::ASCENDING ? $result : \Treewec\Utils\Comparators::negate($result);
    }

    private function useSecondComparator($result) {
        return $result == 0 && $this->secondaryComparator;
    }

    private function setComparator($comparatorName, $comparator) {
        \Treewec\Utils\Instances::isInstanceOf($comparator, '\Treewec\DirectoryIterators\IComparator');
        $this->$comparatorName = $comparator;
    }

}

?>