<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class ComparatorType {

    const NONE = 'NONE';
    const USER = 'USER';
    const RANDOM = 'RANDOM';
    const FILENAME = 'FILENAME';
    const FILE_TYPE = 'FILE_TYPE';
    const MODIFICATION_DATE = 'MODIFICATION_DATE';
    const CREATION_DATE = 'CREATION_DATE';
    const ACCESS_DATE = 'ACCESS_DATE';
    const SIZE = 'SIZE';

    private function __construct() {}

}

?>