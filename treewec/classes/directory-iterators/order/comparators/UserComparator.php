<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class UserComparator implements IComparator {

    private $directoryOrder;
    private $currentOrderArray;
    private $elementsCount;

    public function __construct() {
        $this->directoryOrder = new DirectoryOrder();
        $this->currentOrderArray = array();
        $this->elementsCount = 0;
    }

    public function compare($a, $b) {
        $this->updateDirectoryOrder($a->splFileInfo);
        $a = \Treewec\FileSystem\Path::toScreen($a->splFileInfo->getFilename());
        $b = \Treewec\FileSystem\Path::toScreen($b->splFileInfo->getFilename());
        if ($this->canBeCompared($a, $b)) {
            $a = $this->elementsCount - $this->currentOrderArray[$a];
            $b = $this->elementsCount - $this->currentOrderArray[$b];
            return \Treewec\Utils\Comparators::compareNumbers($a, $b);
        } else {
            return 1;
        }
    }

    private function updateDirectoryOrder($splFileInfo) {
        $change = $this->directoryOrder->changeDirectory($splFileInfo->getPath());
        if ($change) {
            $this->currentOrderArray = $this->directoryOrder->getOrderArray();
            $this->elementsCount = count($this->currentOrderArray);
        }
    }

    private function canBeCompared($a, $b) {
        return array_key_exists($a, $this->currentOrderArray) && array_key_exists($b, $this->currentOrderArray);
    }

}

?>