<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class FilenameComparator implements IComparator {

    private $isUTF8;
    private $isCaseSensitive;
    
    public function __construct() {
        $this->isUTF8 = false;
        $this->isCaseSensitive = false;
    }
    
    public function setUTF8($isUTF8) {
        $this->isUTF8 = is_bool($isUTF8) ? $isUTF8 : $this->isUTF8;
    }

    public function setCaseSensitive($isCaseSensitive) {
        $this->isCaseSensitive = is_bool($isCaseSensitive) ? $isCaseSensitive : $this->isCaseSensitive;
    }

    public function compare($a, $b) {        
        $a = \Treewec\FileSystem\Path::toScreen($a->splFileInfo->getFilename());
        $b = \Treewec\FileSystem\Path::toScreen($b->splFileInfo->getFilename());
        if (!$this->isCaseSensitive) {
            $a = mb_strtolower($a, 'UTF-8');
            $b = mb_strtolower($b, 'UTF-8');
        }
        if ($this->isUTF8) {
            return \Treewec\Utils\Comparators::compareUTF8Strings($a, $b);
        } else {
            return \Treewec\Utils\Comparators::compareStrings($a, $b);
        }
    }

}

?>