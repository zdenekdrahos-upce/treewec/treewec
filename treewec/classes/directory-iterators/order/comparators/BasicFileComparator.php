<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class BasicFileComparator implements IComparator {

    const MODIFICATION_DATE = 'MODIFICATION_DATE';
    const CREATION_DATE = 'CREATION_DATE';
    const ACCESS_DATE = 'ACCESS_DATE';
    const SIZE = 'SIZE';

    private $comparableAttribute;

    public function __construct() {
        $this->comparableAttribute = self::MODIFICATION_DATE;
    }

    public function setComparableAttribute($comparableAttribute) {
        $isClassConstant = \Treewec\Utils\Instances::isClassConstant('\Treewec\DirectoryIterators\BasicFileComparator', $comparableAttribute);
        $this->comparableAttribute = $isClassConstant ? $comparableAttribute : $this->comparableAttribute;
    }

    public function compare($a, $b) {
        $method = $this->getCompareMethod();
        $a = $a->splFileInfo->$method();
        $b = $b->splFileInfo->$method();
        return \Treewec\Utils\Comparators::compareNumbers($a, $b);
    }

    private function getCompareMethod() {
        switch ($this->comparableAttribute) {
            case self::MODIFICATION_DATE: return 'getMTime';
            case self::CREATION_DATE: return 'getCTime';
            case self::ACCESS_DATE: return 'getATime';
            case self::SIZE: return 'getSize';
        }
    }

}

?>