<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryIterators;

final class FileTypeComparator implements IComparator {

    private $fileType;

    public function __construct() {
        $this->fileType = \Treewec\FileSystem\FileType::FILE;
    }

    public function setFileTypeFilter($fileType) {
        $isClassConstant = \Treewec\Utils\Instances::isClassConstant('\Treewec\FileSystem\FileType', $fileType);
        $this->fileType = $isClassConstant ? $fileType : $this->fileType;
    }

    public function compare($a, $b) {
        $method = $this->getCompareMethod();
        $a = (int) $a->splFileInfo->$method();
        $b = (int) $b->splFileInfo->$method();        
        return \Treewec\Utils\Comparators::compareNumbers($a, $b);
    }

    private function getCompareMethod() {
        switch ($this->fileType) {
            case \Treewec\FileSystem\FileType::FILE: return 'isFile';
            case \Treewec\FileSystem\FileType::FOLDER: return 'isDir';
        }
    }

}

?>