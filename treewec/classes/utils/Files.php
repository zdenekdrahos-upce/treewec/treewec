<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Utils;

final class Files {

    /**
     * Needed because SplFileInfo::getBasename($extension) returns word without first character
     * on Linux, if first character is not ASCII
     * @param \SplFileInfo $splFileInfo
     * @param string $extension
     * @return string 
     * Returns filename without extension
     */
    public static function getBasename($splFileInfo, $extension = TREEWEC_DEFAULT_FILE_EXTENSION) {
        return str_replace($extension, '', $splFileInfo->getFilename());
    }

    //http://www.php.net/manual/en/function.filesize.php#106569
    public static function humanFilesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

}

?>