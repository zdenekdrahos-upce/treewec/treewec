<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Utils;

final class Arrays {

    public static function convertArrayToString($array, $separator = '/') {
        $string = '';
        if (is_array($array) && !empty($array) && is_string($separator)) {
            $string .= implode($separator, $array);
        }
        return $string;
    }

    public static function convertStringToArray($string, $separator = '/') {
        $array = array();
        if (is_string($string) && is_string($separator)) {
            $array = explode($separator, $string);
        }
        return $array;
    }

    public static function convertArrayToStringPairs($array, $separator = '=') {
        $pairs = array();
        if (is_array($array) && is_string($separator)) {
            foreach ($array as $key => $value) {
                $pairs[] = $key . $separator . $value;
            }
        }
        return $pairs;
    }

    public static function isArrayWithScalarValues($array) {
        if (is_array($array)) {
            foreach ($array as $value) {
                if (!is_scalar($value)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static function getNumberOfEmptyValues($array) {
        if (is_array($array)) {
            $arrayWithEmptyValues = array_filter($array, array('Treewec\Utils\Variables', 'isEmpty'));
            return count($arrayWithEmptyValues);
        }
        return 0;
    }

}

?>