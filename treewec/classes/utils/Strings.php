<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Utils;

final class Strings {

    public static function uppercaseFirstLetterAndReplaceDashes($string, $replace = ' ') {
        if (is_string($string) && is_string($replace)) {
            $string = self::ucfirstUTF8($string);
            $string = str_replace('-', $replace, $string);
        }
        return $string;
    }
    
    public static function ucfirstUTF8($string) {
        if (is_string($string)) {
            $strlen = mb_strlen($string, 'UTF-8');
            $firstChar = mb_substr($string, 0, 1, 'UTF-8');
            $afterFirstChar = mb_substr($string, 1, $strlen - 1, 'UTF-8');
            return mb_strtoupper($firstChar, 'UTF-8') . $afterFirstChar;
        } 
        return '';
    }

    public static function codePHPInString($string) {
        if (is_string($string)) {
            $codes = array(
                '<?php' => '&amp;lt;?php',
                '?>' => '?&amp;gt;',
                '=>' => '=&amp;gt;',
                '&lt;?php' => '[php',
                '?&gt;' => 'php]',
            );
            return str_replace(array_keys($codes), array_values($codes), $string);
        } 
        return null;
    }

    public static function decodePHPInString($string) {
        if (is_string($string)) {
            $codes = array(
                '&amp;lt;?php' => '<?php',
                '&lt;?php' => '<?php',
                '?&amp;gt;' => '?>',
                '?&gt;' => '?>',
                '-&gt;' => '->',
                '-&amp;gt;' => '->',
                '=&gt;' => '=>',
                '=&amp;gt;' => '=>',
                '[php' => '&lt;?php',
                'php]' => '?&gt;',
            );
            return str_replace(array_keys($codes), array_values($codes), $string);
        } 
        return null;
    }

}

?>