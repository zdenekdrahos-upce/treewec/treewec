<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Utils;

final class Functions {

    public static function redirectToPage($page) {
        $page = is_string($page) ? $page : 'index.php';
        header("Location: {$page}");
        exit;
    }

    public static function getUrlArray() {
        if (TREEWEC_URL_REWRITE && (is_int(strpos($_SERVER['REQUEST_URI'], '?')) || empty($_SERVER['QUERY_STRING']))) {
            $currentPathWithoutIndexPhp = str_replace('index.php', '', $_SERVER['PHP_SELF']);            
            $queryString = 'path=' . str_replace($currentPathWithoutIndexPhp, '', $_SERVER['REQUEST_URI']);
            parse_str($queryString, $_GET);
        } else if (empty($_SERVER['QUERY_STRING'])) {
            $_GET = array();
            $_GET['path'] = '';
        }
        return $_GET;        
    }

}

?>