<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Utils;

/*
 * If a = b -> return 0
 * If a > b -> return 1
 * If a < b -> return -1 
 */
final class Comparators {

    public static function negate($resultAfterCompare) {
        if (!is_numeric($resultAfterCompare)) {
            return 1;
        } else if ($resultAfterCompare == 0) {
            return 0;
        } else {
            return $resultAfterCompare == 1 ? -1 : 1;
        }
    }

    public static function compareNumbers($a, $b) {
        if (!is_numeric($a) || !is_numeric($b)) {
            return 1;
        } else if ($a == $b) {
            return 0;
        } else {
            return $a > $b ? 1 : -1;
        }
    }

    public static function compareNumberOfSlashesInString($a, $b) {
        $a = substr_count($a, '/');
        $b = substr_count($b, '/');
        return self::compareNumbers($b, $a);        
    }

    public static function compareStrings($a, $b) {
        $result = self::compareStringsAndNumbers($a, $b);
        return is_null($result) ? strcmp($a, $b) : $result;
    }

    public static function compareUTF8Strings($a, $b) {
        $result = self::compareStringsAndNumbers($a, $b);
        return is_null($result) ? strcoll($a, $b) : $result;
    }

    private static function compareStringsAndNumbers($a, $b) {
        $isNumericA = is_numeric($a);
        $isNumericB = is_numeric($b);
        $isStringA = is_string($a);
        $isStringB = is_string($b);
        if ($isNumericA && $isNumericB) {
            return self::compareNumbers($a, $b);
        } else if ($isNumericA && $isStringB) {
            return 1;
        } else if ($isNumericB && $isStringA) {
            return -1;
        } else if ($isStringA && $isStringB) {
            return null;
        } else {
            return 1; // invalid arguments
        }
    }

}

?>