<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Utils;

final class Instances {

    /** @throws \Treewec\Exceptions\InstanceException */
    public static function isInstanceOf($object, $class) {
        if (!($object instanceof $class)) {
            throw new \Treewec\Exceptions\InstanceException();
        }
        return true;
    }

    public static function isClassConstant($class, $constant) {
        if (is_string($class) && is_string($constant)) {
            $helper = new \ReflectionClass($class);
            return $helper->getConstant($constant) != false;
        }
        return false;
    }

}

?>