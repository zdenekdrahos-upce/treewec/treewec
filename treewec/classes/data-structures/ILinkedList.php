<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DataStructures;

interface ILinkedList {
    
    public function __construct();
    
    public function clear();
    
    public function isEmpty();
            
    public function size();   
    
    public function insertElement($element);
    
    public function removeElement();
    
}

?>