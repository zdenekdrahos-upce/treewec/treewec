<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DataStructures;

final class FIFO implements ILinkedList {

    private $items;

    public function __construct() {
        $this->items = array();
    }

    public function clear() {
        $this->items = array();
    }

    public function isEmpty() {
        return $this->size() == 0;
    }

    public function size() {
        return count($this->items);
    }

    public function insertElement($element) {
        if (!\Treewec\Utils\Variables::isEmpty($element)) {
            array_push($this->items, $element);
        }
    }

    public function removeElement() {
        if (!$this->isEmpty()) {
            return array_shift($this->items);
        }
        return null;
    }

}

?>