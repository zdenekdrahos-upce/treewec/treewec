<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class ViewDisplayer {

    /**
     * @param \Treewec\View $view
     * @param boolean $contentEditable 
     */
    public static function display($view) {
        if (self::canBeDisplayedContentEditable()) {
            self::setGlobalBackLink();
            self::checkSessionMessage();
            $editor = new Admin\Editors\Editor('contenteditable');
            $editor->includeRequiredFilesInHTMLHead();
            $editor->printHeader();
            $view->activateEditMode();
            $view->display();
            $editor->printFooter();
        } else {
            $view->display();
        }
    }

    private static function canBeDisplayedContentEditable() {
        $isEditorSet = isset($_GET['editor']) && $_GET['editor'] == 'contenteditable';
        $isEditorDefault = TREEWEC_DEFAULT_ADMIN_EDITOR == 'contenteditable' && !isset($_GET['editor']);
        $isSetGlobalCurrentPath = isset($GLOBALS['currentPath']);
        $isInAdminArea = is_int(strpos($_SERVER['PHP_SELF'], 'admin/index.php'));
        return ($isEditorSet || $isEditorDefault) && $isSetGlobalCurrentPath && $isInAdminArea;
    }

    private static function setGlobalBackLink() {
        global $arrayHolder;
        $urlBuilder = new URLBuilder($arrayHolder);
        $urlBuilder->removeParameter('editor');
        $urlBuilder->removeParameter('action');
        $GLOBALS['backLink'] = $urlBuilder->build();
        $urlBuilder->removeParameter('editor');
        $GLOBALS['webLink'] = str_replace('admin/', '', $urlBuilder->build());
    }

    private static function checkSessionMessage() {
        $session = \Treewec\Admin\Session::getInstance();
        if ($session->hasMessage()) {
            $GLOBALS['sessionMessage'] = $session->getMessage();
            $GLOBALS['sessionBgColor'] = $session->getMessageStatus() == 'success' ? '#3c0' : '#f00';
        }
    }

}

?>