<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class View {

    private $viewParts;
    private $dynamicData;
    private $editMode;

    public function __construct() {
        $this->viewParts = array();
        $this->dynamicData = array();
        $this->editMode = false;
    }

    /**
     * @param boolean $includeFile 
     * TRUE - content of the file is printed (not interpred)
     * FALSE - include is used
     */
    public function activateEditMode() {
        $this->editMode = true;
    }

    public function display() {
        extract($this->dynamicData);
        foreach ($this->viewParts as $displayedView) {
            if ($displayedView instanceof FileSystem\File) {
                if ($this->editMode) {
                    $content = file_get_contents($displayedView->getPath());
                    $content = Utils\Strings::codePHPInString($content);
                    echo $content;
                } else {
                    include($displayedView->getPath());
                }
            } else {
                echo $displayedView;
            }
        }
    }

    public function addContent($content) {
        $this->viewParts[] = $content;
    }

    public function addData($data) {
        if (is_array($data)) {
            $this->dynamicData = $this->dynamicData + $data;
        }
    }

}

?>