<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

abstract class Controller {

    /** @var Treewec\View */
    protected $view;
    /** @var Treewec\ArrayHolder */
    protected $arrayHolder;
    /** @var Treewec\URLArgs */
    protected $urlArguments;
    /** @var Treewec\PageInfo */
    protected $pageInfo;

    public function __construct($arrayHolder) {
        $this->view = new View();
        $this->arrayHolder = $arrayHolder;
    }

    public function displayView() {        
        extract($this->getExtractedVariablesToView());
        include($this->getThemeFile('index'));
    }

    public function loadView() {
        try {
            $this->inspectURLAdress();
            $this->loadViewFromUrlPath();
            $this->pageInfo = PageInfoLoader::getPageInfo($this->arrayHolder);
            return true;
        } catch (\Exception $e) {
            $this->processException($e);
        }
        return false;
    }

    private function inspectURLAdress() {
        $this->urlArguments = URLArgs::createFromArrayHolder($this->arrayHolder);
        $urlInspector = $this->getURLArgsInspector();
        $urlInspector->addInspectedObject($this->urlArguments);
        $urlInspector->check();
    }

    private function loadViewFromUrlPath() {
        $abstractFile = FileSystem\FileFactory::build($this->urlArguments->path, $this->getFileRootDirectory());
        if ($abstractFile instanceof FileSystem\File) {
            $this->loadViewFromFile($abstractFile);
        } else if ($abstractFile instanceof FileSystem\Directory) {
            $this->loadViewFromDirectory($abstractFile);
        }
    }

    private function loadViewFromFile($file) {
        $this->view->addContent($file);
    }

    protected function loadViewFromDirectory($directory) {
        try {
            $filePath = $directory->getPath() . TREEWEC_NAME_OF_INDEX_FILE . TREEWEC_DEFAULT_FILE_EXTENSION;
            $this->view->addContent(new FileSystem\File($filePath));
            $this->view->addData(array('path' => TREEWEC_VIEW_DIR . FileSystem\Path::toFileSystem($this->urlArguments->path)));
        } catch (\Treewec\Exceptions\FileSystemException $e) {
            \Treewec\Exceptions\ClientRequestException::throwsFileNotFound();
        }
    }

    protected function processException($exception) {
        $exception = $this->checkExceptionForbidden($exception);
        if ($exception instanceof Exceptions\ClientRequestException) {
            $exception->setHttpResponse();
        }
        $this->view = new View();
        $this->loadViewFromException($exception);
        $this->pageInfo = PageInfoLoader::getErrorPageInfo($this->arrayHolder);
    }

    private function checkExceptionForbidden($exception) {
        if ($exception instanceof Exceptions\ClientRequestException && $exception->getCode() == 403) {
            try {
                FileSystem\FileFactory::build($this->urlArguments->path, $this->getFileRootDirectory());
            } catch (Exceptions\ClientRequestException $fileNotFoundException) {
                return $fileNotFoundException;
            }
        }
        return $exception;
    }

    private function loadViewFromException($exception) {
        $this->view->addData(array('exception' => $exception));
        $this->view->addContent(new FileSystem\File($this->getThemeFile('error')));
    }

    private function getThemeFile($fileName) {
        $fullPath = $this->getThemeRootDirectory() . $fileName . '.php';
        if (!file_exists($fullPath)) {
            $errorMessage = Admin\Message::get('controller-theme-error', array($fileName));
            die($errorMessage);
        }
        return $fullPath;
    }

    protected function getExtractedVariablesToView() {
        return array(
            'pageInfo' => $this->pageInfo,
            'arrayHolder' => $this->arrayHolder,
            'view' => $this->view,
        );
    }

    protected abstract function getURLArgsInspector();

    protected abstract function getThemeRootDirectory();

    protected abstract function getFileRootDirectory();
}

?>