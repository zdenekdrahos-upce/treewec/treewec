<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class PageInfoLoader {

    private $urlArgs;
    private $tempPathArray;
    private $pageInfo;

    private function __construct($arrayHolder) {
        $this->urlArgs = URLArgs::createFromArrayHolder($arrayHolder);
        $pathArray = Utils\Arrays::convertStringToArray($this->urlArgs->path);
        $this->tempPathArray = array_filter($pathArray, array($this, 'isValidHeader'));
        $this->pageInfo = new PageInfo();
        $this->loadPageInfo();
    }

    public static function getPageInfo($arrayHolder) {
        $loader = new self($arrayHolder);
        if (!$loader->isMainPage()) {
            $loader->loadNextDoorPages();
        }
        return $loader->pageInfo;
    }

    public static function getErrorPageInfo($arrayHolder) {
        $loader = new self($arrayHolder);
        $loader->loadErrorHeader();
        return $loader->pageInfo;
    }

    private function loadPageInfo() {
        $this->loadCurrentPath();
        $this->loadPageType();
        $this->loadPageVisibility();
        $this->loadPathLinkArray();
        $this->loadHeader();
        $this->loadHomePage();
        if (!$this->isMainPage()) {            
            $this->loadParentFolder();
        }
    }

    private function loadCurrentPath() {
        $this->pageInfo->currentPath = $this->urlArgs->path;
    }

    private function loadPageType() {
        $this->pageInfo->pageType = FileSystem\FileType::getType($this->pageInfo->currentPath);
    }

    public function loadPageVisibility() {
        $this->pageInfo->pageVisibility = FileSystem\Path::isHidden($this->pageInfo->currentPath) ? 'hidden' : 'public';
    }

    private function loadPathLinkArray() {
        for ($i = 1, $max = $this->getItemCount(); $i <= $max; $i++) {
            $this->pageInfo->pathInfo[] = new OnePage($this->getPathForHeader($i));
        }
    }

    private function loadHeader() {
        $this->pageInfo->header = new OnePage($this->pageInfo->currentPath);
    }

    private function loadErrorHeader() {
        $this->pageInfo->header = new OnePage('');
        $this->pageInfo->header->header = TREEWEC_DEFAULT_ERROR;
    }

    private function loadHomePage() {
        $this->pageInfo->homePage = new OnePage('');
    }

    private function loadParentFolder() {
        $this->pageInfo->parentFolder = new OnePage($this->getPathForHeader($this->getItemCount() - 1));
    }

    private function loadNextDoorPages() {
        $path = $this->getPathForHeader($this->getItemCount() - 1);
        $nd = new DirectoryIterators\NextDoorNeighbours($this->pageInfo->getFilePath(TREEWEC_VIEW_DIR));
        static $attributes = array('P' => 'previousPage', 'N' => 'nextPage');
        foreach ($nd->getNextDoorNeighbours() as $key => $value) {
            if (!is_null($value)) {
                $attribute = $attributes[$key];
                $this->pageInfo->$attribute = new OnePage($path . $value);
            }
        }
    }

    private function getPathForHeader($indexOfLastFile) {
        if ($this->getItemCount() > 0 && $indexOfLastFile > 0) {
            $pathArray = array_slice($this->tempPathArray, 0, $indexOfLastFile);
            $path = Utils\Arrays::convertArrayToString($pathArray, '/');
            $addSlash = $indexOfLastFile < $this->getItemCount() || $this->pageInfo->pageType != FileSystem\FileType::FILE;
            return $addSlash ? ($path . '/') : $path;
        }
        return '';
    }

    private function isMainPage() {
        return $this->getItemCount() == 0;
    }

    private function getItemCount() {
        return count($this->tempPathArray);
    }

    private function isValidHeader($header) {
        return !Utils\Variables::isEmpty($header) && $header != '.';
    }

}

?>