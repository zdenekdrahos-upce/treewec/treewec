<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\DirectoryPrinter;

final class OutputFileHelper {

    /** @var string real path to root directory, default is TREEWEC_VIEW_DIR */
    private $rootDir;
    /** @var string */
    private $rootName;

    public function __construct($rootDirectory, $rootName = TREEWEC_MAINPAGE) {
        $this->setRootDirectory($rootDirectory);
        $this->rootName = is_string($rootName) ? $rootName : TREEWEC_MAINPAGE;
    }

    public function getFilename($splFileInfo, $dashReplace = ' ') {
        if ($splFileInfo->getRealPath() == $this->rootDir) {
            return $this->rootName;
        } else {
            return $this->normalizeFilename($splFileInfo, $dashReplace);
        }
    }

    private function normalizeFilename($splFileInfo, $dashReplace = ' ') {
        $basename = \Treewec\Utils\Files::getBasename($splFileInfo);
        $screenFilename = \Treewec\FileSystem\Path::toScreen($basename);
        $normalizedFilename = \Treewec\Utils\Strings::uppercaseFirstLetterAndReplaceDashes($screenFilename, $dashReplace);
        return $normalizedFilename;
    }

    public function getBaseLinkForURL($splFileInfo) {        
        if ($splFileInfo->getRealPath() == $this->rootDir) {
            return '';
        } else {
            return $this->getURLFilePath($splFileInfo);
        }
    }

    private function getURLFilePath($splFileInfo) {        
        $urlPath = str_replace('\\', '/', $this->getRawPath($splFileInfo));
        $urlPath .= \Treewec\Utils\Files::getBasename($splFileInfo);
        $urlPath .= $splFileInfo->isDir() ? '/' : '';
        if (substr($urlPath, 0, 1) == '/') {
            $urlPath = substr($urlPath, 1);
        }
        return \Treewec\FileSystem\Path::toScreen($urlPath);
    }

    private function getRawPath($splFileInfo) {
        $realParent = realpath($splFileInfo->getPath());
        return substr($realParent, strlen($this->rootDir)) . '/';
    }

    private function setRootDirectory($rootDirectory) {
        $this->rootDir = realpath(TREEWEC_VIEW_DIR);
        if (is_string($rootDirectory) && is_dir($rootDirectory)) {
            $this->rootDir = realpath($rootDirectory);
        }
    }

}

?>