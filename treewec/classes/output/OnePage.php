<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class OnePage {

    /** @var string */
    public $header;
    /** @var string */
    public $path;
    /** @var string */
    public $link;

    public function __construct($path) {
        $name = FileSystem\Path::getFilenameFromPath($path);
        $this->header = $name === '' ? TREEWEC_MAINPAGE : $name;
        $this->path = $path;
        $this->link = URLFactory::getLink($path);
    }

    public function getLink($textOfTheLink = '', $attributes = array()) {
        $textOfTheLink = $textOfTheLink !== '' ? $textOfTheLink : $this->header;
        return HTML\Anchor::create($this->link, $textOfTheLink, $attributes);
    }

    public function __toString() {
        return $this->header;
    }

}

?>