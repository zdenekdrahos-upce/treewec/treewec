<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec;

final class PageInfo {

    /** @var string */
    public $currentPath;
    /** @var string - constant from \Treewec\FileSystem\FileType */
    public $pageType;
    /** @var string - hidden, public */
    public $pageVisibility;
    /** @var array - item = \Treewec\OnePage */
    public $pathInfo;
    /** @var string|\Treewec\OnePage */
    public $header;
    /** @var \Treewec\OnePage */
    public $homePage;
    /** @var \Treewec\OnePage */
    public $previousPage;
    /** @var \Treewec\OnePage */
    public $nextPage;
    /** @var \Treewec\OnePage */
    public $parentFolder;

    public function __construct() {
        foreach (array_keys(get_object_vars($this)) as $attribute) {
            $this->$attribute = false;
        }
        $this->pathInfo = array();
    }

    public function getFilePath($rootDirectory = TREEWEC_VIEW_DIR, $extension = TREEWEC_DEFAULT_FILE_EXTENSION) {
        $extension = $this->pageType == \Treewec\FileSystem\FileType::FILE ? $extension : '';
        $path = $rootDirectory . $this->currentPath . $extension;
        return \Treewec\FileSystem\Path::toFileSystem($path);
    }

}

?>