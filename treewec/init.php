<?php
define('TREEWEC_VERSION', '1.1');

define('TREEWEC_SITE_ROOT', dirname(__FILE__) . '/../');
define('TREEWEC_VIEW_DIR', TREEWEC_SITE_ROOT . 'view/');
define('TREEWEC_CLASS_ROOT', dirname(__FILE__) . '/classes/');
define('TREEWEC_ADMIN_ROOT', dirname(__FILE__) . '/admin/');
define('TREEWEC_USER_FILES', TREEWEC_SITE_ROOT . 'user-files/');
define('TREEWEC_THEMES', TREEWEC_USER_FILES . 'themes/');
define('TREEWEC_CONFIGURATION', TREEWEC_USER_FILES . 'config/');

require_once('autoload.php');
require_once(TREEWEC_CONFIGURATION . 'treewec.php');
require_once(TREEWEC_CONFIGURATION . 'default-texts.php');

define('TREEWEC_DEFAULT_HIDDEN_FILES', '.htaccess;.htpasswd;dir.order');

define('TREEWEC_COOKIE_NAME', 'treewec_language');
define('TREEWEC_COOKIE_EXPIRE', time() + 60 * 60 * 24 * 365);
define('TREEWEC_LANG_ROOT', TREEWEC_ADMIN_ROOT . 'langs/');
define('TREEWEC_LANG_EXT', '.php');

define('TREEWEC_LOGGER_LOG_ROOT', TREEWEC_USER_FILES . 'logs/');
define('TREEWEC_LOGGER_DEFAULT_LOG_NAME', 'log');
define('TREEWEC_LOGGER_EXT', '.csv');
define('TREEWEC_LOGGER_SEP', ';');
define('TREEWEC_LOGGER_TIME_FORMAT', '%Y-%m-%d %H:%M:%S');

setlocale(LC_ALL, "cs_CZ.utf8"); // Works only on Linux
?>