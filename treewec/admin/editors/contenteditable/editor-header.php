<?php if(isset($GLOBALS['sessionMessage'])): ?>
<div style="border: 1px solid black;padding:1ex;margin: 5px 0;color:white;background: <?php echo $GLOBALS['sessionBgColor']; ?>">
    <?php echo $GLOBALS['sessionMessage']; ?>
</div>
<?php endif; ?>
<div>
    <?php if(isset($GLOBALS['webLink'])): ?><a href="<?php echo $GLOBALS['webLink']; ?>"><?php echo \Translator::get('menu', 'linkback'); ?></a> | <?php endif; ?>    
    <?php if(isset($GLOBALS['backLink'])): ?>
    <a href="<?php echo $GLOBALS['backLink']; ?>&action=info"><?php echo \Translator::get('file', 'info', 'header'); ?></a> |
    <a href="<?php echo $GLOBALS['backLink']; ?>&editor=ckeditor"><?php echo \Translator::get('file', 'edit', 'backlink'); ?> CKEditor (WYSIWIG)</a>
    <?php endif; ?>
</div>
<button id="save" style="margin: 0 0 1ex; width:100%; padding:1ex;text-align:center"><?php echo \Translator::get('file', 'edit', 'submit'); ?></button>
<form>    
<div id="toolbar" style="border: 1px dashed #000; border-bottom: 0; background: #eee; color: #000; padding: 1ex;">    
    <a data-wysihtml5-command="formatInline" data-wysihtml5-command-value="strong"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'strong'); ?></a> |
    <a data-wysihtml5-command="formatInline" data-wysihtml5-command-value="em"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'em'); ?></a> |    
    <a data-wysihtml5-command="createLink"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'a'); ?></a> |
    <a data-wysihtml5-command="insertImage"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'img'); ?></a> |
    <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h1"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'h1'); ?></a> |
    <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h2"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'h2'); ?></a> |
    <a data-wysihtml5-command="insertUnorderedList"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'ul'); ?></a> |
    <a data-wysihtml5-command="insertOrderedList"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'ol'); ?></a> |
    <a data-wysihtml5-command="insertSpeech"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'speech'); ?></a> |
    <a data-wysihtml5-action="change_view"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'html'); ?></a>

    <div data-wysihtml5-dialog="createLink" style="display: none; margin: 5px 0 0; padding: 5px;">
        <label>
            <?php echo \Translator::get('file', 'edit', 'wysihtml5', 'link'); ?>:
            <input data-wysihtml5-dialog-field="href" value="http://">
        </label>
        <button data-wysihtml5-dialog-action="save">OK</button>&nbsp;<button data-wysihtml5-dialog-action="cancel"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'cancel'); ?></button>
    </div>

    <div data-wysihtml5-dialog="insertImage" style="display: none; margin: 5px 0 0; padding: 5px;">
        <label>
            <?php echo \Translator::get('file', 'edit', 'wysihtml5', 'image'); ?>:
            <input data-wysihtml5-dialog-field="src" value="http://">
        </label>
        <button data-wysihtml5-dialog-action="save">OK</button>&nbsp;<button data-wysihtml5-dialog-action="cancel"><?php echo \Translator::get('file', 'edit', 'wysihtml5', 'cancel'); ?></button>
    </div>
</div>
<textarea id="editContent" style="border: 1px dashed #000; width: 100%; height: 600px; font: inherit; background: transparent">
