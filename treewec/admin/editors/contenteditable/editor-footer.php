</textarea>
</form>
<small>Powered by <a href="https://github.com/xing/wysihtml5" target="_blank">wysihtml5</a>.</small>

<script src="<?php echo TREEWEC_WEB_ROOT; ?>admin/public/editors/contenteditable/parserRules.js" type="text/javascript"></script>
<script src="<?php echo TREEWEC_WEB_ROOT; ?>admin/public/editors/contenteditable/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script>
  var editor = new wysihtml5.Editor("editContent", {
    toolbar:      "toolbar",
    stylesheets:  <?php echo \Treewec\Admin\Editors\ContentEditableEditor::getStyles(); ?>,
    parserRules:  wysihtml5ParserRules
  });  
  editor.composer.commands.exec("formatInline", "span");
  editor.composer.commands.exec("formatInline", "italic");
</script>