<div id="alerts">
    <noscript>
        <p>
            <strong>CKEditor requires JavaScript to run</strong>. In a browser with no JavaScript
            support, like yours, you should still see the contents (HTML data) and you should
            be able to edit it normally, without a rich editor interface.
        </p>
    </noscript>
</div>

<p>    
    <script type="text/javascript">
        //<![CDATA[
        document.write( '<select disabled="disabled" id="languages" onchange="createEditor( this.value );">' );
        // Get the language list from the _languages.js file.
        for ( var i = 0 ; i < window.CKEDITOR_LANGS.length ; i++ )
        {
            document.write(
            '<option value="' + window.CKEDITOR_LANGS[i].code + '">' +
                window.CKEDITOR_LANGS[i].name +
                '</option>' );
        }
        document.write( '</select>' );
        //]]>
    </script>
</p>

<textarea name="editContent" id="editContent" form="editForm" >