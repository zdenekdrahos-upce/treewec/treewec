</textarea>
<script type="text/javascript">
    //<![CDATA[

    // Set the number of languages.
    //document.getElementById( 'count' ).innerHTML = window.CKEDITOR_LANGS.length;

    var editor;

    function createEditor( languageCode )
    {
        if ( editor )
            editor.destroy();
        editor = CKEDITOR.replace( 'editContent',
        {
            language : languageCode,
            on :
                {
                instanceReady : function()
                {
                    var languages = document.getElementById( 'languages' );
                    languages.value = this.langCode;
                    languages.disabled = false;
                }
            }
        } );
    }
    
    CKEDITOR.on('instanceReady', function(ev)
    {
        var tags = ['p', 'ol', 'ul', 'li', 'blockquote', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'div', 'code'];

        for (var key in tags) {
            ev.editor.dataProcessor.writer.setRules(tags[key],
                {
                    indent : false,
                    breakBeforeOpen : true,
                    breakAfterOpen : false,
                    breakBeforeClose : false,
                    breakAfterClose : true
                });
        }
    });
    
    // At page startup, load the default language:
    createEditor( '' );
    //]]>
</script>
