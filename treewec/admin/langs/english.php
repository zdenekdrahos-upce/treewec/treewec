<?php
# General 
$lang['save'] = 'Save';
# Languages
$lang['czech'] = 'Czech';
$lang['english'] = 'English';
# Menu
$lang['menu'] = array(
    'display' => 'D<br />i<br />s<br />p<br />l<br />a<br />y<br /><br />m<br />e<br />n<br />u<br />',
    'version' => 'Version',
    'main' => 'Main navigation',
    'home' => 'Homepage',
    'pages' => 'All pages',
    'themes' => 'Themes',
    'plugins' => 'Plugins',
    'help' => 'Help',
    'linkback' => 'Back to website',
    'system' => 'Your system',
    'settings' => 'Settings',
    'audit' => 'Check system',
    'langs' => 'Languages',
    'access' => 'Administration',
    'login' => 'Login',
    'logout' => 'Logout',
);
# Home 
$lang['home'] = array(
    'welcome' => 'Welcome in Treewec! Treewec is very simple but very powerful tool for creating websites. It can be used as <abbr title="Content Managment System">CMS</abbr>, tool for microsites or for creating web documentation. Created web is completely customizable via themes.',
    'info' => 'Basic info about your system',
    'pages-count' => 'Number of pages',
    'nav' => 'Same navigation as in left panel. The left panel is shown if you hover (with your mouse) dark strip on the left side on every page.',    
);
# Pages 
$lang['pages'] = array(
    'advanced' => 'Advanced actions',
    'filtering' => 'Filtering',
    'bulk-actions' => 'Bulk actions',
    'table' => array(
        'pages' => 'Pages',
        'actions' => 'Actions',
        'edit' => 'Edit',
        'rename' => 'Rename',
        'hide' => 'Hide',
        'publish' => 'Publish',
        'copy' => 'Copy',
        'delete' => 'Delete',
        'order' => 'Sorting',
        'add' => 'Add new',
        'folder' => 'folder',
        'file' => 'file',
        'public' => 'public',
        'hidden' => 'hidden',
        'invalid-dir' => 'There are errors in order of files from this directory. User defined sort will not work correctly!'
    ),
    'filter' => array(
        'search-type' => 'Search type',
        'root-dir' => 'Root directory',
        'depth' => 'Max depth',
        'file-type' => 'File Types',
        'hidden' => 'Display hidden pages',
        'submit' => 'Filter pages',
        'help' => array(            
            'search-type' => 'Determines way how pages are printed',
            'root-dir' => 'Directory where searching starts',
            'depth' => 'How many levels of subfolders will be searched',
            'file-type' => 'Printed file types. If you choose file then only files from root directory will be printed',
            'hidden' => 'Display hidden pages. Hidden pages cannot be accessed from public area. If you want to hide all pages then set prefix of hidden files to empty string in Settings',
        ),
    ),
    'bulk' => array(
        'all' => 'Select / Deselect All Pages',
        'action' => 'Action',
        'submit' => 'Execute action on selected pages',
        'help' => array(
            'all' => '(De)Selects all pages from table. You can filter page via filtering and then selects pages.',
            'action' => 'Selected action',
        ),
        'confirm' => 'Are you sure? Do you want to execute action on selected items?',
        'selected-pages' => 'Selected pages',
        'go-back' => 'Go back to list of all pages',
        'page' => 'Page',
        'result' => 'Result',
        'errors' => 'Errors',
    ),
);
# File
$lang['file'] = array(
    'info' => array(
        'header' => 'Page information',
        'visibility' => 'Page visibility',
        'breadcrumb' => 'Breadcrumb navigation',
        'neighbours' => 'Neighbours',
        'childs' => 'Childs',
        'file-info' => array(
            'name' => 'Name',
            'type' => 'Type',
            'visibility' => 'File visibility',
            'size' => 'Content Size',
            'date-mod' => 'Date of Last Modification',
            'date-cr' => 'Date of creation',
            'dir-index' => 'user defined folder page',
        ),
        'help' => array(
            'visibility' => 'Hidden if at least one parent folder is hidden',
            'breadcrumb' => 'From mainpage to current page',
            'neighbours' => 'Neighbours',
            'childs' => 'Current page is printed too',
        ),
    ),
    'edit' => array(
        'header' => 'Edit content',
        'dir-index' => 'If you edit and save this page, it will be saved as index page in the folder.',
        'submit' => 'Save page',
        'save-session-expire' => 'You are not logged in. Your session probably expired. You don\'t have to close this page. Just login in other window and then you will able to save this page',
        'editors' => 'Editor selection',
        'backlink' => 'Back to',
        'wysihtml5' => array(
            'strong' => 'Bold',
            'em' => 'Italic',
            'a' => 'Insert link',
            'img' => 'Insert image',
            'h1' => 'H1',
            'h2' => 'H2',
            'ul' => 'Unordered List',
            'ol' => 'Ordered List',
            'speech' => 'Speech',            
            'html' => 'Switch to HTML View',
            'link' => 'Link',
            'image' => 'Image',
            'cancel' => 'Cancel',
        ),
    ),
    'rename' => array(
        'header' => 'Rename',
        'name' => 'New name',
        'help' => 'It must be unique in folder',
        'submit' => 'Rename page',
    ),
    'order' => array(
        'header' => 'Change order',
        'current' => 'Current order',
        'missing' => 'Missing/deleted files in order',
        'submit' => 'Save order',
    ),
    'move' => array(
        'header' => 'Move to different location',
        'new' => 'New folder',
        'help' => 'Page will be deleted from current folder',
        'submit' => 'Move page',
    ),
    'hidden' => array(
        'header' => 'Change public visibility',
        'visibility' => 'Public visibility',
        'help' => 'Public visibility: from hidden to public -> hidden prefix will be removed. Otherwise prefix will be added before current name. If parent folder is hidden then page will be hidden even if page\'s status will be public',
        'submit' => 'Change public visibility',
    ),
    'delete' => array(
        'header' => 'Delete',
        'warning' => 'Are you sure? Do you want to delete this file? Look what are you deleting:',
        'submit' => 'Delete page',
    ),
    'copy' => array(
        'header' => 'Copy to different location',
        'new' => 'New folder',
        'help' => 'Page will be copied to new folder but page will not be deleted from current folder',
        'submit' => 'Copy page',
    ),
    'add' => array(
        'header' => 'Add new page',
        'basic-header' => 'Basic information',
        'name' => 'New name',
        'name-help' => 'It must be unique in folder',
        'type' => 'File type',
        'type-help' => 'File of folder',
        'template' => 'File template',
        'template-help' => 'For folder it\'s used for index page',
        'order-header' => 'User defined ordering',
        'order' => 'Directory order',
        'order-no' => 'Don\'t add to directory order',
        'order-top' => 'Add at the top',
        'order-bottom' => 'Add at the bottom',
        'order-help' => 'It\'s not changed if you move/rename/copy/delete/change-visibility of the file/folder',
        'submit' => 'Add new page',
    ),
);
# Themes
$lang['themes'] = array(
    'list' => 'List of available themes',
    'about-themes' => 'Themes determines external appearance of your web. You can use theme\'s templates as a basis for creating new pages (e.g. for news). In every theme there must be files index.php and error.php and directory templates/.',
    'warning' => 'Editing of theme files will be added in version 1.1. Actually there is only printout of files from theme. You can edit files manually in folders user-files/themes/name/ and v public/themes/name/',
);
# Plugins 
$lang['plugins'] = array(    
    'list' => 'List of installed plugins',
);
# Help
$lang['help'] = array(
    'info' => 'You can find tutorials, tips and tricks on official website of system',
    'link-tutorials' => 'Tutorials, tips and tricks',
    'link-forum' => 'Discussion forum',
    'before-bug' => 'Before reporting problem don\'t forget to check page "Check system". You can find there if basic file permissions are OK.',
);
# Settings
$lang['settings'] = array(
    'general' => 'General settings',
    'texts' => 'Default texts',
    'gen' => array(
        'web' => 'Web',
        'address' => 'Address of your web',
        'recommend-address' => 'According current URL your address should be',
        'rewrite' => 'Pretty SEO URLs (rewrite)',
        'theme' => 'Default theme',
        'editor' => 'Default admin editor',
        'authentication' => 'Authentication in admin area',
        'language' => 'Default language',
        'ordering' => 'Ordering of files',
        'comparator' => 'Sort pages by',
        'order' => 'Default order',
        'depth' => 'Default depth in Pages',
        'files' => 'Files',
        'hidden' => 'Prefix of hidden files',
        'user-hidden' => 'Names of completely hidden files',
        'index' => 'Name of index file in folder',
        'extension' => 'Default file extension in folder',
        'help' => array(
            'address' => 'If you insert wrog address then links will not work correctly on your web',
            'rewrite' => 'If link will be in format /my-pages/ or index.php?path=/my-page/. You need apache rewrite to run pretty SEO URL address.',
            'theme' => 'Default theme is used for every page. If you create new page you can use templates from default theme.',
            'editor' => 'Default admin editor which is automatically loaded in administration. But you can switch it in admin area',
            'authentication' => 'Specify how user can login/logout in administration.',
            'language' => 'Default language in admin area',
            'comparator' => 'Default comparator determines how pages will be sorted in the listings.',
            'order' => 'Default order determines order in listing.',
            'depth' => 'Default depth determines how many subfolders will be printed in pages without filtering',
            'hidden' => 'If prefix is empty then access to public area is completely forbidden! But you can edit them in admin area.',
            'user-hidden' => 'Names with extension, which will be considered as hidden, even if they don\'t start with prefix of hidden files. Use semi-colon separator for more files. These file are NOT display in admin area',
            'index' => 'Name of index file in folder (without extension)',
            'extension' => 'Default file extension in main publication folder.',
        ),
    ),
    'text' => array(
        'general' => 'General',
        'main' => 'Title of main page',
        'error' => 'Title of error page',
        'file' => 'File name',
        'folder' => 'Folder name',
        'nav' => 'Navigation',
        'next' => 'Next page',
        'previous' => 'Previous page',
        'content' => 'Content folder',
        'parent' => 'Parent folder',
        'help' => array(
            'main' => 'Title of main page',
            'error' => 'Title of error page',
            'file' => 'Name of page type, if page is file. You don\'t have to use it on your web',
            'folder' => 'Name of page type, if page is folder. You don\'t have to use it on your web',
            'next' => 'Text in the link to next page',
            'previous' => 'Text of the link to previous page',
            'content' => 'Text of the link to content (main page of the web)',
            'parent' => 'Text of the link to parent page (folder)',
        ),
    ),
);
# Check system
$lang['audit'] = array(
    'minimal' => 'Minimal settings',
    'your' => 'Your settings',
    'is-it-ok' => 'Is it OK?',
    'yes' => 'yes',
    'no' => 'no',
    'webroot-link' => 'Link to homepage of your web',
    'php-version' => 'PHP version',
    'permission-viewdir' => 'File permissions on main public folder (editing pages)',
    'permission-userfiles' => 'File permissions on user files (editing config, themes, plugins)',
    'theme-required-files' => 'Required files in selected theme',
);
# Constants
$lang['constants'] = array(
    'comparators' => array(
        Treewec\DirectoryIterators\ComparatorType::NONE => 'None (depends on operating system)',
        Treewec\DirectoryIterators\ComparatorType::USER => 'User defined file',
        Treewec\DirectoryIterators\ComparatorType::RANDOM => 'Random',
        Treewec\DirectoryIterators\ComparatorType::FILENAME => 'Filename',
        Treewec\DirectoryIterators\ComparatorType::FILE_TYPE => 'Filetype',
        Treewec\DirectoryIterators\ComparatorType::MODIFICATION_DATE => 'Modification date',
        Treewec\DirectoryIterators\ComparatorType::CREATION_DATE => 'Creation date',
        Treewec\DirectoryIterators\ComparatorType::ACCESS_DATE => 'Access date',
        Treewec\DirectoryIterators\ComparatorType::SIZE => 'Size',
    ),
    'order' => array(
        Treewec\DirectoryIterators\OrderType::ASCENDING => 'Ascending',
        Treewec\DirectoryIterators\OrderType::DESCENDING => 'Descending',
    ),
    'search-type' => array(
        Treewec\DirectoryIterators\SearchType::BASIC => 'Only files and dirs from directory, not from subfolders',  
        Treewec\DirectoryIterators\SearchType::DEPTH_FIRST => 'Depth first search',  
        Treewec\DirectoryIterators\SearchType::BREADTH_FIRST => 'Breadth first search',
    ),
    'file-type' => array(
        Treewec\FileSystem\FileType::FILE => 'File',
        Treewec\FileSystem\FileType::FOLDER => 'Folder'
    ),
    'file-types-filter' => array(
        'all' => 'All pages',
        Treewec\FileSystem\FileType::FILE => 'Only files',
        Treewec\FileSystem\FileType::FOLDER => 'Only folders'
    ),
    'neighbours-nav' => array(
        TREEWEC_NAV_NEXT => 'Next page',
        TREEWEC_NAV_PREVIOUS => 'Previous page',
        TREEWEC_NAV_CONTENT => 'Content',
        TREEWEC_NAV_PARENT => 'Parent page',
    ),
    'boolean' => array(
        'ALLOWED' => 'Allowed',
        'DENIED' => 'Denied',
    ),
    'editors' => array(
        'plain' => 'Plain text editor',
        'ckeditor' => 'WYSIWYG editor (CKEditor)',
        'contenteditable' => 'Content editable editor (wysihtml5)'
    ),
    'bulk-actions' => array(
        'hide' => 'Hide pages',
        'publish' => 'Publish pages',
        'delete' => 'Delete pages',
    ),
    'authentication' => array(
        'freeaccess' => 'Free access - everybody can access administration without login',
        'onepassword' => 'One password for all',
    ),
);
# Messages - session and form errors
$lang['messages'] = array(
    // Session
    'general-success' => 'Successfull action',
    'settings-general' => 'General settings was changed',
    'settings-texts' => 'Default text settings was changed',
    'file-add' => 'Successfull creation of new page',
    'file-copy' => 'This page was copied to folder "%s"',
    'file-delete' => 'Delete "%s" from current folder',
    'file-edit' => 'Page was modified and sucessfully saved',
    'file-hidden' => 'Public visibility was changed',
    'file-move' => 'Successfull move to folder "%s"',
    'file-order' => 'Ordering of files was changed',
    // Form errors
    'no-change' => 'No change',
    'invalid-input' => 'Input array has invalid structure',
    'save-error' => 'Error during saving file, control page "Check system" if permissions and files/paths are OK',
    'invalid-value' => 'Invalid value in %s',
    'string-length' => 'Length of %s must be from %d to %d',
    'number-range' => 'Number %s must be from %d to %d',
    'filename-invalid' => '%s - invalid filename (illegal characters)',
    'filename-exists' => '%s - already exists',
    'filename-unexisting' => '%s - not exists',
    'directory-invalid' => '%s - invalid directory name (illegal characters)',
    'directory-unexisting' => '%s - directory not exists',
    /// General settings
    'settings-general-url' => 'Invalid URL address',    
    'settings-general-editor' => 'Nonexisting editor',
    'settings-general-prefix' => 'Only ASCII chars can be in prefix of hidden files',
    'settings-general-extension' => 'Dot has to be first letter in extension',
    /// Pages - bulk actions
    'pages-bulk-action' => 'Invalid action',
    'pages-bulk-selection' => 'You have to select at least one page',
    /// Files
    'files-copy-dir' => 'Actually only files can be copied',
    'files-move-subdir' => 'You cannot move directory to its subdirectory',
    'files-order-file' => 'You can modify order only in folder',
    'files-delete-dir' => 'Directory is not empty, you can delete only empty directories (use bulk actions for files)',
    'files-edit-string' => 'Content must be string',
    'files-add-dir' => 'You can add new page only to folder',
    'files-general-main' => 'You cannot edit main publication folder',
    'files-general-write' => 'Current file is not writable',
    'files-general-read' => 'Current file is not readable',
    // Controller errors
    'controller-theme-error' => 'File "%s.php" is not in the selected theme'
);
# Errors
$lang['error'] = array(
    'info' => 'The operation could not be performed because one or more error(s) occurred:',
    'what' => 'What\'s the problem?',
    'solution' => 'Solution',
    'how-report' => 'How to report problem',
    'report' => array(
        'info' => 'If you problem remains (maybe problem with diacritic), you can send description of your problem on Bitbucket or Treewec homepage:',
        'bitbucket' => 'Send issue to Bitbucket',
        'treewec' => 'Visit forum on Treewec homepage',
        'exception' => 'Show exception',
    ),
    'default' => array(
        'name' => 'Unknown Error',
        'what' => 'Nobody knows. Report problem with detailed description what happened',
        'solution' => 'Please report problem and send also text of the exception',
    ),
    '400' => array(
        'name' => 'Invalid URL Address',        
        'what' => 'Address contains illegal characters or invalid values in parameters. Illegal characters are: ?, ", <, >, :, *, |, ^, ;, \, ../, .. and space. You can use only parameter admin, action and editor in allowed modules.',
        'solution' => 'Remove illegal characters or parameters from URL address. Parameters "dir" and "list" are illegal in admin area. Even if they are allowed in Settings.',
    ),
    '403' => array(
        'name' => 'Access Forbidden',
        'what' => 'You cannot view content of this page',
        'solution' => 'You probably don\'t have permissions to access the page. Login!',
    ),
    '404' => array(
        'name' => 'Page Was Not Found',
        'what' => 'The requested page was not found. The page either does not exist, or page was moved or deleted',
        'solution' => 'Return to <a href="./&admin=home">main page</a> or choose existing page in <a href="&admin=pages">Pages</a>',
    ),
);
# Authentication
$lang['authentication'] = array(
    'freeaccess' => array(
        'info' => 'In free access mode you cannot login or logout',
    ),
    'onepassword' => array(
        'password' => 'Password',
        'login-success' => 'Successfull login.',
        'login-fail-password' => 'Invalid password, try it again.',
        'login-fail-logged' => 'You are already logged in.',
        'logout-success' => 'Successfull logout.',
        'logout-failure' => 'You are not logged in. So you cannot be logged out.',
    ),
);
?>