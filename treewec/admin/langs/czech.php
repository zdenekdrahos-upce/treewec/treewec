<?php
# General 
$lang['save'] = 'Uložit';
# Languages
$lang['czech'] = 'Čeština';
$lang['english'] = 'Angličtina';
# Menu
$lang['menu'] = array(
    'display' => 'Z<br />o<br />b<br />r<br />a<br />z<br />i<br />t<br /><br />m<br />e<br />n<br />u<br />',
    'version' => 'Verze',
    'main' => 'Základní navigace',
    'home' => 'Hlavní strana',
    'pages' => 'Všechny stránky',
    'themes' => 'Vzhledy',
    'plugins' => 'Pluginy',
    'help' => 'Nápověda',
    'linkback' => 'Návrat na web',
    'system' => 'Váš systém',
    'settings' => 'Nastavení',
    'audit' => 'Kontrola systému',
    'langs' => 'Jazyky',
    'access' => 'Administrace',
    'login' => 'Přihlásit',
    'logout' => 'Odhlásit',
);
# Home 
$lang['home'] = array(
    'welcome' => 'Vítejte v Treewecu! Treewec je jednoduchý, ale mocný nástroj pro vytváření webových stránek. Může být využíván jako <abbr title="Content Managment System - redakční systém">CMS</abbr>, nástroj pro microstránky nebo pro vytváření webových dokumentací. Vytvořený web si můžete kompletně přizpůsobit prostřednictvím vzhledů.',
    'info' => 'Základní informace o Vašem systému',
    'pages-count' => 'Počet stránek',
    'nav' => 'Stejná navigace se nachází v levém panelu. Levý panel je zobrazen, pokud najedete myší nad tmavý pruh na levé straně, který se nachází na každé stránce.',    
);
# Pages 
$lang['pages'] = array(
    'advanced' => 'Pokročilé akce',
    'filtering' => 'Filtrování',
    'bulk-actions' => 'Hromadné akce',
    'table' => array(
        'pages' => 'Stránky',
        'actions' => 'Akce',
        'edit' => 'Upravit',
        'rename' => 'Přejmenovat',
        'hide' => 'Skrýt',
        'publish' => 'Publikovat',
        'copy' => 'Kopírovat',
        'delete' => 'Smazat',
        'order' => 'Řazení',
        'add' => 'Přidat',
        'folder' => 'složka',
        'file' => 'soubor',
        'public' => 'veřejná',
        'hidden' => 'skrytá',
        'invalid-dir' => 'Existují chyby v řazení složky, takže uživatelské seřazení nebude fungovat korektně"'
    ),
    'filter' => array(
        'search-type' => 'Způsob prohledávání',
        'root-dir' => 'Výchozí adresář',
        'depth' => 'Maximální hloubka',
        'file-type' => 'Typy souborů',
        'hidden' => 'Zobrazit skryté soubory',
        'submit' => 'Filtrovat stránky',
        'help' => array(            
            'search-type' => 'Určuje v jakém pořadí budou stránky vypsány',
            'root-dir' => 'Adresář ve kterém začne prohledávání',
            'depth' => 'Kolik úrovní podadresářů bude prohledáno',
            'file-type' => 'Pokud zvolíte soubor, tak budou vypsány pouze soubory z výchozího adresáře',
            'hidden' => 'Ke skrytým stránkám nemůže návštěvník přistoupit ve veřejné části. Pokud chcete skrýt všechny stránky, tak nastavte předponu skrytých souborů na prázdný řetězec v Nastavení.',
        ),
    ),
    'bulk' => array(
        'all' => 'Zvolit všechny / žádné stránky',
        'action' => 'Akce',
        'submit' => 'Provést akci u zvolených stránek',
        'help' => array(
            'all' => 'Za/odškstrne všechny stránky z výpisu. Můžete nejdřív filtrovat stránky a až poté si zvolit stránky',
            'action' => 'Zvolená akce',
        ),
        'confirm' => 'Jste si jisti? Opravdu chcete provést akci na těchto stránkách?',
        'selected-pages' => 'Zvolené stránky',
        'go-back' => 'Jít zpět na seznam stránek',
        'page' => 'Stránka',
        'result' => 'Výsledek',
        'errors' => 'Chyby',
    ),
);
# File
$lang['file'] = array(
    'info' => array(
        'header' => 'Informace o stránce',
        'visibility' => 'Viditelnost stránky',
        'breadcrumb' => 'Drobečková navigace',
        'neighbours' => 'Sousední stránky',
        'childs' => 'Podstránky',
        'file-info' => array(
            'name' => 'Jméno',
            'type' => 'Typ',
            'visibility' => 'Viditelnost souboru',
            'size' => 'Velikost obsahu',
            'date-mod' => 'Datum poslední modifikace',
            'date-cr' => 'Datum vytvoření',
            'dir-index' => 'uživatelsky definovaná stránka',
        ),
        'help' => array(
            'visibility' => 'Skrytá, pokud je alespoň jedna nadřazená stránka skrytá',
            'breadcrumb' => 'Cesta od hlavní stránky až k aktuální stránce',
            'neighbours' => 'Sousední stránky',
            'childs' => 'Aktuální stránka je vypsána také',
        ),
    ),
    'edit' => array(
        'header' => 'Upravit obsah',
        'dir-index' => 'Pokud upravíte a uložíte tuto stránku, tak bude použita jako uživatelsky definovaná stránka v adresáři',
        'submit' => 'Uložit stránku',
        'save-session-expire' => 'Nejste přihlášeni. Pravděpodobně vypršela platnost vaší session. Aktuální stránku nemusíte opuštět, takže neztratíte provedené změny. Prostě se přihlašte v novém okně/panelu a následně budete moci stránku opět uložit.',
        'editors' => 'Výběr editoru',
        'backlink' => 'Návrat do ',
        'wysihtml5' => array(
            'strong' => 'Tučně',
            'em' => 'Kurzíva',
            'a' => 'Vložit odkaz',
            'img' => 'Vložit obrázek',
            'h1' => 'H1',
            'h2' => 'H2',
            'ul' => 'Odrážkový seznam',
            'ol' => 'Číslovaný seznam',
            'speech' => 'Řeč',            
            'html' => 'Přepnout do html',
            'link' => 'Odkaz',
            'image' => 'Obrázek',
            'cancel' => 'Zrušit',
        ),
    ),
    'rename' => array(
        'header' => 'Přejmenovat',
        'name' => 'Nové jméno',
        'help' => 'Musí být unikátní v rámci složky',
        'submit' => 'Přejmenovat stránku',
    ),
    'order' => array(
        'header' => 'Změnit řazení ve výpisu',
        'current' => 'Aktuální pořadí',
        'missing' => 'Chybějící stránky nebo stránky určené k odstranění',
        'submit' => 'Uložit pořadí',
    ),
    'move' => array(
        'header' => 'Přesunout do jiné složky',
        'new' => 'Nový adresář',
        'help' => 'Stránka bude z aktuálního adresáře vymazána',
        'submit' => 'Přesunout stránku',
    ),
    'hidden' => array(
        'header' => 'Změnit viditelnost',
        'visibility' => 'Viditelnost stránky',
        'help' => 'Změna na skryté znamená připojení předpony skrytých souborů před jméno. V případě publikování stránky bude předpona odstraněna. Pokud je nadřazená složka skrytá, tak soubor nebude přístupný, i když bude veřejný.',
        'submit' => 'Změnit veřejnou viditelnost',
    ),
    'delete' => array(
        'header' => 'Smazat',
        'warning' => 'Jste si jisti? Opravdu chcete smazat tuto stránku? Podívejte se, co všechno mažate:',
        'submit' => 'Smazat stránku',
    ),
    'copy' => array(
        'header' => 'Zkopírovat do jiné složky',
        'new' => 'Nový adresář',
        'help' => 'Stránka bude zkopírována do nového umístění, ale nebude odstraněna z aktuální složky',
        'submit' => 'Kopírovat stránku',
    ),
    'add' => array(
        'header' => 'Přidat novou stránku',   
        'basic-header' => 'Základní informace o stránce',
        'name' => 'Jméno',
        'name-help' => 'Musí být unikátní v rámci složky',
        'type' => 'Typ stránky',
        'type-help' => 'Soubor nebo složka',
        'template' => 'Šablona stránky',
        'template-help' => 'U složky bude použita pro index ze složky',
        'order-header' => 'Uživatelské řazení',
        'order' => 'Pořadí v rámci složky',
        'order-no' => 'Nepřidávat do řazení složky',
        'order-top' => 'Přidat na začátek řazení',
        'order-bottom' => 'Přidat na konec',
        'order-help' => 'Pořadí není změněno, pokud soubor bude přejmenován, přesunut apod. ',
        'submit' => 'Přidat novou stránku',
    ),
);
# Themes
$lang['themes'] = array(
    'list' => 'Seznam dostupných vzhledů',
    'about-themes' => 'Vzhledy určují celkový vzhled webu, zatímco šablony stránek můžete využívat jako základ při vytváření stránek (vhodné např. u novinek). V každém vzhledu musí být povinně soubory index.php a error.php a složka templates/.',
    'warning' => 'Ve verzi 1.1 bude doplněna možnost editace souborů vzhledu. V současnosti se zobrazí pouze výpis souborů vzhledu. Soubory můžete ale ručně upravit ve složkách v user-files/themes/jméno/ a v public/themes/jméno/',    
);
# Plugins 
$lang['plugins'] = array(    
    'list' => 'Seznam používaných pluginů',
);
# Help
$lang['help'] = array(
    'info' => 'Návody, tipy a triky naleznete na oficiálním webu systému',
    'link-tutorials' => 'Návody, tipy a triky',
    'link-forum' => 'Diskuzní fórum',
    'before-bug' => 'Před nahlášením problému nezapomeňte zkontrolovat stránku "Kontrola systému", zda jsou všechna základní práva a nastavení v pořádku.',
);
# Settings
$lang['settings'] = array(
    'general' => 'Obecné nastavení',
    'texts' => 'Výchozí texty',
    'gen' => array(
        'web' => 'Web',
        'address' => 'Adresa vašeho webu',
        'recommend-address' => 'Podle současné URL adresy by adresa měla být',
        'rewrite' => 'Hezké URL adresy (rewrite)',
        'theme' => 'Výchozí vzhled',
        'editor' => 'Výchozí editor v administraci',
        'authentication' => 'Autentizace v administraci',
        'language' => 'Výchozí jazyk',
        'ordering' => 'Řazení souborů',
        'comparator' => 'Řadit stránky podle',
        'order' => 'Výchozí pořadí výpisu',
        'depth' => 'Výchozí hloubka ve všech stránkách ',
        'files' => 'Soubory',
        'hidden' => 'Předpona skrytých souborů',
        'user-hidden' => 'Jména úplně skrytých souborů',
        'index' => 'Jméno indexového souboru v adresáři',
        'extension' => 'Výchozí přípona souborů',
        'help' => array(
            'address' => 'Pokud bude zadaná neplatná adresa, tak nebudou na webu správně fungovat odkazy',
            'rewrite' => 'Zda se bude požívat adresa /moje-stranka/ nebo index.php?path=/moje-stranka/. V případě povolení pěkných URL adres je nutno mít k dispozici zapnutý rewrite webserveru Apache.',
            'theme' => 'Výchozí vzhled je používán při zobrazení každé stránky, i těch chybových. Při vytváření můžete využívat šablony z výchozího vzhledu.',
            'editor' => 'Editor, který automaticky naběhne při editaci stránek. Při editaci můžete editor libovolně změnit.',            
            'authentication' => 'Určuje, jakým způsobem se uživatel přihlásí do administrace',
            'language' => 'Výchozí jazyk v administraci',
            'comparator' => 'Výchozí způsob porovnávání určuje, v jakém pořadí budou řazeny všechny stránky na webu ve výpisech.',
            'order' => 'Určuje pořadí výpisu stránek v seznamech',
            'depth' => 'Určuje, kolik bude vypsáno podadresářů, na stránce Všechny stránky bez použití filtrování',
            'hidden' => 'Pokud je předpona nevyplněna, tak jsou všechny stránky nepřístupné z hlavního webu a lze k nim přistoupit pouze v administraci.',
            'user-hidden' => 'Jméno souboru s příponou, které budou považovány za skryté i pokud nezačínají na předponu skrytých. Více jmen oddělte středníkem. Tyto soubory nebudou zobrazeny ani v administraci!',
            'index' => 'Jméno souboru ze složky (bez přípony), který bude používán při zobrazení složky.',
            'extension' => 'Přípona souborů v hlavním adresáři. Je nutná proto, aby tato přípona nebyla zobrazena v URL adrese.',
        ),
    ),
    'text' => array(
        'general' => 'Obecné',
        'main' => 'Nadpis hlavní stránky',
        'error' => 'Nadpis chybové stránky',
        'file' => 'Soubor',
        'folder' => 'Adresář',
        'nav' => 'Navigace',
        'next' => 'Následující stránka',
        'previous' => 'Předchozí stránka',
        'content' => 'Hlavní obsah',
        'parent' => 'Nadřazená stránka',
        'help' => array(
            'main' => 'Nadpis hlavní stránky',
            'error' => 'Nadpis u chybových stránek',
            'file' => 'Jméno typu stránky, která je ve skutečnosti souborem. Nemusíte ho na webu vůbec používat.',
            'folder' => 'Jméno typu stránky, která je adresářem. Nemusíte ho na webu vůbec používat.',
            'next' => 'Text u následující stránky při pohybu mezi stránkami',
            'previous' => 'Text u předchozí stránky při pohybu mezi stránkami',
            'content' => 'Text u odkazu na hlavní obsah, tj. hlavní stránku webu',
            'parent' => 'Text u odkazu na nadřazenou/rodičovskou složku',
        ),
    ),
);
# Check system
$lang['audit'] = array(
    'minimal' => 'Minimální nastavení',
    'your' => 'Vaše nastavení',
    'is-it-ok' => 'Je to v pořádku?',
    'yes' => 'ano',
    'no' => 'ne',
    'webroot-link' => 'Adresa domovské stránky vašeho webu',
    'php-version' => 'PHP verze',
    'permission-viewdir' => 'Souborová práva u hlavního publikačního adresáře (editace stránek)',
    'permission-userfiles' => 'Souborová práva u adresáře s uživatelskými soubory (editace nastavení, vzhledů a pluginů)',
    'theme-required-files' => 'Povinné soubory ve zvoleném vzhledu',
);
# Constants
$lang['constants'] = array(
    'comparators' => array(
        Treewec\DirectoryIterators\ComparatorType::NONE => 'Žádné (pořadí závisí na operačním systému)',
        Treewec\DirectoryIterators\ComparatorType::USER => 'Uživatelského souboru',
        Treewec\DirectoryIterators\ComparatorType::RANDOM => 'Náhodně',
        Treewec\DirectoryIterators\ComparatorType::FILENAME => 'Jména',
        Treewec\DirectoryIterators\ComparatorType::FILE_TYPE => 'Typu souboru',
        Treewec\DirectoryIterators\ComparatorType::MODIFICATION_DATE => 'Data poslední úpravy souboru',
        Treewec\DirectoryIterators\ComparatorType::CREATION_DATE => 'Data vytvoření',
        Treewec\DirectoryIterators\ComparatorType::ACCESS_DATE => 'Data posledního přístupu',
        Treewec\DirectoryIterators\ComparatorType::SIZE => 'Velikosti',
    ),
    'order' => array(
        Treewec\DirectoryIterators\OrderType::ASCENDING => 'Vzestupně',
        Treewec\DirectoryIterators\OrderType::DESCENDING => 'Sestupně',
    ),
    'search-type' => array(
        Treewec\DirectoryIterators\SearchType::BASIC => 'Základní výpis souborů a složek, bez podadresářů',  
        Treewec\DirectoryIterators\SearchType::DEPTH_FIRST => 'Prohlídka do hloubky',  
        Treewec\DirectoryIterators\SearchType::BREADTH_FIRST => 'Prohlídka do šířky',
    ),
    'file-type' => array(
        Treewec\FileSystem\FileType::FILE => 'Soubor',
        Treewec\FileSystem\FileType::FOLDER => 'Složka'
    ),
    'file-types-filter' => array(
        'all' => 'Všechny stránky',
        Treewec\FileSystem\FileType::FILE => 'Pouze soubory',
        Treewec\FileSystem\FileType::FOLDER => 'Pouze složky'
    ),
    'neighbours-nav' => array(
        TREEWEC_NAV_NEXT => 'Následující stránka',
        TREEWEC_NAV_PREVIOUS => 'Předchozí stránka',
        TREEWEC_NAV_CONTENT => 'Obsah',
        TREEWEC_NAV_PARENT => 'Rodičovská stránka',
    ),
    'boolean' => array(
        'ALLOWED' => 'Povoleno',
        'DENIED' => 'Zakázáno',
    ),
    'editors' => array(
        'plain' => 'Obyčejný textový editor',
        'ckeditor' => 'WYSIWYG editor (CKEditor)',
        'contenteditable' => 'Editace přímo na stránce (wysihtml5)'
    ),
    'bulk-actions' => array(
        'hide' => 'Skrýt stránky',
        'publish' => 'Publikovat stránky',
        'delete' => 'Smazat stránky',
    ),
    'authentication' => array(
        'freeaccess' => 'Volný přístup - každý může přistoupit do administrace bez přihlášení',
        'onepassword' => 'Jedno heslo pro všechny',
    ),
);
# Messages - session and form errors
$lang['messages'] = array(
    // Session
    'general-success' => 'Úspěšná akce',
    'settings-general' => 'Obecné nastavení bylo uloženo',
    'settings-texts' => 'Výchozí texty byly pozměněny',
    'file-add' => 'Úspěšné vytvoření nové stránky',
    'file-copy' => 'Stránka byla zkopírována do složky "%s"',
    'file-delete' => 'Úspěšné smazání stránky "%s" z aktuální složky',
    'file-edit' => 'Stránka byla upravena a úspěšně uložena',
    'file-hidden' => 'Veřejná viditelnost byla změněna',
    'file-move' => 'Soubor byl přesunut do složky "%s"',
    'file-order' => 'Pořadí souborů bylo změněno',
    // Form errors
    'no-change' => 'Žádná změna',
    'invalid-input' => 'Vstupní pole nemá odpovídající strukturu',
    'save-error' => 'Chyba během ukládání stránky, zkontrolujte stránku "Kontrola systému", zda jsou platná oprávnění k souborům',
    'invalid-value' => 'Neplatná hodnota v "%s"',
    'string-length' => 'Délka "%s" musí být od %d do %d',
    'number-range' => 'Číslo "%s" musí být od %d do %d',
    'filename-invalid' => '%s - neplatné jméno souboru (obsahuje zakázané znaky)',
    'filename-exists' => '%s - už existuje',
    'filename-unexisting' => '%s - neexistuje',
    'directory-invalid' => '%s - neplatné jméno adresáře (obsahuje zakázané znaky)',
    'directory-unexisting' => '%s - adresář neexistuje',
    /// General settings
    'settings-general-url' => 'Neplatná URL adresa',    
    'settings-general-editor' => 'Neexistující editor',
    'settings-general-prefix' => 'Předpona skrytých souborů musí obsahovat pouze základní ASCII znaky (bez háčků a čátek)',
    'settings-general-extension' => 'Přípona souborů musí začínat tečkou',
    /// Pages - bulk actions
    'pages-bulk-action' => 'Neplatná akce',
    'pages-bulk-selection' => 'Musíte zvolit alespoň jednu stránku',
    /// Files
    'files-copy-dir' => 'V současnosti mohou být kopírovány pouze soubory',
    'files-move-subdir' => 'Nemůžete přesunout složku do vlastní podsložky',
    'files-order-file' => 'Pořadí můžete měnit pouze ve složkách',
    'files-delete-dir' => 'Složka není prázdná, můžete smazat pouze prázdnou složku (na soubory použijte hromadné akce)',
    'files-edit-string' => 'Obsah musí být textový řetězec',
    'files-add-dir' => 'Novou stránku můžete přidat pouze do složky',
    'files-general-main' => 'Nemůžete upravovat hlavní publikační složku',
    'files-general-write' => 'Do aktuálního souboru nelze zapisovat',
    'files-general-read' => 'Aktuální soubor není ke čtení',
    // Controller errors
    'controller-theme-error' => 'Soubor "%s.php" neni ve zvolenem vzhledu.'
);
# Errors
$lang['error'] = array(
    'info' => 'Operaci nelze provést, protože se vyskytly následující chyby:',
    'what' => 'Co je za problém?',
    'solution' => 'Řešení problému',
    'how-report' => 'Jak oznámit problém',
    'report' => array(
        'info' => 'Pokud Váš problém přetrvává (možná kvůli diakritice), můžete odeslat popis problému i s výjimkou na Bitbucket nebo na oficiální stránky systému Treewec:',
        'bitbucket' => 'Odeslat zprávu na Bitbucket',
        'treewec' => 'Navštívit fórum na Treewec',
        'exception' => 'Zobrazit text výjimky',
    ),
    'default' => array(
        'name' => 'Neznámá chyba',
        'what' => 'Nikdo neví, co se děje',
        'solution' => 'Nahlaste prosím chybu s detailním popisem, co se stalo. Připojte také text výjimky',
    ),
    '400' => array(
        'name' => 'Neplatná URL adresa',        
        'what' => 'Adresa obsahuje zakázané znaky nebo neplatné hodnoty v parametrech. Neplatné znaky jsou: ?, ", <, >, :, *, |, ^, ;, \, ../, .. a mezera. Z parametrů je dovoleno používat pouze parametry admin, action a editor (a to pouze v povolených modulech)',
        'solution' => 'Odstraňte neplatné znaky nebo parametry z adresy. Parametry "dir" a "list" nejsou povoleny v administraci, i když jsou povoleny v nastavení (to se týká veřejné části)',
    ),
    '403' => array(
        'name' => 'Přístup zakázán',
        'what' => 'Nemůžete zobrazit obsah této stránky',
        'solution' => 'Pravděpodobně nemáte dostatečná oprávnění k přístupu na tuto stránku. Přihlaste se!',
    ),
    '404' => array(
        'name' => 'Stránka nebyla nalezena',
        'what' => 'Požadovaná stránka nebyla nalezena, pravděpodobně neexistuje, mohla být např. přesunuta nebo odsraněna',
        'solution' => 'Vraťte se na <a href="./&admin=home">hlavní stránku</a> nebo zvolte jinou stránku z výpisu všech stránek na <a href="&admin=pages">Všechny stránky</a>',
    ),
);
# Authentication
$lang['authentication'] = array(
    'freeaccess' => array(
        'info' => 'Při povoleném přístupu všem se nemůžete přihlásit ani odhlásit.',
    ),
    'onepassword' => array(
        'password' => 'Heslo',
        'login-success' => 'Úspěšné přihlášení.',
        'login-fail-password' => 'Špatné heslo, zkuste to znovu.',
        'login-fail-logged' => 'Už jste přihlášeni.',
        'logout-success' => 'Byli jste úspěšně odhlášeni.',
        'logout-failure' => 'Nebyli jste odhlášeni, protože nejste přihlášeni.',
    ),
);
?>