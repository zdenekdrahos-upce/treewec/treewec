<?php
$message = 'logout-failure';
if (isset($_SESSION['treewec_logged'])) {
    unset($_SESSION['treewec_logged']);
    $message = 'logout-success';
}
?>
            <p><?php echo \Translator::get('authentication', 'onepassword', $message); ?></p>