<?php
$formProcessor = new \Treewec\Admin\Third\FormProcessor(false);
if (isset($_POST['submit']) && isset($_POST['password'])) {
    $formProcessor->check_condition(!isset($_SESSION['treewec_logged']), Translator::get('authentication', 'onepassword', 'login-fail-logged'));
    $formProcessor->check_condition(Treewec\Admin\Authentication\OnePassword::isPasswordValid($_POST['password']), Translator::get('authentication', 'onepassword', 'login-fail-password'));
    if ($formProcessor->is_valid()) {
        $_SESSION['treewec_logged'] = true;
    }
} else {
    $formProcessor->init_vars(array('password'));
}
?>

            <?php if ($formProcessor->is_valid() && isset($_POST['submit'])): ?>

                <h2><?php echo \Translator::get('authentication', 'onepassword', 'login-success'); ?></h2>

            <?php else: ?>

                <?php 
                $errors = $formProcessor->get_errors();
                require_once(TREEWEC_ADMIN_ROOT . 'theme/error-listing.php'); 
                ?>

                <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                    <table class="form">
                        <tr>
                            <th><label for="fpassword"><?php echo \Translator::get('authentication', 'onepassword', 'password'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('password', 'password'); ?></td>
                        </tr>
                    </table>                
                    <input type="submit" name="submit" value="<?php echo \Translator::get('menu', 'login'); ?>" />
                </form>

            <?php endif; ?>