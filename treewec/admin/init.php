<?php
require_once(dirname(__FILE__) . '/../init.php');

// init languages
$GLOBALS['languageSwitch'] = new LanguageSwitch(TREEWEC_LANG_ROOT, TREEWEC_COOKIE_NAME);// because if GET['language'] is set -> to change cookie
\LanguageHelper::redirectToCleanURL();
\Translator::init($GLOBALS['languageSwitch']);
?>