<?php
/*
 * $exception 
 * - instanceof \Treewec\Exceptions\ClientRequestException
 * - code: 400 (invalid URL address), 404 (file not found)
 */
$errorCode = in_array($exception->getCode(), array(400, 403, 404), true) ? (string) $exception->getCode() : 'default';
$event = new \Treewec\Admin\ErrorEvent($errorCode);
\Treewec\Logger::log_event($event, 'admin/errors');
?>

        <h1><?php echo \Translator::get('error', $errorCode, 'name'); ?></h1>
        
        <h2><?php echo \Translator::get('error', 'what'); ?></h2>        
        <p><?php echo \Translator::get('error', $errorCode, 'what'); ?></p>
        
        <h2><?php echo \Translator::get('error', 'solution'); ?></h2>
        <p><?php echo \Translator::get('error', $errorCode, 'solution'); ?></p>

        <h2><?php echo \Translator::get('error', 'how-report'); ?></h2>
        <p><?php echo \Translator::get('error', 'report', 'info'); ?></p>
        <ul>
            <li><a href="https://bitbucket.org/zdenekdrahos/treewec/issues/new" target="_blank"><?php echo \Translator::get('error', 'report', 'bitbucket'); ?></a></li>
            <li><a href="http://www.treewec.g6.cz/forums/" target="_blank"><?php echo \Translator::get('error', 'report', 'treewec'); ?></a></li>
        </ul>
        <h3><a href="javascript:toggleOneDiv('exception', 'toogle');"><?php echo \Translator::get('error', 'report', 'exception'); ?></a></h3>
        <div class="toogle" name="exception">
            <code><?php echo $exception; ?></code>
        </div>