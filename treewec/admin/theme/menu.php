<?php
$menu = array(
    'main' => array('home','pages','themes','plugins','help'),
    'system' => array('settings', 'audit'),
    'access' => array('login', 'logout'),
);

$urlBuilder = Treewec\URLBuilder::getEmptyURLBuilder();
foreach ($menu as $menuTitle => $pages) {
    $newPages = array();
    foreach ($pages as $name) {        
        $urlBuilder->addOrModifyParameter('admin', $name);
        $newPages[$name] = $urlBuilder->build();        
    }
    $menu[$menuTitle] = $newPages;
}
$menu['main']['linkback'] = \Treewec\Admin\AdvancedFunctions::getLinkBackToWebsite();
?>

            <?php foreach($menu as $menuTitle => $pages): ?>
            <strong><?php echo \Translator::get('menu', $menuTitle); ?></strong>
            <ul>
                <?php foreach ($pages as $pageTitle => $link): ?>
                <li><a href="<?php echo $link; ?>"><?php echo \Translator::get('menu', $pageTitle); ?></a></li>
                <?php endforeach; ?>
            </ul>
            <?php endforeach; ?>

            <strong><?php echo \Translator::get('menu', 'langs'); ?></strong>
            <?php \LanguageHelper::printListOfAvailableLanguages();  ?>
