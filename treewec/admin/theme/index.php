<?php $session = Treewec\Admin\Session::getInstance(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <base href="<?php echo TREEWEC_WEB_ROOT . 'admin/'; ?>" />
        <title>TREEWEC | Admin area</title>
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width">
        <link href="<?php echo TREEWEC_WEB_ROOT . 'admin/'; ?>public/css/style.css" type="text/css" rel="stylesheet" media="all" />
        <script src="<?php echo TREEWEC_WEB_ROOT . 'admin/'; ?>public/scripts/jquery.js" type="text/javascript"></script>
        <script src="<?php echo TREEWEC_WEB_ROOT . 'admin/'; ?>public/scripts/wtooltip.min.js" type="text/javascript"></script>
        <link href="<?php echo TREEWEC_WEB_ROOT . 'admin/'; ?>public/images/favicon.ico" rel="shortcut icon" />
        <!--[if lt IE 9]>
        <script src="<?php echo TREEWEC_WEB_ROOT . 'admin/'; ?>public/scripts/html5.js"></script>
        <script type='text/javascript'>alert('Your IE is OLD. Treewec administration will not work very well. Get update or change browser!');</script>        
        <![endif]-->
        <!--[if lt IE 8]><link href="<?php echo TREEWEC_WEB_ROOT . 'admin/'; ?>public/css/ie.css" type="text/css" rel="stylesheet" media="all" /><![endif]-->
        <!--[if lt IE 7]><link href="<?php echo TREEWEC_WEB_ROOT . 'admin/'; ?>public/css/ie6.css" type="text/css" rel="stylesheet" media="all" /><![endif]-->
        <script type="text/javascript">
            $(document).ready(function() {
                $("[title]").wTooltip({
                    className: "wTooltip", style: false
                });
            });
        </script>
        <?php if (isset($editor)): ?>
            <?php $editor->includeRequiredFilesInHTMLHead(); ?>
        <?php endif; ?>
    </head>
    <body>     

        <div id="treewec">
            <figure>
                <?php echo \Translator::get('menu', 'display'); ?>
            </figure>
            <nav>
                <strong>Treewec</strong>
                <em><?php echo \Translator::get('menu', 'version'); ?>: <span><?php echo TREEWEC_VERSION; ?></span></em>            
                <?php include('menu.php'); ?>
            </nav>
        </div>

        <figure id="status" class="<?php echo $session->hasMessage() ? $session->getMessageStatus() : 'hidden'; ?>">
            <?php if ($session->hasMessage()): ?><span><?php echo $session->getMessage(); ?></span><?php endif; ?>
        </figure>

        <div id="content">
            <?php            
            if ($moduleController instanceof Treewec\Admin\AdminModuleController) {
                echo "<h1>{$h1}</h1>\n";
                $moduleController->displayView(); 
            } else {
                $view->display();// if error in URL, path ... (Exception was caught in Controller::loadView())
            }
            ?>
        </div>

    </body>
</html>