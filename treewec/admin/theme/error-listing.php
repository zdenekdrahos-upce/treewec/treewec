            
            <?php if (isset($errors) && is_array($errors) && count($errors) > 0): ?>
            <aside class="errors">
                <h2><?php echo \Translator::get('error', 'info'); ?></h2>
                <ul>
                    <?php foreach ($errors as $error): ?><li><span><?php echo $error; ?></span></li><?php endforeach; ?>            
                </ul>
            </aside>
            <?php endif; ?>   
