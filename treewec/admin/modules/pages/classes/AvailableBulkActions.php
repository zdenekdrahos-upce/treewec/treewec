<?php

/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class AvailableBulkActions {

    private static $availableActions;

    public static function isValidAction($action) {
        foreach (array_keys(self::getAvailableActions()) as $avAction) {
            if ($avAction == $action) {
                return true;
            }
        }
        return false;
    }

    public static function getActionDescription($action) {
        foreach (self::getAvailableActions() as $actionKey => $description) {
            if ($actionKey == $action) {
                return $description;
            }
        }
        return '';
    }

    public static function getAvailableActions() {
        if (is_null(self::$availableActions)) {
            self::loadActions();
        }
        return self::$availableActions;
    }

    private static function loadActions() {
        $actions = array('hide', 'publish', 'delete');        
        self::$availableActions = array();
        foreach ($actions as $action) {
            self::$availableActions[$action] = \Translator::get('constants', 'bulk-actions', $action);
        }
            
    }

}

?>