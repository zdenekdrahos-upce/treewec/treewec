<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class PagesFilteringForm extends SinglePageFormSubmission {

    public function __construct($submitName, $escapeValues) {
        parent::__construct($submitName, $escapeValues);
        parent::setSuccessAction(FormSuccessAction::NOTHING);
    }

    protected function performAction() {
        return true;
    }

    protected function checkFormFields() {
        foreach (get_class_methods('Treewec\Admin\PagesFilteringForm') as $method) {
            if (is_int(strpos($method, 'check')) && $method != 'checkFormFields') {
                $this->$method();
            }
        }
    }

    private function checkSearchType() {
        $this->formTester->testClassConstant('\Treewec\DirectoryIterators\SearchType', $_POST['search_type'], \Translator::get('pages', 'filter', 'search-type'));
    }

    private function checkFileType() {
        if ($_POST['file_type']) {
            $this->formTester->testClassConstant('Treewec\FileSystem\FileType', $_POST['file_type'], \Translator::get('pages', 'filter', 'file-type'));
        }
    }

    private function checkHiddenFiles() {
        $this->formTester->testBoolean($_POST['hidden_files'], \Translator::get('pages', 'filter', 'hidden'));
    }

    private function checkMaxDepth() {
        $this->formTester->testNumber($_POST['max_depth'], array('min' => 1, 'max' => 5), \Translator::get('pages', 'filter', 'depth'));
    }

    private function checkRootDirectory() {
        if ($_POST['root_dir']) {
            $this->formTester->testExistingDirectory($_POST['root_dir'], TREEWEC_VIEW_DIR, \Translator::get('pages', 'filter', 'root-dir'));
        }
    }

    private function checkComparator() {
        $this->formTester->testClassConstant('\Treewec\DirectoryIterators\ComparatorType', $_POST['comparator'], \Translator::get('settings', 'gen', 'comparator'));
    }

    private function checkOrder() {
        $this->formTester->testClassConstant('Treewec\DirectoryIterators\OrderType', $_POST['order'], \Translator::get('settings', 'gen', 'order'));
    }

    protected function checkChange() {
        // change is not checked
    }

    protected function getStructure() {
        return array(
            'search_type' => \Treewec\DirectoryIterators\SearchType::DEPTH_FIRST,
            'max_depth' => 5, 'root_dir' => '', 'file_type' => '', 'hidden_files' => true,
            'comparator' => TREEWEC_DEFAULT_COMPARATOR, 'order' => TREEWEC_DEFAULT_ORDER
        );
    }

}

?>