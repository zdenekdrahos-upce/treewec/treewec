<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class PagesBulkForm extends SinglePageFormSubmission {

    public function __construct($submitName, $escapeValues) {
        parent::__construct($submitName, $escapeValues);
        parent::setSuccessAction(FormSuccessAction::NOTHING);
    }

    protected function performAction() {
        return true;
    }

    protected function checkFormFields() {
        $this->checkAction();
        $this->checkBulkArray();
    }

    private function checkAction() {        
        $this->formProcessor->check_condition(AvailableBulkActions::isValidAction($_POST['action']), Message::get('pages-bulk-action'));
    }

    private function checkBulkArray() {
        $this->formProcessor->check_condition(
                isset($_POST['bulk']) && is_array($_POST['bulk']) && count($_POST['bulk']) > 0, 
                Message::get('pages-bulk-selection')
        );
        if ($this->formProcessor->is_valid()) {
            foreach ($_POST['bulk'] as $selectedFile) {
                $this->checkSelectedFile($selectedFile);
            }
        }
    }

    private function checkSelectedFile($filePath) {
        $extension = \Treewec\FileSystem\Path::getFilenameExtension($filePath);
        $path = TREEWEC_VIEW_DIR . \Treewec\FileSystem\Path::toFileSystem($filePath) . $extension;
        $this->formProcessor->check_condition(file_exists($path), Message::get('filename-unexisting', array($filePath)));
    }
    
    protected function checkChange() {
        // change is not checked
    }
    
    protected function getStructure() {
        return array('action' => '');
    }

}

?>