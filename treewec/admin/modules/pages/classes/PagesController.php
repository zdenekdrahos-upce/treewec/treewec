<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class PagesController extends AdminModuleController {

    private $pageInfo;
    private $bulkForm;

    public function __construct() {
        parent::__construct();
        $this->bulkForm = new PagesBulkForm('submitBulk', true);
        parent::addForm(new PagesFilteringForm('submitFilter', true));
        parent::addForm($this->bulkForm);
    }

    public function addDataToView($args) {
        $this->pageInfo = $args['pageInfo'];
        parent::addDataToView($args);
    }

    public function loadView() {
        parent::loadView();
        if ($this->bulkForm->isValid()) {
            $this->changeViewFileToBulk();
            if (isset($_POST['confirm'])) {
                $this->executeBulkAction();
            }
        } else {
            $this->loadPrintoutLoader();
        }
    }

    private function loadPrintoutLoader() {
        $header = TREEWEC_MAINPAGE;
        $rootDir = '';
        if (isset($_POST['submitFilter']) && $this->getErrorCount() == 0) {
            $header = $_POST['root_dir'];
            $rootDir = \Treewec\FileSystem\Path::toFileSystem($_POST['root_dir']);
            $pl = $this->getPrintoutLoader(TREEWEC_VIEW_DIR . $rootDir, (int) $_POST['max_depth'], (bool) $_POST['hidden_files'], $_POST['file_type']);
            $pl->setComparator(\Treewec\DirectoryIterators\ComparatorFactory::get($_POST['comparator'], $_POST['order']));
        } else {
            if ($this->pageInfo->currentPath && $this->pageInfo->pageType == \Treewec\FileSystem\FileType::FOLDER) {
                $rootDir = \Treewec\FileSystem\Path::toFileSystem($this->pageInfo->currentPath);
                $header = $this->pageInfo->currentPath;
            }
            $pl = $this->getPrintoutLoader(TREEWEC_VIEW_DIR . $rootDir, (int) TREEWEC_DEFAULT_DEPTH_IN_PAGES, true);
        }
        $this->view->addData(array(
            'printoutLoader' => $pl,
            'filterFolder' => $header,
        ));
    }

    private function getPrintoutLoader($rootDir, $maxDepth, $displayHidden, $fileType = false) {
        $printer = new HTMLTablePrinter();
        $printer->setProcessRoot(true);
        $searchType = isset($_POST['search_type']) ? $_POST['search_type'] : \Treewec\DirectoryIterators\SearchType::DEPTH_FIRST;
        return \Treewec\PrintoutLoader::get($printer, $rootDir, $maxDepth, $displayHidden, $fileType, $searchType);
    }

    private function changeViewFileToBulk() {
        $this->view = new \Treewec\View();
        $path = $this->moduleViewFolder . 'bulk.php';
        $this->view->addContent(new \Treewec\FileSystem\File($path));
    }

    private function executeBulkAction() {
        $bulkResults = array();
        $form = $this->getFormForBulk();
        $this->sortFilesAccordingDepth();
        foreach ($_POST['bulk'] as $name) {
            try {
                $file = \Treewec\FileSystem\FileFactory::build($name);
                $form->setFilePath($file->getPath());
                $form->resetForm();
                $form->process();
                $bulkResults[] = $this->getResult($form, $name);
            } catch (\Exception $e) {
                $bulkResults[] = array('name' => $name, 'result' => 'no', 'errors' => $e->getMessage());
            }
        }
        $this->view->addData(array('bulkResults' => $bulkResults));
    }

    private function sortFilesAccordingDepth() {
        uasort($_POST['bulk'], array('\Treewec\Utils\Comparators', 'compareNumberOfSlashesInString'));
    }

    private function getResult($form, $name) {
        $item = array();
        $item['name'] = $name ? $name : TREEWEC_MAINPAGE;
        $item['result'] = $form->isValid() ? 'yes' : 'no';
        $item['errors'] = implode('<br />', $form->getFormErrors());
        return $item;
    }

    /** @return \Treewec\Admin\FileAbstractForm */
    private function getFormForBulk() {
        $form = FileFormFactory::getInstanceFromPOST('submitBulk');
        $form->setSuccessAction(FormSuccessAction::NOTHING);
        if ($form instanceof FileHiddenForm) {
            $_POST['visibility'] = $_POST['action'] == 'publish' ? '1' : '0';
        }
        return $form;
    }

}

?>