
            <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

                <table class="form">
                    <tr>
                        <th><label for="froot_dir"><?php echo \Translator::get('pages', 'filter', 'root-dir'); ?></label></th>
                        <td><?php 
                        $printoutLoader->setDirectoryPath(TREEWEC_VIEW_DIR);
                        $printoutLoader->setSearchType(\Treewec\DirectoryIterators\SearchType::DEPTH_FIRST);
                        $printoutLoader->setMaxDepth(5);
                        $printoutLoader->setDisplayHiddenFiles(true);
                        $printoutLoader->setFileTypeFilter(Treewec\FileSystem\FileType::FOLDER);
                        $printoutLoader->setPrinter(new \Treewec\Admin\HTMLFormSelect('root_dir'), true);
                        $printoutLoader->display();
                        ?></td>
                        <td class="help" title="<?php echo \Translator::get('pages', 'filter', 'help', 'root-dir'); ?>"></td>
                    </tr>
                    <tr>
                        <th><label for="fsearch_type"><?php echo \Translator::get('pages', 'filter', 'search-type'); ?></label></th>
                        <td><?php
                            $items = \Treewec\Admin\AdvancedFunctions::getConstantsWithTranslatedTexts('\Treewec\DirectoryIterators\SearchType', 'search-type');
                            \Treewec\Admin\FormHTMLElements::select('search_type', $items); 
                        ?></td>
                        <td class="help" title="<?php echo \Translator::get('pages', 'filter', 'help', 'search-type'); ?>"></td>
                    </tr>
                    <tr>
                        <th><label for="fmax_depth"><?php echo \Translator::get('pages', 'filter', 'depth'); ?></label></th>
                        <td><?php \Treewec\Admin\FormHTMLElements::selectNumbers('max_depth',1,5); ?></td>
                        <td class="help" title="<?php echo \Translator::get('pages', 'filter', 'help', 'depth'); ?>"></td>
                    </tr>
                    <tr>
                        <th><label for="ffile_type"><?php echo \Translator::get('pages', 'filter', 'file-type'); ?></label></th>
                        <td><?php
                            $itemsAll = array('' => Translator::get('constants', 'file-types-filter', 'all'));
                            $itemsDefault = \Treewec\Admin\AdvancedFunctions::getConstantsWithTranslatedTexts('\Treewec\FileSystem\FileType', 'file-types-filter');
                            $items = array_merge($itemsAll, $itemsDefault);
                            \Treewec\Admin\FormHTMLElements::select('file_type', $items); 
                        ?></td>
                        <td class="help" title="<?php echo \Translator::get('pages', 'filter', 'help', 'file-type'); ?>"></td>
                    </tr>
                    <tr>
                        <th><label for="fhidden_files"><?php echo \Translator::get('pages', 'filter', 'hidden'); ?></label></th>
                        <td><?php \Treewec\Admin\FormHTMLElements::selectBoolean('hidden_files'); ?></td>
                        <td class="help" title="<?php echo \Translator::get('pages', 'filter', 'help', 'hidden'); ?>"></td>
                    </tr>
                    <tr>
                        <th><label for="fcomparator"><?php echo \Translator::get('settings', 'gen', 'comparator'); ?></label></th>
                        <td><?php 
                            $items = \Treewec\Admin\AdvancedFunctions::getConstantsWithTranslatedTexts('\Treewec\DirectoryIterators\ComparatorType', 'comparators');
                            \Treewec\Admin\FormHTMLElements::select('comparator', $items); 
                        ?></td>
                        <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'comparator'); ?>"></td>
                    </tr>
                    <tr>
                        <th><label for="forder"><?php echo \Translator::get('settings', 'gen', 'order'); ?></label></th>
                        <td><?php
                            $items = \Treewec\Admin\AdvancedFunctions::getConstantsWithTranslatedTexts('\Treewec\DirectoryIterators\OrderType', 'order');
                            \Treewec\Admin\FormHTMLElements::select('order', $items); 
                        ?></td>
                        <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'order'); ?>"></td>
                   </tr>
                        
                </table>
                
                <input type="submit" name="submitFilter" value="<?php echo \Translator::get('pages', 'filter', 'submit'); ?>" />
            </form>