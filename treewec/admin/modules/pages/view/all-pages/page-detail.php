
                <tr>
                    <td>
                        <input form="bulkForm" type="checkbox" name="bulk[]" id="<?php echo $baseLink; ?>" value="<?php echo $baseLink; ?>" />
                    </td>
                    <td class="page">                        
                        <span><?php echo $prefix; ?></span>
                        <?php if ($fileInfo->type == 'folder'): ?>
                        <a href="<?php echo $link; ?>&admin=pages" class="dir"><img src="public/images/<?php echo $iconName; ?>" alt="" /></a>
                        <?php else: ?>
                        <img src="public/images/<?php echo $iconName; ?>" alt="" />
                        <?php endif; ?>
                        <label for="<?php echo $baseLink; ?>"><?php echo $fileInfo->name . "\n"; ?></label>
                    </td>
                    <td>
                        <a href="<?php echo $link; ?>"><?php echo \Translator::get('pages', 'table', 'edit'); ?></a>
                        <a href="<?php echo $link; ?>&action=rename"><?php echo \Translator::get('pages', 'table', 'rename'); ?></a>                            
                        <a href="<?php echo $link; ?>&action=hidden"><?php echo \Translator::get('pages', 'table', $fileInfo->visibility == 'hidden' ? 'publish' : 'hide'); ?></a>
                        <?php if ($fileInfo->type == 'file'): ?>                                       
                        <a href="<?php echo $link; ?>&action=copy"><?php echo \Translator::get('pages', 'table', 'copy'); ?></a>
                        <a href="<?php echo $link; ?>&action=delete"><?php echo \Translator::get('pages', 'table', 'delete'); ?></a>
                        <?php else: ?>
                        <a href="<?php echo $link; ?>&action=order"><?php echo \Translator::get('pages', 'table', 'order'); ?></a>
                        <a href="<?php echo $link; ?>&action=add"><?php echo \Translator::get('pages', 'table', 'add'); ?></a>
                        <?php endif; ?>
                    </td>
                </tr>