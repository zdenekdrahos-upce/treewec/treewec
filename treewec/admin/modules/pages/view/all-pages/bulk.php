
                <form id="bulkForm" method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                    <table class="form">
                        <tr>
                            <th><label for="fselectAll"><?php echo \Translator::get('pages', 'bulk', 'all'); ?></label></th>
                            <td><input id="fselectAll" type="checkbox" onclick="javascript:toggleCheckboxes(this.checked, 'bulk[]')"></td>
                            <td class="help" title="<?php echo \Translator::get('pages', 'bulk', 'help', 'all'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="faction"><?php echo \Translator::get('pages', 'bulk', 'action'); ?></th>
                            <td><?php 
                                $items = Treewec\Admin\AvailableBulkActions::getAvailableActions();
                                \Treewec\Admin\FormHTMLElements::select('action', $items); 
                            ?></td>
                            <td class="help" title="<?php echo \Translator::get('pages', 'bulk', 'help', 'action'); ?>"></td>
                        </tr>
                    </table>                
                    <input type="submit" name="submitBulk" value="<?php echo \Translator::get('pages', 'bulk', 'submit'); ?>" />
                </form>