                        
            <h2><?php echo $filterFolder; ?></h2>
            <?php
            echo \Treewec\Admin\AdvancedFunctions::getBreadcrumbNavForPages($pageInfo);
            $printoutLoader->display();            
            ?>

            <h2><?php echo \Translator::get('pages', 'advanced'); ?></h2>
            <ul class="content">
                <li><a href="javascript:toggleOneDiv('filters', 'toogle2');"><?php echo \Translator::get('pages', 'filtering'); ?></a></li>
                <li><a href="javascript:toggleOneDiv('bulk', 'toogle2');"><?php echo \Translator::get('pages', 'bulk-actions'); ?></a></li> 
            </ul>
            
            <div class="toogle2" name="filters" <?php echo isset($_POST['submitFilter']) ? 'id="displayed"' : ''; ?>>
                <h3><?php echo \Translator::get('pages', 'filtering'); ?></h3>
                <?php require_once(TREEWEC_ADMIN_ROOT . 'theme/error-listing.php'); ?>
                <?php include('all-pages/filtering.php'); ?>
            </div>
            
            <div class="toogle2" name="bulk">
                <h3><?php echo \Translator::get('pages', 'bulk-actions'); ?></h3>
                <?php include('all-pages/bulk.php'); ?>
            </div>