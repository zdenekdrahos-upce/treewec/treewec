
            <?php if(isset($bulkResults)) {
                include('bulk/result.php');
            } else {
                include('bulk/confirm.php');
            }
            ?>

            <?php
            $urlBuilder = Treewec\URLBuilder::getEmptyURLBuilder();
            $urlBuilder->addOrModifyParameter('admin', 'pages');
            ?>
            <h3><a href="<?php echo $urlBuilder->build(); ?>"><?php echo \Translator::get('pages', 'bulk', 'go-back'); ?></a><h3>