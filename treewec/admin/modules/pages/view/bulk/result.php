
            <h2>
                <?php echo \Translator::get('pages', 'bulk', 'action'); ?>: 
                <?php echo Treewec\Admin\AvailableBulkActions::getActionDescription($_POST['action']); ?>
            </h2>

            <table class="default audit">
                <thead>
                    <tr>
                        <th class="leftAlign"><?php echo \Translator::get('pages', 'bulk', 'page'); ?></th>
                        <th><?php echo \Translator::get('pages', 'bulk', 'result'); ?></th>
                        <th><?php echo \Translator::get('pages', 'bulk', 'errors'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($bulkResults as $item): ?>
                    <tr>
                        <td class="leftAlign"><strong><?php echo $item['name']; ?></strong></td>
                        <td class="<?php echo $item['result']; ?>"><?php echo $item['result']; ?></td>
                        <td style="width: 60%;"><?php echo $item['errors']; ?></td>                        
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>