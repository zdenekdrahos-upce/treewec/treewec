    
            <p><?php echo \Translator::get('pages', 'bulk', 'confirm'); ?></p>
            
            <form id="bulkForm" method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                
                <?php
                foreach ($_POST['bulk'] as $name) {
                    echo '<input type="hidden" name="bulk[]" value="' . $name . '" />';
                } 
                ?>
                <input type="hidden" name="action" value="<?php echo $_POST['action']; ?>" /> 
                <input type="hidden" name="confirm" value="true" /> 
                
                <table class="form">
                    <tr>
                        <th><?php echo \Translator::get('pages', 'bulk', 'selected-pages'); ?></th>
                        <td class="leftAlign">
                            <?php echo implode('<br />', $_POST['bulk']); ?>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="faction"><?php echo \Translator::get('pages', 'bulk', 'action'); ?></th>
                        <td class="leftAlign">                            
                            <?php echo Treewec\Admin\AvailableBulkActions::getActionDescription($_POST['action']); ?>
                        </td>
                    </tr>
                </table>                
                <input type="submit" name="submitBulk" value="<?php echo \Translator::get('pages', 'bulk', 'submit'); ?>" />
            </form>