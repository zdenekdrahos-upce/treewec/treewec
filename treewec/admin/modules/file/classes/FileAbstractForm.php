<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

abstract class FileAbstractForm extends SinglePageFormSubmission {

    /** @var \SplFileInfo */
    protected $splFileInfo;

    public function setFilePath($filePath) {
        $this->splFileInfo = new \SplFileInfo($filePath);
    }

    public function resetForm() {
        $this->formProcessor->reset_errors();
    }

    protected function checkFormInput() {
        parent::checkFormInput();
        $this->checkSplFileInfo();
        $this->checkFileIsNotMainPublicationDir();
    }

    protected function rename($newPath) {
        return rename($this->splFileInfo->getPathname(), $newPath);
    }

    protected function getPathInCurrentDirectory($filename) {
        $newPath = $this->getCurrentDirectory() . \Treewec\FileSystem\Path::toFileSystem($filename);
        $newPath .= $this->splFileInfo->isDir() ? '' : TREEWEC_DEFAULT_FILE_EXTENSION;
        return $newPath;
    }

    protected function getPathInOtherDirectory($directory, $rootDir = TREEWEC_VIEW_DIR) {
        $newDir = \Treewec\FileSystem\Path::toFileSystem($directory);
        return $rootDir . $newDir . $this->splFileInfo->getFilename();
    }

    protected function checkNewFile($newFilename, $dirPath, $errorName) {
        $this->formTester->testNewFilename($newFilename, $dirPath, $this->splFileInfo->isDir(), $errorName);
    }

    protected function getCurrentDirectory() {
        return $this->splFileInfo->getPath() . '/';
    }

    protected function getCurrentNameForScreen() {
        $defaultName = $this->splFileInfo ? \Treewec\Utils\Files::getBasename($this->splFileInfo) : '';
        return \Treewec\FileSystem\Path::toScreen($defaultName);
    }

    protected function getSuccessLink($path, $removeAction) {        
        $urlBuilder = new \Treewec\URLBuilder(new \Treewec\Holders\ArrayHolder($_GET));
        $urlBuilder->addOrModifyParameter('path', $path);
        if ($removeAction) {
            $urlBuilder->addOrModifyParameter('action', null);
        }
        return $urlBuilder->build();
    }

    protected function checkSplFileInfo() {
        $isInstanceOf = $this->splFileInfo instanceof \SplFileInfo;
        $this->formProcessor->check_condition($isInstanceOf, 'Invalid SplFileInfo');
        if ($isInstanceOf) {
            $this->formProcessor->check_condition($this->splFileInfo->isWritable(), Message::get('files-general-write'));
            $this->formProcessor->check_condition($this->splFileInfo->isReadable(), Message::get('files-general-read'));
        }
    }

    private function checkFileIsNotMainPublicationDir() {
        $isNotRoot = ($this->splFileInfo->getPathname() . '/') != TREEWEC_VIEW_DIR;
        $this->formProcessor->check_condition($isNotRoot, Message::get('files-general-main'));
    }

}

?>