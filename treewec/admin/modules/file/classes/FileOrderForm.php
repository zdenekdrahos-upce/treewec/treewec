<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FileOrderForm extends FileAbstractForm {

    private $directoryOrder;

    public function __construct($submitName, $escapeValues) {
        parent::__construct($submitName, $escapeValues);
        $this->directoryOrder = new \Treewec\DirectoryIterators\DirectoryOrder();
    }

    public function setFilePath($filePath) {
        parent::setFilePath($filePath);        
        $this->splFileInfo = new \SplFileInfo($filePath);        
        $this->directoryOrder->changeDirectory($this->splFileInfo->getPathname());
    }

    protected function performAction() {
        $this->directoryOrder->changeOrderArray($_POST['order']);        
        return $this->directoryOrder->saveOrder();
    }

    protected function checkFormInput() {        
        SinglePageFormSubmission::checkFormInput();
        FileAbstractForm::checkSplFileInfo();
    }

    protected function checkFormFields() {
        $this->formProcessor->check_condition($this->splFileInfo->isDir(), Message::get('files-order-file'));
        if ($_POST['order']) {
            $this->checkOrderedFiles();
        }
    }

    private function checkOrderedFiles() {
        foreach ($_POST['order'] as $file) {
            $this->formTester->testString($file, array('min' => 1, 'max' => 100), \Translator::get('file', 'file-info', 'name'));
            if (!$this->formProcessor->is_valid()) {
                return;
            }
        }
    }

    protected function checkChange() {
        $new = $_POST['order'] ? implode(',', $_POST['order']) : '';
        $old = implode(',', array_keys($this->directoryOrder->getOrderArray()));
        $this->formProcessor->check_condition($new != $old, Message::get('no-change'));
    }

    protected function getSuccessMessage() {
        return Message::get('file-order');
    }
    
    protected function getStructure() {
        return array('order' => '');
    }

}

?>