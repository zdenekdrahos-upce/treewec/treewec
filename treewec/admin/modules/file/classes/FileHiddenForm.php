<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

// child of FileRenameFOrm because hidden is only rename (add/remove prefix of hidden files to current name)
final class FileHiddenForm extends FileRenameForm {

    protected function checkChange() {
        $_POST['new_name'] = $this->getNewName();
        parent::checkChange();
    }

    protected function checkFormFields() {
        $this->formTester->testBoolean($_POST['visibility'], \Translator::get('file', 'actions', 'ahidden', 'visibility'));
        parent::checkFormFields();
    }

    private function getNewName() {
        if ($this->splFileInfo) {
            $newName = $_POST['visibility'] ? $this->getNewPublicName() : $this->getNewHiddenName();
            return \Treewec\FileSystem\Path::toScreen($newName);
        }
        return '';
    }

    private function getNewHiddenName() {
        return TREEWEC_PREFIX_OF_HIDDEN_FILES . \Treewec\Utils\Files::getBasename($this->splFileInfo);
    }

    private function getNewPublicName() {
        $basename = \Treewec\Utils\Files::getBasename($this->splFileInfo);
        return mb_substr($basename, strlen(TREEWEC_PREFIX_OF_HIDDEN_FILES), mb_strlen($basename, 'UTF-8'), 'UTF-8');
    }

    protected function getSuccessMessage() {
        return Message::get('file-hidden');
    }

    protected function getStructure() {
        $defaultVisibility = $this->splFileInfo ? !\Treewec\FileSystem\Filename::isHidden($this->splFileInfo->getFilename()) : true;        
        return array('visibility' => $defaultVisibility);
    }

}

?>