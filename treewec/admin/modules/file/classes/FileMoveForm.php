<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FileMoveForm extends FileAbstractForm {

    private $rootDirectory = TREEWEC_VIEW_DIR;

    public function setRootDirectory($rootDir) {
        if (is_dir($rootDir)) {
            $this->rootDirectory = $rootDir;
        }
    }

    protected function performAction() {
        $newPath = parent::getPathInOtherDirectory($_POST['new_folder'], $this->rootDirectory);
        return parent::rename($newPath);
    }

    protected function checkFormFields() {
        $filename = \Treewec\FileSystem\Path::toScreen(\Treewec\Utils\Files::getBasename($this->splFileInfo));
        parent::checkNewFile($filename, $this->rootDirectory . \Treewec\FileSystem\Path::toFileSystem($_POST['new_folder']), \Translator::get('file', 'file-info', 'name'));
        if ($_POST['new_folder']) {
            $this->formTester->testExistingDirectory($_POST['new_folder'], $this->rootDirectory, \Translator::get('file', 'actions', 'amove', 'new'));
        }
        $this->checkMoveToSubDirectory();
    }

    private function checkMoveToSubDirectory() {
        $currentPath = $this->splFileInfo->getPathname();        
        $newPath = parent::getPathInOtherDirectory($_POST['new_folder'], $this->rootDirectory);
        $isNotSubDir = is_bool(mb_strpos($newPath, $currentPath, 0, 'UTF-8'));
        $this->formProcessor->check_condition($isNotSubDir, Message::get('files-move-subdir'));
    }

    protected function checkChange() {
        // no change - its checked with file_exists
    }

    protected function loadSuccessLink() {        
        $newPath = $_POST['new_folder'] . $this->getCurrentNameForScreen();
        $newPath .= $this->splFileInfo->isDir() ? '/' : '';
        $this->successLink = parent::getSuccessLink($newPath, false);
    }

    protected function getSuccessMessage() {
        return Message::get('file-move', array($_POST['new_folder']));
    }

    protected function getStructure() {
        return array('new_folder' => '');
    }

}

?>