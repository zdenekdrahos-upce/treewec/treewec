<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FileAddForm extends FileAbstractForm {

    protected function performAction() {
        $this->updateOrderFile();
        $path = $this->splFileInfo->getPathname() . '/' . \Treewec\FileSystem\Path::toFileSystem($_POST['name']);
        if ($_POST['file_type'] == \Treewec\FileSystem\FileType::FOLDER) {
            mkdir($path);
            $path .= '/' . \Treewec\FileSystem\Path::toFileSystem(TREEWEC_NAME_OF_INDEX_FILE);
        }
        return $this->createNewFile($path);
    }

    private function updateOrderFile() {
        if ($_POST['order'] != 'no') {
            $dirOrder = new \Treewec\DirectoryIterators\DirectoryOrder();
            $dirOrder->changeDirectory($this->splFileInfo->getPathname());
            $method = $_POST['order'] == 'top' ? 'addNewAtTop' : 'addNewAtBottom';
            $extension = $_POST['file_type'] == \Treewec\FileSystem\FileType::FILE ? TREEWEC_DEFAULT_FILE_EXTENSION : '';
            $filename = $_POST['name'] . $extension;
            $dirOrder->$method($filename);
            $dirOrder->saveOrder();
        }
    }

    private function createNewFile($path) {
        $templatePath = TREEWEC_THEMES . TREEWEC_DEFAULT_THEME . '/templates/' . $_POST['file_template'] . TREEWEC_DEFAULT_FILE_EXTENSION;
        $this->templater->loadContentFromFile($templatePath);
        $path = $path . TREEWEC_DEFAULT_FILE_EXTENSION;
        file_put_contents($path, '');
        return $this->templater->saveContentToFile($path);
    }

    protected function checkFormInput() {
        // if templates folder is empty
        $_POST['file_template'] = isset($_POST['file_template']) ? $_POST['file_template'] : '';
        SinglePageFormSubmission::checkFormInput();
        FileAbstractForm::checkSplFileInfo();
    }

    protected function checkFormFields() {
        $this->formProcessor->check_condition($this->splFileInfo->isDir(), Message::get('files-add-dir'));
        $this->checkName();
        $this->checkType();
        $this->checkOrder();
        $this->checkFileTemplate();
    }

    private function checkName() {
        $currentDir = $this->splFileInfo->getPathname() . '/';
        $isDir = $_POST['file_type'] == \Treewec\FileSystem\FileType::FOLDER;
        $this->formTester->testNewFilename($_POST['name'], $currentDir, $isDir, \Translator::get('file', 'actions', 'aadd', 'name'));
    }

    private function checkType() {
        $this->formTester->testClassConstant('\Treewec\FileSystem\FileType', $_POST['file_type'], \Translator::get('file', 'actions', 'aadd', 'type'));
    }

    private function checkOrder() {
        $this->formProcessor->check_condition(in_array($_POST['order'], array('no', 'top', 'bottom'), true), Message::get('invalid-value', array(\Translator::get('file', 'actions', 'aadd', 'order'))));
    }

    private function checkFileTemplate() {
        $isTemplate = is_file(TREEWEC_THEMES . TREEWEC_DEFAULT_THEME . '/templates/' . $_POST['file_template'] . TREEWEC_DEFAULT_FILE_EXTENSION);
        $this->formProcessor->check_condition($isTemplate, Message::get('filename-unexisting', array(\Translator::get('file', 'actions', 'aadd', 'template'))));
    }

    protected function loadSuccessLink() {
        $arrayHolder = new \Treewec\Holders\ArrayHolder($_GET);
        $pageInfo = \Treewec\PageInfoLoader::getPageInfo($arrayHolder);
        $extension = $_POST['file_type'] == \Treewec\FileSystem\FileType::FOLDER ? '/' : '';
        $newPath = $pageInfo->currentPath . $_POST['name'] . $extension;
        $this->successLink = parent::getSuccessLink($newPath, true);
    }

    protected function getSuccessMessage() {
        return Message::get('file-add');
    }

    protected function getStructure() {
        return array('name' => '', 'file_type' => '', 'file_template' => '', 'order' => '');
    }

}

?>