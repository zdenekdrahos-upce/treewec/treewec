<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

class FileRenameForm extends FileAbstractForm {

    protected function performAction() {
        $newPath = parent::getPathInCurrentDirectory($_POST['new_name']);
        return parent::rename($newPath);
    }

    protected function checkFormFields() {
        parent::checkNewFile($_POST['new_name'], $this->getCurrentDirectory(), \Translator::get('file', 'actions', 'arename', 'name'));
    }

    protected function loadSuccessLink() {        
        $arrayHolder = new \Treewec\Holders\ArrayHolder($_GET);
        $pageInfo = \Treewec\PageInfoLoader::getPageInfo($arrayHolder);
        $currentName = $this->getCurrentNameForScreen();
        $newPath = str_replace($currentName, $_POST['new_name'], $pageInfo->currentPath);
        $this->successLink = parent::getSuccessLink($newPath, false);
    }

    protected function getStructure() {
        return array('new_name' => parent::getCurrentNameForScreen());
    }

}

?>