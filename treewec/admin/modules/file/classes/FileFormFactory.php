<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FileFormFactory {

    // used in FileController::__construct()
    public static function getInstanceFromGET($submitName) {
        $action = isset($_GET['action']) ? $_GET['action'] : 'edit';
        $escapeValues = isset($_GET['action']) ? true : false;
        return self::getInstance($action, $submitName, $escapeValues);
    }

    // used in PagesController::getFormForBulk() in bulk operations
    public static function getInstanceFromPOST($submitName) {
        $formAction = in_array($_POST['action'], array('hide', 'publish'), true) ? 'hidden' : $_POST['action'];
        return self::getInstance($formAction, $submitName, true);
    }

    private static function getInstance($actionName, $submitName, $escapeValues) {
        $formClass = 'Treewec\Admin\File' . ucfirst($actionName) . 'Form';
        return new $formClass($submitName, $escapeValues);
    }

}

?>