<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FileEditForm extends FileAbstractForm {

    protected function performAction() {        
        $this->templater->loadContentFromString($_POST['editContent']);
        return $this->templater->saveContentToFile($this->splFileInfo->getPathname());
    }

    protected function checkFormFields() {
        $this->formProcessor->check_condition(is_string($_POST['editContent']), Message::get('files-edit-string'));
    }

    protected function checkChange() {        
        $_POST['editContent'] = \Treewec\Utils\Strings::decodePHPInString($_POST['editContent']);
        if ($this->splFileInfo && $this->splFileInfo->isDir()) {
            $this->updateFolderPath();
        }
        parent::checkChange();
    }

    private function updateFolderPath() {
        $folderIndex = $this->splFileInfo->getPathname() . '/' . TREEWEC_NAME_OF_INDEX_FILE . TREEWEC_DEFAULT_FILE_EXTENSION;
        if (!file_exists($folderIndex)) {
            file_put_contents($folderIndex, ''); // file must exists before saving
        }
        $this->splFileInfo = new \SplFileInfo($folderIndex);
    }

    protected function getSuccessMessage() {
        return Message::get('file-edit');
    }

    protected function getStructure() {
        $defaultContent = $this->splFileInfo && $this->splFileInfo->isFile() ? file_get_contents($this->splFileInfo->getPathname()) : '';
        return array('editContent' => $defaultContent);
    }

}

?>