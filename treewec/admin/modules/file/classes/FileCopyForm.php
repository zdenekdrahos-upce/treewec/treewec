<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FileCopyForm extends FileAbstractForm {

    private $rootDirectory = TREEWEC_VIEW_DIR;

    public function setRootDirectory($rootDir) {
        if (is_dir($rootDir)) {
            $this->rootDirectory = $rootDir;
        }
    }

    protected function performAction() {
        $this->templater->loadContentFromFile($this->splFileInfo->getPathname());
        $newPath = parent::getPathInOtherDirectory($_POST['new_folder'], $this->rootDirectory);
        file_put_contents($newPath, '');
        return $this->templater->saveContentToFile($newPath);
    }

    protected function checkFormFields() {
        $this->formProcessor->check_condition(!$this->splFileInfo->isDir(), Message::get('files-copy-dir'));
        $filename = \Treewec\FileSystem\Path::toScreen(\Treewec\Utils\Files::getBasename($this->splFileInfo));
        parent::checkNewFile($filename, $this->rootDirectory . \Treewec\FileSystem\Path::toFileSystem($_POST['new_folder']), \Translator::get('file', 'file-info', 'name'));
        if ($_POST['new_folder']) {
            $this->formTester->testExistingDirectory($_POST['new_folder'], $this->rootDirectory, \Translator::get('file', 'actions', 'acopy', 'new'));
        }
    }

    protected function checkChange() {
        // no change - its checked with file_exists
    }

    protected function getSuccessMessage() {        
        return Message::get('file-copy', array($_POST['new_folder']));
    }

    protected function getStructure() {
        return array('new_folder' => '');
    }

}

?>