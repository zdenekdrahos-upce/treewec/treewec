<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

// TODO: refactoring
final class FileController extends AdminModuleController {

    /** @var string */
    private $filePath;
    /** @var \Treewec\Admin\SinglePageFormSubmission */
    private $form;

    public function __construct() {
        parent::__construct();
        if (!isset($_GET['action']) || $_GET['action'] != 'info') {
            $this->form = FileFormFactory::getInstanceFromGET('submit');
            parent::addForm($this->form);
        }
    }

    public function addDataToView($args) {
        $this->updateProcessing($args['pageInfo']);
        parent::addDataToView($args);
    }

    private function updateProcessing($pageInfo) {        
        $file = \Treewec\FileSystem\FileFactory::build($pageInfo->currentPath);
        $this->filePath = $file->getPath();
        if ($this->form) {
            $this->form->setFilePath($this->filePath);
        }
    }

    public function loadView() {
        parent::loadView();
        $this->loadFileInfo();
        if (isset($_GET['action']) && $_GET['action'] == 'add') {
            $this->checkTemplatesInTheme();
        }
    }

    private function loadFileInfo() {
        $fileInfo = FileInfo::getFromPath($this->filePath);
        $fileInfo->loadFolderContentSize();
        $this->view->addData(array('fileInfo' => $fileInfo));
        if ($fileInfo->type == 'folder') {
            $folderFileInfo = FileInfo::getFromPath($this->filePath . '/' . TREEWEC_NAME_OF_INDEX_FILE . TREEWEC_DEFAULT_FILE_EXTENSION);
            $this->view->addData(array('folderFileInfo' => $folderFileInfo));
        }
    }

    private function checkTemplatesInTheme() {
        if (!file_exists(TREEWEC_THEMES . TREEWEC_DEFAULT_THEME . '/templates/')) {
            \Treewec\Exceptions\ClientRequestException::throwsFileNotFound();
        }
    }

}

?>