<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FileDeleteForm extends FileAbstractForm {

    protected function performAction() {
        $fullPath = $this->splFileInfo->getPathname();
        if ($this->splFileInfo->isDir()) {
            $files = array('dir.order', TREEWEC_NAME_OF_INDEX_FILE . TREEWEC_DEFAULT_FILE_EXTENSION);
            $deleteFiles = $this->deleteFolderFilesIfExists($fullPath, $files);            
            return $deleteFiles && rmdir($fullPath);
        } else {
            return unlink($fullPath);
        }
    }

    private function deleteFolderFilesIfExists($folderPath, $files) {
        $delete = true;
        foreach ($files as $filename) {
            $file = $folderPath . '/' . $filename;
            if (file_exists($file)) {
                $delete = $delete && unlink($file);
            }
        }
        return $delete;
    }

    protected function checkFormFields() {
        if ($this->splFileInfo->isDir()) {
            $fileCounter = new FileCounter();
            $fileCounter->setProcessRoot(false);
            $pl = \Treewec\PrintoutLoader::get($fileCounter, $this->splFileInfo->getPathname(), 1, true, false, \Treewec\DirectoryIterators\SearchType::BASIC);
            $pl->display();
            $counts = $fileCounter->getCounts();
            $this->formProcessor->check_condition($counts['totalCount'] == 0, Message::get('files-delete-dir'));
        }
    }

    protected function checkChange() {
        // no change
    }

    protected function loadSuccessLink() {
        $arrayHolder = new \Treewec\Holders\ArrayHolder($_GET);
        $pageInfo = \Treewec\PageInfoLoader::getPageInfo($arrayHolder);
        $oldName = $this->getCurrentNameForScreen();
        $newPath = mb_substr($pageInfo->currentPath, 0, -mb_strlen($oldName, 'UTF-8'), 'UTF-8');        
        $this->successLink = parent::getSuccessLink($newPath, true);
    }

    protected function getSuccessMessage() {
        return Message::get('file-delete', array($this->getCurrentNameForScreen()));
    }

    protected function getCurrentNameForScreen() {
        $oldName = parent::getCurrentNameForScreen();
        return $oldName . ($this->splFileInfo->isDir() ? '/' : '');
    }

    protected function getStructure() {
        return array();
    }

}

?>