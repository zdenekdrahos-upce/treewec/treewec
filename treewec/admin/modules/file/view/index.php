<?php
/*
 * Original attributes from Treewec\Controller
 * @var Treewec\PageInfo $pageInfo
 * @var Treewec\View $view
 * @var Treewec\URLArgs $urlArguments
 */            
//TODO: refactoring, move to Controller
$urlBuilder = new \Treewec\URLBuilder($arrayHolder);
$urlBuilder->removeParameter('action');
$actions = array('info' => 'info', 'edit' => null);
if ($pageInfo->currentPath) {
    $actions['rename'] = 'rename';
    $actions['delete'] = 'delete';
    $actions['hidden'] = 'hidden';
    $actions['move'] = 'move';
}
if ($pageInfo->pageType == Treewec\FileSystem\FileType::FILE) {    
    $actions['copy'] = 'copy';
} else {
    $actions['order'] = 'order';
    $actions['add'] = 'add';
}
$selectedAction = isset($_GET['action']) ? $_GET['action'] : 'edit';
?>
            
            <nav>
                <ul>
                    <?php 
                    // TODO: same in edit -> function($arrayHolder, $editedParameter, $items)
                    $urlBuilder = new \Treewec\URLBuilder($arrayHolder);
                    $urlBuilder->removeParameter('editor');
                    $urlBuilder->removeParameter('action');
                    foreach($actions as $key => $action) {
                        $selected = \Treewec\HTML\AttributeFactory::getAttributeClass('selected', $key == $selectedAction);
                        $text = \Translator::get('file', $key, 'header');
                        $urlBuilder->addOrModifyParameter('action', $action);
                        echo '<li>' . \Treewec\HTML\Anchor::create($urlBuilder->build(), $text, array($selected)) . '</li>';
                    }
                    ?>
                </ul>
            </nav>

            <?php require_once(TREEWEC_ADMIN_ROOT . 'theme/error-listing.php'); ?>

            <h2><?php echo \Translator::get('file', $selectedAction, 'header'); ?></h2>
            <?php include(TREEWEC_ADMIN_ROOT . 'modules/file/view/actions/' . $selectedAction. '.php'); ?>
