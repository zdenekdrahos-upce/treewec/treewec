
                <table class="default">
                    <tr>
                        <th><?php echo \Translator::get('file', 'info', 'file-info', 'name'); ?></th>
                        <th><?php echo \Translator::get('file', 'info', 'file-info', 'type'); ?></th>
                        <th><?php echo \Translator::get('file', 'info', 'file-info', 'visibility'); ?></th>
                        <th><?php echo \Translator::get('file', 'info', 'file-info', 'size'); ?></th>
                        <th><?php echo \Translator::get('file', 'info', 'file-info', 'date-mod'); ?></th>
                        <th><?php echo \Translator::get('file', 'info', 'file-info', 'date-cr'); ?></th>
                    </tr>
                    <tr>                    
                        <td><strong><?php echo $fileInfo->name; ?><strong></td>                                
                        <td><?php echo \Translator::get('pages', 'table', $fileInfo->type); ?></td>
                        <td><?php echo \Translator::get('pages', 'table', $fileInfo->visibility); ?></td>
                        <td><?php echo $fileInfo->size; ?></td>
                        <td><?php echo $fileInfo->dateModification; ?></td>
                        <td><?php echo $fileInfo->dateCreation; ?></td>
                    </tr>
                    <?php if ($fileInfo->type == 'folder'): ?>
                     <tr>
                         <td colspan="3"><strong><?php echo TREEWEC_NAME_OF_INDEX_FILE; ?></strong> (<?php echo \Translator::get('file', 'info', 'file-info', 'dir-index'); ?>)</td>
                         <td><?php echo $folderFileInfo->size; ?></td>
                         <td><?php echo $folderFileInfo->dateModification; ?></td>
                         <td><?php echo $folderFileInfo->dateCreation; ?></td>
                    </tr>
                     <?php endif; ?>
                </table>


                <table class="form">
                    <tr>
                        <th><?php echo \Translator::get('file', 'info', 'visibility'); ?></th>
                        <td class="leftAlign"><?php echo \Translator::get('pages', 'table', $pageInfo->pageVisibility); ?></td>
                        <td class="help" title="<?php echo \Translator::get('file', 'info', 'help', 'visibility'); ?>"></td>
                    </tr>
                    <tr>
                        <th><?php echo \Translator::get('file', 'info', 'breadcrumb'); ?></th>
                        <td class="leftAlign"><?php echo \Treewec\Admin\AdvancedFunctions::getBreadcrumbNav($pageInfo); ?></td>
                        <td class="help" title="<?php echo \Translator::get('file', 'info', 'help', 'breadcrumb'); ?>"></td>
                    </tr>
                    <tr>
                        <th><?php echo \Translator::get('constants', 'neighbours-nav', TREEWEC_NAV_PARENT); ?></th>
                        <td class="leftAlign"><?php echo \Treewec\Admin\AdvancedFunctions::getPageLink($pageInfo->parentFolder); ?></td>
                        <td class="help" title="<?php echo \Translator::get('file', 'info', 'help', 'neighbours'); ?>"></td>
                    </tr>
                    <tr>
                        <th><?php echo \Translator::get('constants', 'neighbours-nav', TREEWEC_NAV_PREVIOUS); ?></th>
                        <td class="leftAlign"><?php echo \Treewec\Admin\AdvancedFunctions::getPageLink($pageInfo->previousPage); ?></td>
                        <td class="help" title="<?php echo \Translator::get('file', 'info', 'help', 'neighbours'); ?>"></td>
                    </tr>
                    <tr>
                        <th><?php echo \Translator::get('constants', 'neighbours-nav', TREEWEC_NAV_NEXT); ?></th>
                        <td class="leftAlign"><?php echo \Treewec\Admin\AdvancedFunctions::getPageLink($pageInfo->nextPage); ?></td>
                        <td class="help" title="<?php echo \Translator::get('file', 'info', 'help', 'neighbours'); ?>"></td>
                    </tr>
                    <tr>
                        <th><?php echo \Translator::get('file', 'info', 'childs'); ?></th>
                        <td class="leftAlign"><?php $fileInfo->printTree(); ?></td>
                        <td class="help" title="<?php echo \Translator::get('file', 'info', 'help', 'childs'); ?>"></td>
                    </tr>
                </table>
