
            <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

                <fieldset>
                    <legend><?php echo \Translator::get('file', 'add', 'basic-header'); ?></legend>
                    <table class="form">
                        <tr>
                        <th><label for="fname"><?php echo \Translator::get('file', 'add', 'name'); ?></label></th>
                        <td><?php \Treewec\Admin\FormHTMLElements::input('name'); ?></td>
                        <td class="help" title="<?php echo \Translator::get('file', 'add', 'name-help'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="ffile_type"><?php echo \Translator::get('file', 'add', 'type'); ?></label></th>
                            <td><?php 
                                $items = array(
                                    Treewec\FileSystem\FileType::FILE => Translator::get('constants', 'file-type', Treewec\FileSystem\FileType::FILE),
                                    Treewec\FileSystem\FileType::FOLDER => Translator::get('constants', 'file-type', Treewec\FileSystem\FileType::FOLDER),
                                );
                                \Treewec\Admin\FormHTMLElements::select('file_type', $items); 
                            ?></td>
                            <td class="help" title="<?php echo \Translator::get('file', 'add', 'type-help'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="ffile_template"><?php echo \Translator::get('file', 'add', 'template'); ?></label></th>
                            <td><?php 
                            $printer = new \Treewec\Admin\HTMLFormSelect('file_template');
                            $printer->setRootDirectory(TREEWEC_THEMES . TREEWEC_DEFAULT_THEME . '/templates/');
                            $printer->setProcessRoot(false);
                            $printoutLoader = Treewec\PrintoutLoader::get(
                                    $printer, TREEWEC_THEMES . TREEWEC_DEFAULT_THEME . '/templates/', 3, true, false, 
                                    \Treewec\DirectoryIterators\SearchType::DEPTH_FIRST
                            );
                            $printoutLoader->display();
                            ?></td>
                            <td class="help" title="<?php echo \Translator::get('file', 'add', 'template-help'); ?>"></td>
                        </tr>
                    </table>
                </fieldset>

                <fieldset>
                    <legend><?php echo \Translator::get('file', 'add', 'order-header'); ?></legend>
                    <table class="form">
                        <tr>
                            <th><label for="forder"><?php echo \Translator::get('file', 'add', 'order'); ?></label></th>
                            <td class="leftAlign">
                                <input type="radio" name="order" id="forder0" value="no" /> <label for="forder0"><?php echo \Translator::get('file', 'add', 'order-no'); ?></label><br />
                                <input type="radio" name="order" id="forder1" value="top" /> <label for="forder1"><?php echo \Translator::get('file', 'add', 'order-top'); ?></label><br />
                                <input type="radio" name="order" checked="checked" id="forder2" value="bottom" /> <label for="forder2"><?php echo \Translator::get('file', 'add', 'order-bottom'); ?></label><br />
                            </td>
                            <td class="help" title="<?php echo \Translator::get('file', 'add', 'order-help'); ?>"></td>
                        </tr>
                    </table>
                </fieldset>

                <input type="submit" name="submit" value="<?php echo \Translator::get('file', 'add', 'submit'); ?>" />
            </form>