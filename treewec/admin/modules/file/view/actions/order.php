<?php
$path = TREEWEC_VIEW_DIR . Treewec\FileSystem\Path::toFileSystem($_GET['path']);
$dirChecker = new \Treewec\Admin\DirectoryOrderChecker($path);
$filesCountInColumn = max(count($dirChecker->getFilesInOrderedList()), count($dirChecker->getMissingFilesInOrderedList()));
$minHeight = 'style="min-height:' . ($filesCountInColumn * 40) . 'px"';
?>


                <table class="default order">
                    <tr>
                        <th><?php echo \Translator::get('file','order', 'current'); ?></th>
                        <th><?php echo \Translator::get('file','order', 'missing'); ?></th>
                    </tr>
                    <tr>
                        <td>                            
                            <ul id="currentOrder" class="connectedSortable" <?php echo $minHeight; ?>>
                                <?php foreach($dirChecker->getFilesInOrderedList() as $filename): ?>
                                <?php if ($dirChecker->isFilenameInDir($filename)): ?>
                                <li class="ok" id="<?php echo $filename; ?>"><?php echo $filename; ?></li>
                                <?php else: ?>
                                <li class="invalid" id="<?php echo $filename; ?>"><?php echo $filename; ?></li>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        </td>
                        <td>
                            <ul id="deletedOrder" class="connectedSortable" <?php echo $minHeight; ?>>
                                <?php foreach($dirChecker->getMissingFilesInOrderedList() as $filename): ?>
                                <li class="missing" id="<?php echo $filename; ?>"><?php echo $filename; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </td>
                    </tr>
                </table>
                <button id="saveOrder"><?php echo \Translator::get('file','order', 'submit'); ?></button>
                
                
<script src="public/scripts/jquery-ui-1.8.21.custom.min.js" type="text/javascript"></script>
<script>
    $(function() {
        $( "#currentOrder, #deletedOrder" ).sortable({
            connectWith: ".connectedSortable"
    	}).disableSelection();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#saveOrder").click(function (e) {
            var order = $('#currentOrder').sortable('toArray');
            $.ajax({
                url: './sort.php',
                type: 'POST',
                data: {order: order, fileUrl: '<?php echo $pageInfo->currentPath; ?>'},
                success:function (data) {alert(data);}
            });
        });
    });
</script>