
            <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

                <table class="form">
                    <tr>
                        <th><label for="fnew_name"><?php echo \Translator::get('file', 'rename', 'name'); ?></label></th>
                        <td><?php \Treewec\Admin\FormHTMLElements::input('new_name'); ?></td>
                        <td class="help" title="<?php echo \Translator::get('file', 'rename', 'help'); ?>"></td>
                    </tr>
                </table>
                
                <input type="submit" name="submit" value="<?php echo \Translator::get('file', 'rename', 'submit'); ?>" />
            </form>