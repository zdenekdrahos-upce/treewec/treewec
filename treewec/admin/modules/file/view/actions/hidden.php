
            <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

                <table class="form">
                    <tr>
                        <th><label for="fvisibility"><?php echo \Translator::get('file', 'hidden', 'visibility'); ?></label></th>
                        <td><?php \Treewec\Admin\FormHTMLElements::selectBoolean('visibility'); ?></td>
                        <td class="help" title="<?php echo \Translator::get('file', 'hidden', 'help'); ?>"></td>
                    </tr>
                </table>
                
                <input type="submit" name="submit" value="<?php echo \Translator::get('file', 'hidden', 'submit'); ?>" />
            </form>