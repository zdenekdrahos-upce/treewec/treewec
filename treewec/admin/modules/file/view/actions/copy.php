
            <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

                <table class="form">
                    <tr>
                        <th><label for="fnew_folder"><?php echo \Translator::get('file', 'copy', 'new'); ?></label></th>
                        <td><?php 
                            $printer = new \Treewec\Admin\HTMLFormSelect('new_folder');
                            $printer->setProcessRoot(true);
                            $printoutLoader = Treewec\PrintoutLoader::get(
                                    $printer, TREEWEC_VIEW_DIR, 10, true, Treewec\FileSystem\FileType::FOLDER, 
                                    \Treewec\DirectoryIterators\SearchType::DEPTH_FIRST
                            );
                            $printoutLoader->display();
                        ?></td>
                        <td class="help" title="<?php echo \Translator::get('file', 'copy', 'help'); ?>"></td>
                    </tr>
                </table>
                
                <input type="submit" name="submit" value="<?php echo \Translator::get('file', 'copy', 'submit'); ?>" />
            </form>