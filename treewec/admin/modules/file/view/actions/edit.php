<?php                
//TODO: refactoring, move to Controller
$urlBuilder = new \Treewec\URLBuilder($arrayHolder);
$urlBuilder->removeParameter('action');
?>
                
                <p><?php echo \Translator::get('file', 'edit', 'dir-index'); ?></p>
                <?php 
                echo $editor->printHeader(); 
                $view->activateEditMode();
                $view->display();
                echo $editor->printFooter();
                ?>

                <form id="editForm" method="POST" action="<?php echo $urlBuilder->build(); ?>">          
                    <input id="save" type="submit" name="submit" value="<?php echo \Translator::get('file', 'edit', 'submit'); ?>" />
                </form>

                <h3><?php echo \Translator::get('file', 'edit', 'editors'); ?></h3>                
                <nav>
                    <ul>
                    <?php                
                    //TODO: refactoring, move to Controller
                    $urlBuilder = new \Treewec\URLBuilder($arrayHolder);
                    $urlBuilder->removeParameter('action');
                    foreach (\Treewec\Admin\Editors\AvailableEditors::getAvailableEditors() as $editorName) {
                        $urlBuilder->addOrModifyParameter('editor', $editorName);
                        echo '<li>' . \Treewec\HTML\Anchor::create($urlBuilder->build(), $editorName) . '</li>';
                    }
                    ?>
                    </ul>
                </nav>