<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class SettingsGeneralForm extends SinglePageFormSubmission {

    protected function performAction() {
        $_POST['default_theme'] = substr($_POST['default_theme'], 0, -1);
        $this->normalizeBooleans();
        $this->normalizeConstants();
        $this->templater->loadContentFromFile(TREEWEC_ADMIN_ROOT . 'modules/settings/view/templates/treewec.php');
        $this->templater->replaceValues($_POST);
        return $this->templater->saveContentToFile(TREEWEC_CONFIGURATION . "treewec.php");
    }

    private function normalizeBooleans() {
        $booleanFields = array('url_rewrite');
        foreach ($booleanFields as $field) {
            $_POST[$field] = $_POST[$field] ? 'true' : 'false';
        }
    }

    private function normalizeConstants() {
        $constants = array(
            'default_comparator' => 'Treewec\DirectoryIterators\ComparatorType::',
            'default_order' => 'Treewec\DirectoryIterators\OrderType::',
        );
        foreach ($constants as $field => $class) {
            $_POST[$field] = $class . strtoupper($_POST[$field]);
        }
    }

    protected function checkFormFields() {
        foreach (get_class_methods('Treewec\Admin\SettingsGeneralForm') as $method) {
            if (is_int(strpos($method, 'check')) && $method != 'checkFormFields') {
                $this->$method();
            }
        }
    }

    private function checkWebAddress() {
        $this->formProcessor->check_condition(filter_var($_POST['web_root'], FILTER_VALIDATE_URL), Message::get('settings-general-url'));
    }

    private function checkURLRewrite() {
        $this->formTester->testBoolean($_POST['url_rewrite'], \Translator::get('settings', 'gen', 'rewrite'));
    }

    private function checkDefaultTheme() {
        $this->formTester->testExistingDirectory($_POST['default_theme'], TREEWEC_THEMES, \Translator::get('settings', 'gen', 'theme'));
    }

    private function checkDefaultEditor() {
        $this->formProcessor->check_condition(
                Editors\AvailableEditors::isExistingEditor($_POST['default_admin_editor']), Message::get('settings-general-editor')
        );
    }

    private function checkAdminAuthentication() {
        $reflection = new \ReflectionClass($_POST['admin_authentication']);        
        $this->formProcessor->check_condition(
                $reflection->implementsInterface('\Treewec\Admin\Authentication\IAuthentication'),
                Message::get('invalid-value', array(\Translator::get('settings', 'gen', 'authentication')))
        );
    }

    private function checkDefaultLanguage() {
        $this->formProcessor->check_condition($GLOBALS['languageSwitch']->isLanguageSupported($_POST['default_language']), Message::get('invalid-value', array($errorName)));
    }

    private function checkDefaultComparator() {
        $this->formTester->testClassConstant('\Treewec\DirectoryIterators\ComparatorType', $_POST['default_comparator'], \Translator::get('settings', 'gen', 'comparator'));
    }

    private function checkDefaultOrder() {
        $this->formTester->testClassConstant('Treewec\DirectoryIterators\OrderType', $_POST['default_order'], \Translator::get('settings', 'gen', 'order'));
    }

    private function checkDefaultDepth() {
        $this->formTester->testNumber($_POST['default_depth_in_pages'], array('min' => 1, 'max' => 10), \Translator::get('settings', 'gen', 'depth'));
    }

    private function checkPrefixOfHiddenFiles() {
        $this->formTester->testString($_POST['prefix_of_hidden_files'], array('min' => 0, 'max' => 10), \Translator::get('settings', 'gen', 'hidden'));
        $this->formProcessor->check_condition(\Treewec\FileSystem\Filename::isValid($_POST['prefix_of_hidden_files']), Message::get('filename-invalid', array(\Translator::get('settings', 'gen', 'hidden'))));
        $this->formProcessor->check_condition(mb_detect_encoding($_POST['prefix_of_hidden_files']) == 'ASCII', Message::get('settings-general-prefix'));
    }

    private function checkUserHiddenFiles() {
        $this->formTester->testString($_POST['user_hidden_files'], array('min' => 0, 'max' => 200), \Translator::get('settings', 'gen', 'user-hidden'));        
    }

    private function checkIndexFilename() {
        $this->formTester->testFilename($_POST['name_of_index_file'], 'name_of_index_file');
    }

    private function checkDefaultExtension() {
        $this->formTester->testString($_POST['default_file_extension'], array('min' => 0, 'max' => 5), \Translator::get('settings', 'gen', 'extension'));
        if ($this->formProcessor->is_valid() && $_POST['default_file_extension'] !== '') {
            $this->formProcessor->check_condition(
                    substr($_POST['default_file_extension'], 0, 1) == '.', Message::get('settings-general-extension')
            );
        }
    }

    protected function getSuccessMessage() {
        return Message::get('settings-general');
    }

    protected function getStructure() {
        $keys = array(
            'web_root', 'url_rewrite', 'default_theme', 'default_admin_editor', 'default_depth_in_pages',
            'prefix_of_hidden_files', 'user_hidden_files', 'name_of_index_file', 'default_file_extension',
            'default_comparator', 'default_order', 'admin_authentication', 'default_language'
        );
        $structure = array();
        foreach ($keys as $key) {
            $structure[$key] = constant('TREEWEC_' . strtoupper($key));
        }
        $structure['default_theme'] .= '/';
        return $structure;
    }

}

?>