<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class SettingsTextForm extends SinglePageFormSubmission {

    protected function performAction() {
        $this->templater->loadContentFromFile(TREEWEC_ADMIN_ROOT . 'modules/settings/view/templates/default-texts.php');
        $this->templater->replaceValues($_POST);
        return $this->templater->saveContentToFile(TREEWEC_CONFIGURATION . 'default-texts.php');
    }

    protected function checkFormFields() {
        $translation = $this->getKeysForTranslation();
        foreach (array_keys($this->getStructure()) as $arrayKey) {
            $translatedText = \Translator::get('settings', 'text', $translation[$arrayKey]);
            $this->formTester->testString($_POST[$arrayKey], array('min' => 1, 'max' => 100), $translatedText);
        }
    }

    private function getKeysForTranslation() {
        return array(
            'treewec_mainpage' => 'main',
            'treewec_default_error' => 'error',
            'treewec_nav_next' => 'next',
            'treewec_nav_previous' => 'previous',
            'treewec_nav_content' => 'content',
            'treewec_nav_parent' => 'parent',
        );
    }

    protected function getSuccessMessage() {
        return Message::get('settings-texts');
    }

    protected function getStructure() {
        $keys = array(
            'treewec_mainpage', 'treewec_default_error',
            'treewec_nav_next', 'treewec_nav_previous', 'treewec_nav_content', 'treewec_nav_parent'
        );
        $structure = array();
        foreach ($keys as $key) {
            $structure[$key] = constant(strtoupper($key));
        }
        return $structure;
    }

}

?>