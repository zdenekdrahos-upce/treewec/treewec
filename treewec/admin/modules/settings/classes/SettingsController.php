<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class SettingsController extends AdminModuleController {

    public function __construct() {
        parent::__construct();
        parent::addForm(new SettingsGeneralForm('submitGeneral', false));
        parent::addForm(new SettingsTextForm('submitText', false));
    }

    public function loadView() {
        parent::loadView();
        $this->loadRecommendedURL();
        $this->loadSelectedDiv();
    }

    private function loadRecommendedURL() {
        $this->view->addData(array('recommendedURL' => AdvancedFunctions::getRecommendedWebrootURL()));
    }

    private function loadSelectedDiv() {
        $selectedDiv = isset($_POST['submitText']) ? 'texts' : 'general';
        $this->view->addData(array('selectedDiv' => $selectedDiv));
    }

}

?>