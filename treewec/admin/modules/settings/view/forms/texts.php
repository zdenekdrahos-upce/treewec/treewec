            
            <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

                <fieldset>
                    <legend><?php echo \Translator::get('settings', 'text', 'general'); ?></legend>
                    <table class="form">
                        <tr>
                            <th><label for="ftreewec_mainpage"><?php echo \Translator::get('settings', 'text', 'main'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('treewec_mainpage'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'text', 'help', 'main'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="ftreewec_default_error"><?php echo \Translator::get('settings', 'text', 'error'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('treewec_default_error'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'text', 'help', 'error'); ?>"></td>
                        </tr>
                    </table>
                </fieldset>
                
                <fieldset>
                    <legend><?php echo \Translator::get('settings', 'text', 'nav'); ?></legend>
                    <table class="form">
                        <tr>
                            <th><label for="ftreewec_nav_next"><?php echo \Translator::get('settings', 'text', 'next'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('treewec_nav_next'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'text', 'help', 'next'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="ftreewec_nav_previous"><?php echo \Translator::get('settings', 'text', 'previous'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('treewec_nav_previous'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'text', 'help', 'previous'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="ftreewec_nav_content"><?php echo \Translator::get('settings', 'text', 'content'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('treewec_nav_content'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'text', 'help', 'content'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="ftreewec_nav_parent"><?php echo \Translator::get('settings', 'text', 'parent'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('treewec_nav_parent'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'text', 'help', 'parent'); ?>"></td>
                        </tr>
                    </table>
                </fieldset>
                
                <input type="submit" name="submitText" value="<?php echo \Translator::get('save'); ?>" />
            </form>