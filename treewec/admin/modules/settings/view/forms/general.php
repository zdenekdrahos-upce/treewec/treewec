
            <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

                <fieldset>
                    <legend><?php echo \Translator::get('settings', 'gen', 'web'); ?></legend>
                    <table class="form">
                        <tr>
                            <th><label for="fweb_root"><?php echo \Translator::get('settings', 'gen', 'address'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('web_root', 'url'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'address'); ?>"></td>
                        </tr>
                        <?php if ($recommendedURL != $_POST['web_root']): ?>
                        <tr>
                            <td colspan="3"><?php echo \Translator::get('settings', 'gen', 'recommend-address'); ?> <strong><?php echo $recommendedURL; ?><strong></td>
                        </tr>
                        <?php endif; ?>
                        <tr>
                            <th><label for="furl_rewrite"><?php echo \Translator::get('settings', 'gen', 'rewrite'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::selectBoolean('url_rewrite'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'rewrite'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="fdefault_theme"><?php echo \Translator::get('settings', 'gen', 'theme'); ?></label></th>
                            <td><?php 
                            $printer = new \Treewec\Admin\HTMLFormSelect('default_theme');
                            $printer->setRootDirectory(TREEWEC_THEMES);
                            $printer->setProcessRoot(false);
                            $printoutLoader = Treewec\PrintoutLoader::get(
                                    $printer, TREEWEC_THEMES, 1, true, Treewec\FileSystem\FileType::FOLDER, 
                                    \Treewec\DirectoryIterators\SearchType::BASIC
                            );
                            $printoutLoader->display();
                            ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'theme'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="fdefault_admin_editor"><?php echo \Translator::get('settings', 'gen', 'editor'); ?></label></th>
                            <td><?php 
                            // TODO: dynamic load of editors
                            $printer = new \Treewec\Admin\HTMLFormSelect('default_admin_editor');
                            $items = array(
                                'plain' => Translator::get('constants', 'editors', 'plain'),
                                'ckeditor' => Translator::get('constants', 'editors', 'ckeditor'),
                                'contenteditable' => Translator::get('constants', 'editors', 'contenteditable'),
                            );
                            \Treewec\Admin\FormHTMLElements::select('default_admin_editor', $items); 
                            ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'editor'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="fadmin_authentication"><?php echo \Translator::get('settings', 'gen', 'authentication'); ?></label></th>
                            <td><?php                             
                            $items = array(
                                '\Treewec\Admin\Authentication\FreeAccess' => Translator::get('constants', 'authentication', 'freeaccess'),
                                '\Treewec\Admin\Authentication\OnePassword' => Translator::get('constants', 'authentication', 'onepassword'),
                            );
                            \Treewec\Admin\FormHTMLElements::select('admin_authentication', $items); 
                            ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'authentication'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="fdefault_language"><?php echo \Translator::get('settings', 'gen', 'language'); ?></label></th>
                            <td><?php 
                            $items = array(
                                'czech' => Translator::get('czech'),
                                'english' => Translator::get('english'),
                            );
                            \Treewec\Admin\FormHTMLElements::select('default_language', $items); 
                            ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'language'); ?>"></td>
                        </tr>
                    </table>
                </fieldset>
                
                <fieldset>
                    <legend><?php echo \Translator::get('settings', 'gen', 'ordering'); ?></legend>
                    <table class="form">
                        <tr>
                            <th><label for="fdefault_comparator"><?php echo \Translator::get('settings', 'gen', 'comparator'); ?></label></th>
                            <td><?php 
                                $items = \Treewec\Admin\AdvancedFunctions::getConstantsWithTranslatedTexts('\Treewec\DirectoryIterators\ComparatorType', 'comparators');
                                \Treewec\Admin\FormHTMLElements::select('default_comparator', $items); 
                            ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'comparator'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="fdefault_order"><?php echo \Translator::get('settings', 'gen', 'order'); ?></label></th>
                            <td><?php
                                $items = \Treewec\Admin\AdvancedFunctions::getConstantsWithTranslatedTexts('\Treewec\DirectoryIterators\OrderType', 'order');
                                \Treewec\Admin\FormHTMLElements::select('default_order', $items); 
                            ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'order'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="fdefault_depth_in_pages"><?php echo \Translator::get('settings', 'gen', 'depth'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::selectNumbers('default_depth_in_pages', 1, 10); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'depth'); ?>"></td>
                        </tr>
                    </table>
                </fieldset>
                
                <fieldset>
                    <legend><?php echo \Translator::get('settings', 'gen', 'files'); ?></legend>
                    <table class="form">
                        <tr>
                            <th><label for="fprefix_of_hidden_files"><?php echo \Translator::get('settings', 'gen', 'hidden'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('prefix_of_hidden_files'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'hidden'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="fuser_hidden_files"><?php echo \Translator::get('settings', 'gen', 'user-hidden'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('user_hidden_files'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'user-hidden'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="fname_of_index_file"><?php echo \Translator::get('settings', 'gen', 'index'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('name_of_index_file'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'index'); ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="fdefault_file_extension"><?php echo \Translator::get('settings', 'gen', 'extension'); ?></label></th>
                            <td><?php \Treewec\Admin\FormHTMLElements::input('default_file_extension'); ?></td>
                            <td class="help" title="<?php echo \Translator::get('settings', 'gen', 'help', 'extension'); ?>"></td>
                        </tr>
                    </table>
                </fieldset>
                
                <input type="submit" name="submitGeneral" value="<?php echo \Translator::get('save'); ?>" />
            </form>