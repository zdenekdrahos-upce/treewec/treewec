<?php
# Treewec settings
define('TREEWEC_URL_REWRITE', {url_rewrite});
define('TREEWEC_WEB_ROOT', '{web_root}');
define('TREEWEC_DEFAULT_THEME', '{default_theme}');
define('TREEWEC_DEFAULT_ADMIN_EDITOR', '{default_admin_editor}');
define('TREEWEC_ADMIN_AUTHENTICATION', '{admin_authentication}');
define('TREEWEC_DEFAULT_LANGUAGE', '{default_language}');
# Default settings
define('TREEWEC_DEFAULT_COMPARATOR', {default_comparator});
define('TREEWEC_DEFAULT_ORDER', {default_order});
define('TREEWEC_DEFAULT_DEPTH_IN_PAGES', {default_depth_in_pages});
# Files
define('TREEWEC_PREFIX_OF_HIDDEN_FILES', '{prefix_of_hidden_files}');
define('TREEWEC_USER_HIDDEN_FILES', '{user_hidden_files}');
define('TREEWEC_DEFAULT_FILE_EXTENSION', '{default_file_extension}');
define('TREEWEC_NAME_OF_INDEX_FILE', '{name_of_index_file}');
?>