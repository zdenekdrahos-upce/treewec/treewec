<?php
# Header in root folder (name of the website/application)
define('TREEWEC_MAINPAGE', '{treewec_mainpage}');
# ERROR Message - used in header if there was error 404,...
define('TREEWEC_DEFAULT_ERROR', '{treewec_default_error}');
# Breadcrumb navigation
define('TREEWEC_NAV_NEXT', '{treewec_nav_next}');
define('TREEWEC_NAV_PREVIOUS', '{treewec_nav_previous}');
define('TREEWEC_NAV_CONTENT', '{treewec_nav_content}');
define('TREEWEC_NAV_PARENT', '{treewec_nav_parent}');
?>