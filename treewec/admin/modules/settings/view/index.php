<?php
$generalId = $selectedDiv == 'general' ? ' id="displayed"' : '';
$textsId = $selectedDiv == 'texts' ? ' id="displayed"' : '';
?>

            <ul class="content">
                <li><a href="javascript:toggleOneDiv('general', 'toogle');"><?php echo \Translator::get('settings', 'general'); ?></a></li>
                <li><a href="javascript:toggleOneDiv('texts', 'toogle');"><?php echo \Translator::get('settings', 'texts'); ?></a></li>
            </ul>
            
            <?php require_once(TREEWEC_ADMIN_ROOT . 'theme/error-listing.php'); ?>        
            
            <div class="toogle" name="general"<?php echo $generalId; ?>>
                <h2><?php echo \Translator::get('settings', 'general'); ?></h2>
                <?php include('forms/general.php'); ?>
            </div>
            
            <div class="toogle" name="texts"<?php echo $textsId; ?>>
                <h2><?php echo \Translator::get('settings', 'texts'); ?></h2>
                <?php include('forms/texts.php'); ?>
            </div>