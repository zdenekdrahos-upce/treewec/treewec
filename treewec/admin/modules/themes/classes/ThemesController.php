<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class ThemesController extends AdminModuleController {

    private $urlArguments;
    private $themeName;
    
    public function addDataToView($args) {        
        $this->urlArguments = $args['urlArgs'];                
        if ($this->urlArguments->path) {
            $this->themeName = mb_substr($this->urlArguments->path, 0, -1, 'UTF-8');
            $this->updateView();
        }
        parent::addDataToView($args);
    }

    public function loadView() {        
        if ($this->urlArguments->path) {
            $this->loadThemePath();
        }
    }

    private function updateView() {
        $this->view = new \Treewec\View();        
        $path = $this->moduleViewFolder . 'detail.php';
        $this->view->addContent(new \Treewec\FileSystem\File($path));
    }

    private function loadThemePath() {
        $filesPath = TREEWEC_THEMES . $this->themeName . '/';
        $publicPath = TREEWEC_SITE_ROOT . 'public/themes/' . $this->themeName . '/';
        $this->view->addData(array('themeFilePath' => $filesPath));
        $this->view->addData(array('themePublicPath' => $publicPath));
    }

}

?>