            
            <p><?php echo \Translator::get('themes', 'about-themes'); ?></p>
            <p><strong><?php echo \Translator::get('themes', 'warning'); ?></strong></p>
            
            <h2><?php echo \Translator::get('themes', 'list'); ?></h2>
            <?php
            $printer = new \Treewec\Admin\HTMLThemesPrinter();
            $pl = Treewec\PrintoutLoader::get($printer, TREEWEC_THEMES, 1, true, false, \Treewec\DirectoryIterators\SearchType::BASIC);
            $pl->display();
            ?>