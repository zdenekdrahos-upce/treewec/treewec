            
            <p><?php echo \Translator::get('home', 'welcome'); ?></p>
            
            <h2><?php echo \Translator::get('home', 'info'); ?></h2>
            <ul>
                <li><?php echo \Translator::get('home', 'pages-count'); ?>: <strong><?php echo "{$pageCounts['totalCount']} ({$pageCounts['fileCount']} <img src=\"public/images/file.png\" />, {$pageCounts['dirCount']} <img src=\"public/images/folder.png\" />)"; ?></strong></li>
                <li><?php echo \Translator::get('menu', 'version'); ?>: <strong><?php echo $treewecVersion; ?></strong></li>
            </ul>
            
            <h2><?php echo \Translator::get('menu', 'main'); ?></h2>
            <p><?php echo \Translator::get('home', 'nav'); ?></p>
            <?php include(TREEWEC_ADMIN_ROOT . 'theme/menu.php'); ?>