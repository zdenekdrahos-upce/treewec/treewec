<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class HomeController extends AdminModuleController {

    public function loadView() {
        $this->view->addData(array(
            'pageCounts' => $this->getPageCount(),
            'treewecVersion' => TREEWEC_VERSION,
        ));
    }
    
    private function getPageCount() {
        $fileCounter = new FileCounter();
        $fileCounter->setProcessRoot(true);
        $pl = \Treewec\PrintoutLoader::get($fileCounter, TREEWEC_VIEW_DIR, 1000, true, false, \Treewec\DirectoryIterators\SearchType::DEPTH_FIRST);
        $pl->display();
        return $fileCounter->getCounts();
    }

}

?>