<?php
$linkTutorials = $GLOBALS['languageSwitch']->getNameOfSelectedLanguage() == 'english' ? 'en/tutorials/' : 'cs/návody/';
?>

            <p><?php echo \Translator::get('help', 'info'); ?> <a href="http://www.treewec.g6.cz" target="_blank">Treewec</a>:</p>
            <ul>
                <li><a href="http://www.treewec.g6.cz/<?php echo $linkTutorials; ?>" target="_blank"><?php echo \Translator::get('help', 'link-tutorials'); ?></a></li>
                <li><a href="http://www.treewec.g6.cz/forums/" target="_blank"><?php echo \Translator::get('help', 'link-forum'); ?></a></li>
            </ul>            
            
            <h2><?php echo \Translator::get('error', 'how-report'); ?></h2>
            <p><?php echo \Translator::get('help', 'before-bug'); ?></p>
            <ul>
                <li><a href="https://bitbucket.org/zdenekdrahos/treewec/issues/new" target="_blank"><?php echo \Translator::get('error', 'report', 'bitbucket'); ?></a></li>
                <li><a href="http://www.treewec.g6.cz/forums/" target="_blank"><?php echo \Translator::get('error', 'report', 'treewec'); ?></a></li>
            </ul>
            
            <h2><?php echo \Translator::get('menu', 'main'); ?></h2>
            <p><?php echo \Translator::get('home', 'nav'); ?></p>
            <?php include(TREEWEC_ADMIN_ROOT . 'theme/menu.php'); ?>