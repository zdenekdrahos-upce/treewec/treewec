<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class AuthenticationController extends AdminModuleController {

    public function __construct() {}

    public function loadView() {
        $class = str_replace('\Treewec\Admin\Authentication\\', '', TREEWEC_ADMIN_AUTHENTICATION);
        $path = TREEWEC_ADMIN_ROOT . 'authentication/' . strtolower($class);
        $filename = $_GET['admin'];
        $this->view = new \Treewec\View();
        $this->view->addContent(new \Treewec\FileSystem\File("{$path}/{$filename}.php"));
    }

}

?>