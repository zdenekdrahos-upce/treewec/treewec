                        
            <table class="default audit">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th><?php echo \Translator::get('audit', 'minimal'); ?></th>
                        <th><?php echo \Translator::get('audit', 'your'); ?></th>
                        <th><?php echo \Translator::get('audit', 'is-it-ok'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($audit as $item): ?>
                    <tr>
                        <th><strong><?php echo \Translator::get('audit', $item['title']); ?></strong></th>
                        <td><?php echo $item['min']; ?></td>
                        <td><?php echo $item['user']; ?></td>
                        <td class="<?php echo $item['result']; ?>"><?php echo \Translator::get('audit', $item['result']); ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>