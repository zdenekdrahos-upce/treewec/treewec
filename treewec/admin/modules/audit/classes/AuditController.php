<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class AuditController extends AdminModuleController {

    private $audit;

    public function loadView() {
        $this->audit = array();
        $this->loadPHPVersion();
        $this->loadPermissionOnView();
        $this->loadPermissionOnUserFiles();
        $this->loadWebRootLink();
        $this->loadThemeFiles();
        $this->view->addData(array('audit' => $this->audit));
    }

    private function loadThemeFiles() {
        $requiredFiles = array('index.php', 'error.php', 'templates/');
        $themeFiles = array();
        foreach ($requiredFiles as $file) {
            if (file_exists(TREEWEC_THEMES . TREEWEC_DEFAULT_THEME . "/{$file}")) {
                $themeFiles[] = $file;
            }
        }
        $this->audit[] = array(
            'title' => 'theme-required-files',
            'min' => implode(', ', $requiredFiles),
            'user' => implode(', ', $themeFiles),
            'result' => $this->getResultText(count($themeFiles) == 3)
        );
    }

    private function loadWebRootLink() {
        $min = AdvancedFunctions::getRecommendedWebrootURL();
        $user = TREEWEC_WEB_ROOT;
        $this->audit[] = array(
            'title' => 'webroot-link',
            'min' => $min,
            'user' => $user,
            'result' => $this->getResultText($min == $user)
        );
    }

    private function loadPHPVersion() {
        $result = ((float) phpversion()) >= 5.3;
        $this->audit[] = array(
            'title' => 'php-version',
            'min' => '5.3',
            'user' => phpversion(),
            'result' => $this->getResultText($result)
        );
    }

    private function loadPermissionOnView() {
        $this->loadPermissionItem('permission-viewdir', 666, TREEWEC_VIEW_DIR);
    }

    private function loadPermissionOnUserFiles() {
        $this->loadPermissionItem('permission-userfiles', 666, TREEWEC_USER_FILES);
    }

    private function loadPermissionItem($key, $minimal, $path) {
        $filePerms = $this->getFilePermissions($path);
        $result = $filePerms >= $minimal;
        $this->audit[] = array(
            'title' => $key,
            'min' => $minimal,
            'user' => $filePerms,
            'result' => $this->getResultText($result)
        );
    }

    private function getResultText($result) {
        return $result ? 'yes' : 'no';
    }

    private function getFilePermissions($path) {
        return substr(sprintf('%o', fileperms($path)), -3);
    }

}

?>