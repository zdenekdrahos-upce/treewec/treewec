<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin\Editors;

final class ContentEditableEditor {

    private static $stylesPath = 'xx';

    public static function getStyles() {
        self::$stylesPath = TREEWEC_CONFIGURATION . 'contenteditable-styles.php';
        $styles = array();
        if (self::existsFileWithStyles()) {
            $styles = self::getStylesArray();
        }
        return self::transformStylesToString($styles);
    }

    private static function existsFileWithStyles() {
        return file_exists(self::$stylesPath);
    }

    private static function getStylesArray() {
        include(self::$stylesPath);
        if (isset($contentEditableStyles) && is_array($contentEditableStyles)) {
            return $contentEditableStyles;
        }
        return array();
    }

    private static function transformStylesToString($styles) {
        return '["' . implode('", "', $styles) . '"]';
    }

}

?>