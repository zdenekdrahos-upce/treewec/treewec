<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin\Editors;

final class Editor {
    
    private $dirRoot;
            
    public function __construct($editorName) {
        $this->dirRoot = AvailableEditors::getEditorFolder($editorName);
    }
    
    public function includeRequiredFilesInHTMLHead() {
        require_once($this->dirRoot . 'editor-htmlhead.php');
    }
            
    public function printHeader() {
        require_once($this->dirRoot . 'editor-header.php');
    }
            
    public function printFooter() {
        require_once($this->dirRoot . 'editor-footer.php');
    }
    
}

?>