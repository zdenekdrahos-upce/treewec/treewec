<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin\Editors;

final class AvailableEditors {

    private static $availableEditors;

    public static function getEditorFolder($editor) {
        $dir = TREEWEC_ADMIN_ROOT . 'editors/';
        $editor = self::isExistingEditor($editor) ? $editor : TREEWEC_DEFAULT_ADMIN_EDITOR;
        return $dir . $editor . '/';
    }

    public static function isExistingEditor($editor) {
        if (is_string($editor)) {
            foreach (self::getAvailableEditors() as $avEditor) {
                if ($avEditor == $editor) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function getAvailableEditors() {
        if (is_null(self::$availableEditors)) {
            self::loadEditors();
        }
        return self::$availableEditors;
    }

    private static function loadEditors() {
        $dirIterator = new \Treewec\DirectoryIterators\DirectoryIterator(TREEWEC_ADMIN_ROOT . 'editors/');
        if ($dirIterator->hasNext()) {
            $dirIterator->next(); // skip root folder
        };
        self::$availableEditors = array();
        while ($dirIterator->hasNext()) {
            $iteratorElement = $dirIterator->next();
            self::$availableEditors[] = $iteratorElement->splFileInfo->getFilename();
        }
    }

}

?>