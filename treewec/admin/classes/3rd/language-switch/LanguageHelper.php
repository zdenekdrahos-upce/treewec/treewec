<?php
/**
 * PHP Language-Switch class - simple PHP class to switch languages on website
 * Copyright (C) 2011,2012  Zdenek Drahos (https://bitbucket.org/zdenekdrahos/php-language-switch-class)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

final class LanguageHelper {
    
    public static function redirectToCleanURL() {
        if (isset($_GET['language'])) {
            unset($_GET['language']);
            header('Location: ' . self::getHrefForLink(null));
            exit;
        }
    }

    public static function printListOfAvailableLanguages() {
        $langs = $GLOBALS['languageSwitch']->getAvailableLanguages();
        self::printLinksOfAvailableLanguages($langs, $GLOBALS['languageSwitch']);
    }

    public static function printLinksOfAvailableLanguages($langs, $languageSwitch) {
        if (is_array($langs) && $languageSwitch instanceof LanguageSwitch) {
            echo '<ul class="language-chooser">';
            foreach ($langs as $language) {
                if ($language == $languageSwitch->getNameOfSelectedLanguage()) {
                    echo '<li>' . Translator::get($language) . '</li>';
                } else {
                    echo '<li><a href="' . self::getHrefForLink($language) . '">' . Translator::get($language) . '</a></li>';
                }
            }
            echo '</ul>';
        }
    }

    private static function getHrefForLink($language) {
        $urlBuilder = new \Treewec\URLBuilder(new \Treewec\Holders\ArrayHolder($_GET));
        $urlBuilder->addOrModifyParameter('language', $language);
        return $urlBuilder->build();
    }

}

?>