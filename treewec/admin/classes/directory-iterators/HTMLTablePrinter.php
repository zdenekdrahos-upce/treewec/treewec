<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

class HTMLTablePrinter extends \Treewec\DirectoryPrinter\Printer {

    public function __construct() {
        parent::__construct();
    }
    
    public function process() {
        include(TREEWEC_ADMIN_ROOT . 'modules/pages/view/all-pages/table-header.php');
        parent::process();
        include(TREEWEC_ADMIN_ROOT . 'modules/pages/view/all-pages/table-footer.php');
    }

    protected function processElement($iteratorElement) {
        $prefix = str_repeat('&mdash;', $iteratorElement->depth);   
        $baseLink = $this->outputter->getBaseLinkForURL($iteratorElement->splFileInfo);
        $link = $this->getURLLink($iteratorElement->splFileInfo);
        $fileInfo = FileInfo::getFromSplFileInfo($iteratorElement->splFileInfo);
        $iconName = $this->getIconName($iteratorElement->splFileInfo);
        include(TREEWEC_ADMIN_ROOT . 'modules/pages/view/all-pages/page-detail.php');
    }

    private function getIconName($splFileInfo) {
        if ($splFileInfo->isFile()) {
            return 'file.png';
        } else {
            $dirChecker = new DirectoryOrderChecker($splFileInfo->getPathname() . '/');
            if ($dirChecker->isValid()) {
                return 'folder.png';
            } else {
                $title = \Translator::get('pages', 'table', 'invalid-dir');
                return 'folder-invalid.png" title="' . $title . '"';
            }
        } 
    }

}

?>