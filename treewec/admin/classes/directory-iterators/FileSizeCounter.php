<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FileSizeCounter extends \Treewec\DirectoryIterators\IteratorProcessor {

    private $fileSize;

    public function __construct() {
        parent::__construct();
        $this->fileSize = 0;
        parent::setProcessRoot(false);
    }

    protected function processElement($iteratorElement) {
        if (!$iteratorElement->splFileInfo->isDir()) {
            $this->fileSize += $iteratorElement->splFileInfo->getSize();
        }
    }

    public function getSize() {
        return $this->fileSize;
    }

}

?>