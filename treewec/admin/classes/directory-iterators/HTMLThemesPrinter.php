<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

class HTMLThemesPrinter extends \Treewec\DirectoryPrinter\HTMLListPrinter {

    public function __construct() {
        parent::__construct();
        $this->setProcessRoot(false);
        $this->setRootDirectory(TREEWEC_THEMES);
        $this->urlBuilder->addOrModifyParameter('admin', 'themes');
    }

}

?>