<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class HTMLFormSelect extends \Treewec\DirectoryPrinter\Printer {

    private $selectName;

    public function __construct($selectName = 'root_dir') {
        parent::__construct();
        parent::setPrintLinks(false);
        $this->selectName = $selectName;
    }
    
    public function process() {
        echo '<select name="' . $this->selectName . '" id="f' . $this->selectName . '" size="1">';
        parent::process();
        echo '</select>';
    }

    protected function processElement($iteratorElement) {               
        $prefix = str_repeat('&ndash; ', $iteratorElement->depth);
        $baseLink = $this->outputter->getBaseLinkForURL($iteratorElement->splFileInfo);
        echo '<option ' . (($baseLink == $_POST[$this->selectName]) ? 'selected="true" ' : '') . 'value="' . $baseLink . '">';
        echo $prefix . ($baseLink ? $baseLink : TREEWEC_MAINPAGE);
    }

}

?>