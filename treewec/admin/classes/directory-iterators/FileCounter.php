<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FileCounter extends \Treewec\DirectoryIterators\IteratorProcessor {

    private $fileCount;
    private $dirCount;

    public function __construct() {
        parent::__construct();
        $this->fileCount = 0;
        $this->dirCount = 0;
    }

    protected function processElement($iteratorElement) {
        if ($iteratorElement->splFileInfo->isDir()) {
            $this->dirCount++;
        } else {
            $this->fileCount++;
        }
    }

    public function getCounts() {
        return array(
            'totalCount' => $this->fileCount + $this->dirCount,
            'dirCount' => $this->dirCount,
            'fileCount' => $this->fileCount
        );
    }

}

?>