<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin\Authentication;

final class OnePassword implements IAuthentication {

    public function isLoggedIn() {
        return isset($_SESSION['treewec_logged']);
    }

    public static function isPasswordValid($password) {
        return $password == self::getPassword();
    }

    private static function getPassword() {
        return 'demo';
    }

}

?>