<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class Authentication {

    public static function isAuthenticated() {        
        $class = TREEWEC_ADMIN_AUTHENTICATION;
        $object = new $class();
        $class = '\Treewec\Admin\Authentication\IAuthentication';
        if ($object instanceof $class) {
            return $object->isLoggedIn() || (isset($_GET['admin']) && in_array($_GET['admin'], array('login', 'logout'), true));
        } else {
            die('Invalid admin authentication class');
        }
    }

}

?>