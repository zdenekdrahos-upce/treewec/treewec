<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

//TODO: refactoring
final class AdminAreaController extends \Treewec\Controller {

    /** @var Treewec\Admin\AdminModuleController */
    private $moduleController;
    /** @var Treewec\Admin\Editors\Editor - or null if FileModuleController is not selected */
    private $editor;
    /** @var boolean isAuthenticated */
    private $isAuthenticated;

    public function __construct($arrayHolder) {
        parent::__construct($arrayHolder);
        $this->moduleController = false;
        $this->isAuthenticated = false;
        session_start();
    }

    public function loadView() {        
        if (parent::loadView()) {
            try {
                $this->isAuthenticated = Authentication::isAuthenticated();
                if (!$this->isAuthenticated) {
                    \Treewec\Exceptions\ClientRequestException::throwsForbiddenAccess();
                }
                $this->loadModuleController();
                $this->moduleController->loadView();
            } catch (\Exception $e) {
                $this->moduleController = false;
                parent::processException($e);
            }
        }
    }

    private function loadModuleController() {
        // TODO: method for loading urlAgrs - add default values for nonselected args
        $controllerName = $this->urlArguments->admin ? ucfirst($this->urlArguments->admin) : 'File';
        $controllerName = in_array($this->urlArguments->admin, array('login', 'logout'), true) ? 'Authentication' : $controllerName;
        $controllerName = 'Treewec\Admin\\' . $controllerName . 'Controller';
        $this->moduleController = new $controllerName();
        if ($this->moduleController instanceof FileController) {
            $this->updateFileController();
        } else if ($this->moduleController instanceof ThemesController) {
            $this->moduleController->addDataToView(array(
                'urlArgs' => $this->urlArguments
            ));
        } else if ($this->moduleController instanceof PagesController) {
            $this->moduleController->addDataToView(array(                
                'pageInfo' => $this->pageInfo
            ));
        }
    }

    private function updateFileController() {        
        if ($this->pageInfo->pageType == \Treewec\FileSystem\FileType::FOLDER) {
            $this->updateViewForDirectory();
        }
        if ($this->urlArguments->action == '') {
            $this->loadEditor();
        }
        $this->moduleController->addDataToView(array(
            'arrayHolder' => $this->arrayHolder,
            'pageInfo' => $this->pageInfo,
            'view' => $this->view,
            'editor' => $this->editor,
        ));
    }

    private function updateViewForDirectory() {
        $this->view = new \Treewec\View();
        $folderIndex = $this->pageInfo->getFilePath(TREEWEC_VIEW_DIR) . TREEWEC_NAME_OF_INDEX_FILE . TREEWEC_DEFAULT_FILE_EXTENSION;
        $this->view->addContent(new \Treewec\FileSystem\File($folderIndex));
    }

    private function loadEditor() {        
        $this->editor = new Editors\Editor($this->urlArguments->editor);
        if ($this->isContentEditable()) {
            $this->displayContentEditable();            
        }
    }
    
    private function isContentEditable() {
        $isEditorSet = $this->urlArguments->editor == 'contenteditable';
        $isEditorDefault = TREEWEC_DEFAULT_ADMIN_EDITOR == 'contenteditable' && is_null($this->urlArguments->editor);
        $isNotSetAction = is_null($this->urlArguments->action);
        $isNotSetAdmin = is_null($this->urlArguments->admin);
        return ($isEditorSet || $isEditorDefault) && $isNotSetAction && $isNotSetAdmin && $this->isAuthenticated;
    }
    
    private function displayContentEditable() {        
        $this->moduleController = false;
        $GLOBALS['currentPath'] = $this->pageInfo->currentPath;        
        parent::displayView();
        die;
    }

    protected function getExtractedVariablesToView() {
        $adminVars = array(
            'moduleController' => $this->moduleController,
            'h1' => $this->getPageH1()
        );        
        if ($this->moduleController instanceof FileController) {
            $adminVars['editor'] = $this->editor;
        }
        return array_merge($adminVars, parent::getExtractedVariablesToView());
    }

    private function getPageH1() {        
        if ($this->urlArguments->admin && !($this->urlArguments->admin == 'themes' && $this->urlArguments->path)) {
            return \Translator::get('menu', $this->urlArguments->admin);
        } else {
            return \Treewec\Utils\Strings::uppercaseFirstLetterAndReplaceDashes($this->pageInfo->header->header, ' ');
        }
    }

    protected function loadViewFromDirectory($directory) {
        $index = $directory->getPath() . TREEWEC_NAME_OF_INDEX_FILE . TREEWEC_DEFAULT_FILE_EXTENSION;
        if (!file_exists($index)) {
            // otherwise error 404 in admin -> create empty index file
            file_put_contents($index, '');
        }
        parent::loadViewFromDirectory($directory);
    }

    protected function getURLArgsInspector() {
        return new \Treewec\AdminURLInspector();
    }

    protected function getThemeRootDirectory() {
        if ($this->isContentEditable()) {
            return TREEWEC_THEMES . TREEWEC_DEFAULT_THEME . '/';
        }
        return TREEWEC_ADMIN_ROOT . 'theme/';
    }

    protected function getFileRootDirectory() {
        if ($this->urlArguments->admin == 'themes' && $this->urlArguments->path) {
            return TREEWEC_THEMES;
        } else {
            return TREEWEC_VIEW_DIR;
        }
    }

}

?>