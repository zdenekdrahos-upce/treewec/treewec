<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

abstract class AdminModuleController {

    protected $moduleViewFolder;
    /** @var Treewec\View */
    protected $view;
    /** @var array Treewec\Admin\SinglePageFormSubmission */
    private $forms;

    public function __construct() {
        $this->forms = array();
        $this->view = new \Treewec\View();
        $module = isset($_GET['admin']) ? $_GET['admin'] : 'file';
        $this->moduleViewFolder = TREEWEC_ADMIN_ROOT . "modules/{$module}/view/";
        $this->loadIndexFile();
    }
    
    public function addDataToView($args) {
        $this->view->addData($args);
    }

    public function loadView() {
        $this->processForms();
    }

    public function displayView() {
        $this->view->display();
    }

    protected function addForm($form) {
        if (\Treewec\Utils\Instances::isInstanceOf($form, 'Treewec\Admin\SinglePageFormSubmission')) {
            $this->forms[] = $form;
        }
    }

    protected function getErrorCount() {        
        return count($this->getFormErrors());
    }

    private function processForms() {
        foreach ($this->forms as $form) {
            $form->process();
        }
        $this->view->addData(array('errors' => $this->getFormErrors()));
    }

    private function getFormErrors() {
        $errors = array();
        foreach ($this->forms as $form) {
            $errors = array_merge($errors, $form->getFormErrors());
        }
        return $errors;
    }

    private function loadIndexFile() {
        $path = $this->moduleViewFolder . 'index.php';
        $this->view->addContent(new \Treewec\FileSystem\File($path));
    }

}

?>