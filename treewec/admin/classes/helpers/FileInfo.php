<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FileInfo {

    private static $timeFormat = '%d.%m.%Y %H:%M';
    public $name;
    public $path;
    public $type;
    public $size;
    public $dateModification;
    public $dateCreation;
    public $visibility;

    /** @param string $path */
    public static function getFromPath($path) {
        if (!is_string($path) || !is_readable($path)) {
            \Treewec\Exceptions\FileSystemException::throwsNonreadablePath();
        }
        return new self(new \SplFileInfo($path));
    }

    /** @param \SplFileInfo $splFileInfo */
    public static function getFromSplFileInfo($splFileInfo) {
        return new self($splFileInfo);
    }

    private function __construct($splFileInfo) {
        $this->name = \Treewec\FileSystem\Path::toScreen(\Treewec\Utils\Files::getBasename($splFileInfo));
        $this->type = $splFileInfo->isDir() ? 'folder' : 'file';
        $this->path = $splFileInfo->getPathname();
        $this->path .= $splFileInfo->isDir() ? '/' : '';
        $this->size = \Treewec\Utils\Files::humanFilesize(filesize($this->path));
        $this->dateModification = strftime(self::$timeFormat, $splFileInfo->getMTime());
        $this->dateCreation = strftime(self::$timeFormat, $splFileInfo->getCTime());
        $this->visibility = \Treewec\FileSystem\Filename::isHidden($splFileInfo->getFilename()) ? 'hidden' : 'public';
    }

    public function loadFolderContentSize() {
        if ($this->type == 'folder') {            
            $fileSizeCounter = new FileSizeCounter();
            $pl = \Treewec\PrintoutLoader::get($fileSizeCounter, $this->path, 1000, true, false, \Treewec\DirectoryIterators\SearchType::DEPTH_FIRST);
            $pl->display();
            $this->size = \Treewec\Utils\Files::humanFilesize($fileSizeCounter->getSize());
        }
    }

    public function printTree() {
        $printer = \Treewec\PrintoutLoader::getHTMLListPrinter(true, true, true);
        $printoutLoader = \Treewec\PrintoutLoader::get($printer, $this->path, 10, true, false, \Treewec\DirectoryIterators\SearchType::DEPTH_FIRST);
        $printoutLoader->display();
    }

}

?>