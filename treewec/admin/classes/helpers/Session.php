<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

class Session {

    private static $instance;
    private $message;

    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $this->checkMessage();
    }

    public function hasMessage() {
        return $this->message ? true : false;
    }

    public function getMessageStatus() {
        return $this->message->status;
    }

    public function getMessage() {
        return $this->message->content;
    }
    
    public function setMessageSuccess($successMessage) {
        $this->setMessage('success', $successMessage);
    }

    public function setMessageFail($failMessage) {
        $this->setMessage('failure', $failMessage);
    }

    private function setMessage($status, $message) {
        $_SESSION['message'] = array();
        $_SESSION['message']['status'] = $status;
        $_SESSION['message']['content'] = $message;
    }

    private function checkMessage() {        
        $this->message = false;
        if (isset($_SESSION['message'])) {
            $this->message = new SessionMessage;
            $this->message->status = $_SESSION['message']['status'];
            $this->message->content = $_SESSION['message']['content'];
            unset($_SESSION['message']);
        }
    }

}

class SessionMessage {
    public $status;
    public $content;
}

?>