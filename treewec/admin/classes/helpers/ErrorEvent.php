<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class ErrorEvent {

    private $time;
    private $ipAddress;
    private $exceptionCode;
    private $requestedPage;
    private $previousPage;

    public function __construct($exceptionCode) {
        $this->time = strftime(TREEWEC_LOGGER_TIME_FORMAT, $_SERVER['REQUEST_TIME']);
        $this->ipAddress = $_SERVER['REMOTE_ADDR'];
        $this->exceptionCode = $exceptionCode;
        $this->requestedPage = urldecode($_SERVER['REQUEST_URI']);
        $this->previousPage = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '-';
    }

    public function __toString() {
        return '"' . implode('";"', get_object_vars($this)) . '"' . "\n";
    }
}

?>