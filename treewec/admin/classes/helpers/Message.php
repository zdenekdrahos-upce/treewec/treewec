<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class Message {

    public static function get() {
        $args = func_get_args();
        array_unshift($args, 'messages');
        $method = self::isFormattedString($args) ? 'getFormattedString' : 'get';
        return call_user_func_array(array('\Translator', $method), $args);
    }

    private static function isFormattedString($functionArgs) {
        $count = count($functionArgs);
        return is_array($functionArgs[$count - 1]);
    }

}

?>