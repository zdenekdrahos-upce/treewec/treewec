<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

class AdvancedFunctions {

    public static function hasArrayValidStructure($array, $structure) {
        if (is_array($array) && is_array($structure)) {
            if (count($array) >= count($structure)) {
                foreach ($structure as $key) {
                    if (!array_key_exists($key, $array)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public static function getConstantsWithTranslatedTexts($class, $translatorKey, $toLower = false) {
        $rf = new \ReflectionClass($class);
        $constants = $rf->getConstants();
        $constantsWithText = array();
        foreach (array_keys($constants) as $constant) {
            $constantsWithText[$constant] = \Translator::get('constants', $translatorKey, $constant);
        }
        return $constantsWithText;
    }

    public static function getRecommendedWebrootURL() {
        $protocol = $_SERVER['SERVER_PORT'] == 80 ? 'http' : 'https';
        $root = str_replace('admin/index.php', '', $_SERVER['PHP_SELF']);
        return $protocol . '://' . $_SERVER['HTTP_HOST'] . $root;
    }

    public static function getLinkBackToWebsite() {
        $count = 1;
        $path = isset($_GET['path']) ? $_GET['path'] : '';
        return str_replace('admin/', '', \Treewec\URLFactory::getLink($path), $count);
    }

    public static function getBreadcrumbNav($pageInfo) {
        $nav = '';
        if ($pageInfo instanceof \Treewec\PageInfo) {
            $nav .= self::getPageLink($pageInfo->homePage);
            foreach ($pageInfo->pathInfo as $page) {
                $nav .= ' &raquo; ' . self::getPageLink($page);
            }
        }
        return $nav;
    }

    public static function getBreadcrumbNavForPages($pageInfo) {
        $urlBuilder = \Treewec\URLBuilder::getEmptyURLBuilder();
        $urlBuilder->addOrModifyParameter('admin', 'pages');
        array_unshift($pageInfo->pathInfo, $pageInfo->homePage);
        $nav = '';
        foreach ($pageInfo->pathInfo as $page) {
            $urlBuilder->addOrModifyParameter('path', $page->path);
            $nav .= ' &raquo; ' . \Treewec\HTML\Anchor::create($urlBuilder->build(), $page->header);
        }
        return $nav;
    }

    public static function getPageLink($page) {
        return $page ? $page->getLink() : '-';
    }

}

?>