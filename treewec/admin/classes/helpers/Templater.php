<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class Templater {

    private $content;

    public function __construct() {
        $this->content = false;
    }

    public function loadContentFromFile($filePath) {
        try {
            $template = new \Treewec\FileSystem\File($filePath);
            $this->content = file_get_contents($template->getPath());
        } catch (\Exception $e) {}
    }

    public function loadContentFromString($newContent) {
        $this->content = $newContent;
    }

    public function saveContentToFile($filePath) {
        try {
            if (is_string($this->content)) {
                $savedFile = new \Treewec\FileSystem\File($filePath);
                return is_int(file_put_contents($savedFile->getPath(), $this->content));
            }            
        } catch (\Exception $e) {}
        return false;
    }

    public function replaceValues($array) {
        if (is_array($array) && $this->content) {
            foreach ($array as $key => $value) {
                $this->content = str_replace("{{$key}}", $value, $this->content);
            }
        }
    }

}

?>