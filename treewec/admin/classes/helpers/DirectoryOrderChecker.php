<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class DirectoryOrderChecker {

    private $path;
    private $dirContent;
    private $orderedContent;

    public function __construct($directoryPath) {
        $this->path = $directoryPath;
        $this->loadDirContent();
        $this->loadOrderedContent();
    }

    public function isFilenameInOrderedList($filename) {
        return in_array($filename, $this->orderedContent);
    }

    public function isFilenameInDir($filename) {
        return in_array($filename, $this->dirContent);
    }

    public function getFilesInOrderedList() {
        return $this->orderedContent;
    }

    public function getMissingFilesInOrderedList() {
        $missing = array();
        foreach ($this->dirContent as $filename) {
            if (!$this->isFilenameInOrderedList($filename)) {
                $missing[] = $filename;
            }
        }
        return $missing;
    }

    public function isValid() {
        return $this->isFileCountEqual() && $this->areFilenamesOk();
    }

    private function isFileCountEqual() {
        return count($this->orderedContent) == count($this->dirContent);
    }

    private function areFilenamesOk() {
        foreach ($this->dirContent as $filename) {            
            if (!$this->isFilenameInOrderedList($filename)) {
                return false;
            }
        }
        return true;
    }

    private function loadOrderedContent() {
        $dirOrder = new \Treewec\DirectoryIterators\DirectoryOrder();
        $dirOrder->changeDirectory($this->path);
        $this->orderedContent = array_keys($dirOrder->getOrderArray());
    }

    private function loadDirContent() {
        // TODO: refactoring
        $iterator = new \Treewec\DirectoryIterators\DirectoryIterator($this->path);
        $filter = new \Treewec\DirectoryIterators\IteratorFilters();
        $filter->addFilter(new \Treewec\DirectoryIterators\FilterFilename());
        $iterator->setFilter($filter);
        if ($iterator->hasNext()) {
            $iterator->next();
        }
        $this->dirContent = array();
        while ($iterator->hasNext()) {
            $filename = $iterator->next()->splFileInfo->getFilename();
            $this->dirContent[] = \Treewec\FileSystem\Path::toScreen($filename);
        }
    }

}

?>