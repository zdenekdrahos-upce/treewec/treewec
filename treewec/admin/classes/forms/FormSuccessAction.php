<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FormSuccessAction {

    const NOTHING = 'NOTHING';
    const PRINT_MESSAGE = 'PRINT_MESSAGE';
    const REDIRECT = 'REDIRECT';

    private function __construct() {}

    public static function process($actionType, $message, $redirectLink) {
        $actionType = \Treewec\Utils\Instances::isClassConstant('\Treewec\Admin\FormSuccessAction', $actionType) ? $actionType : self::NOTHING;
        switch ($actionType) {
            case self::NOTHING:
                return;
            case self::PRINT_MESSAGE;
                echo $message;
                break;
            case self::REDIRECT:
                self::redirect($message, $redirectLink);
                break;
        }
    }

    private static function redirect($message, $redirectLink) {
        $session = Session::getInstance();
        $session->setMessageSuccess($message);
        \Treewec\Utils\Functions::redirectToPage($redirectLink);
    }

}

?>