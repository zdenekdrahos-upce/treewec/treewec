<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FormTester {

    /** @var \Treewec\Admin\Third\FormProcessor */
    protected $formProcessor;

    public function __construct($formProcessor) {
        $this->formProcessor = $formProcessor;
    }

    public function testBoolean($value, $errorName = 'Value') {
        $this->formProcessor->check_condition(in_array($value, array('0', '1'), true), Message::get('invalid-value', array($errorName)));
    }

    public function testClassConstant($class, $constant, $errorName = 'Constant') {
        $constant = is_string($constant) ? strtoupper($constant) : '';
        $this->formProcessor->check_condition(
                \Treewec\Utils\Instances::isClassConstant($class, $constant), Message::get('invalid-value', array($errorName))
        );
    }

    public function testString($string, $range, $errorName = 'String') {
        $this->formProcessor->check_condition(is_string($string), Message::get('invalid-value', array($errorName)));
        if (is_string($string)) {
            $this->formProcessor->check_range(mb_strlen($string, 'UTF-8'), $range['min'], $range['max'], Message::get('string-length', array($errorName, $range['min'], $range['max'])));
        }
    }

    public function testNumber($number, $range, $errorName = 'Number') {
        $this->formProcessor->check_condition(is_numeric($number), Message::get('invalid-value', array($errorName)));
        if ($this->formProcessor->is_valid()) {
            $this->formProcessor->check_range($number, $range['min'], $range['max'], Message::get('number-range', array($errorName, $range['min'], $range['max'])));
        }
    }

    public function testFilename($filename, $errorName = 'Filename') {
        $this->testString($filename, array('min' => 1, 'max' => 200), $errorName);
        $this->formProcessor->check_condition(
                \Treewec\FileSystem\Filename::isValid($filename), Message::get('filename-invalid', array($errorName))
        );
    }

    public function testNewFilename($filename, $dirPath, $isDir, $errorName = 'Filename') {
        $this->testFilename($filename, $errorName);
        $filename = \Treewec\FileSystem\Path::toFileSystem($filename);
        $extension = $isDir ? '' : TREEWEC_DEFAULT_FILE_EXTENSION;
        $this->formProcessor->check_condition(
                !file_exists($dirPath . $filename . $extension), Message::get('filename-exists', array($errorName))
        );
    }

    public function testExistingDirectory($dirName, $dirPath, $errorName = 'Directory') {
        $this->testString($dirName, array('min' => 1, 'max' => 200), $errorName);
        $this->formProcessor->check_condition(
                \Treewec\FileSystem\Path::isValid($dirName), Message::get('directory-invalid', array($errorName))
        );
        $dirName = \Treewec\FileSystem\Path::toFileSystem($dirName);
        $this->formProcessor->check_condition(
                file_exists($dirPath . $dirName), Message::get('directory-unexisting', array($errorName))
        );
    }

}

?>