<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

abstract class SinglePageFormSubmission {

    /** @var string */
    private $submitName;
    /** @var \Treewec\Admin\FormSuccessAction */
    private $successAction;
    /** @var string */
    protected $successLink;
    /** @var \Treewec\Admin\FormTester */
    protected $formTester;
    /** @var \Treewec\Admin\Third\FormProcessor */
    protected $formProcessor;
    /** @var Treewec\Admin\Templater */
    protected $templater;

    public function __construct($submitName, $escapeValues) {
        $this->submitName = $submitName;
        $this->successAction = FormSuccessAction::REDIRECT;
        $this->successLink = '';
        $this->templater = new Templater();
        $this->formProcessor = new Third\FormProcessor(false);
        if ($escapeValues) {
            $this->formProcessor->escape_values();
        }
        $this->formTester = new FormTester($this->formProcessor);
    }

    public function isValid() {
        return $this->isSend() && $this->formProcessor->is_valid();
    }

    public function getFormErrors() {
        return $this->formProcessor->get_errors();
    }

    public function setSuccessAction($action) {
        if (\Treewec\Utils\Instances::isClassConstant('\Treewec\Admin\FormSuccessAction', $action)) {
            $this->successAction = $action;
        }
    }

    public function process() {
        if ($this->isSend()) {
            $this->checkFormInput();
            if ($this->formProcessor->is_valid()) {
                $this->checkFormFields();
                if ($this->formProcessor->is_valid()) {
                    if ($this->successAction == FormSuccessAction::REDIRECT) {
                        $this->loadSuccessLink();
                    }
                    if ($this->performAction()) {                        
                        $this->processSuccess();
                    } else if ($this->formProcessor->is_valid()) {
                        $this->formProcessor->add_error($this->getActionError());
                    }
                }
            }
        } else {
            $this->initFormFields();
        }
    }

    private function isSend() {
        return isset($_POST[$this->submitName]);
    }

    protected function checkFormInput() {        
        $this->checkChange();        
        $this->checkArrayStructure();
    }

    protected abstract function checkFormFields();

    /** @return boolean  */
    protected abstract function performAction();

    protected function loadSuccessLink() {
        $this->successLink = $_SERVER['REQUEST_URI'];
    }

    protected function processSuccess() {
        FormSuccessAction::process(
                $this->successAction,
                $this->getSuccessMessage(),
                $this->successLink
        );
    }

    protected function getSuccessMessage() {        
        return Message::get('general-success');
    }

    protected function getActionError() {
        return Message::get('save-error');
    }

    protected function checkChange() {
        foreach ($this->getStructure() as $arrayKey => $defaultValue) {
            if ($_POST[$arrayKey] != $defaultValue) {
                return;
            }
        }
        $this->formProcessor->add_error(Message::get('no-change'));
    }

    private function checkArrayStructure() {
        $this->formProcessor->check_condition(
                AdvancedFunctions::hasArrayValidStructure($_POST, array_keys($this->getStructure())), Message::get('invalid-input')
        );
    }

    private function initFormFields() {
        foreach ($this->getStructure() as $key => $defaultValue) {
            $_POST[$key] = isset($_POST[$key]) ? $_POST[$key] : $defaultValue;
        }
    }

    /** @return array  */
    protected abstract function getStructure();
}

?>