<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace Treewec\Admin;

final class FormHTMLElements {

    public static function input($name, $type = 'text') {
        echo '<input type="' . $type . '" name="' . $name . '" id="f' . $name . '" value="' . $_POST[$name] . '" />';
    }
    
    public static function select($name, $items) {
        echo '<select name="' . $name . '" id="f' . $name . '" size="1">';
        foreach ($items as $key => $value) {
            echo '<option ' . (($key == $_POST[$name]) ? 'selected="true" ' : '') . 'value="' . $key . '">' . $value;
        }
        echo '</select>';
    }
    
    public static function selectBoolean($name) {
        $items = array(true => 'ALLOWED', false => 'DENIED');
        foreach ($items as $key => $value) {
            $items[$key] = \Translator::get('constants', 'boolean', $value);
        }
        self::select($name, $items);
    }
    
    public static function selectNumbers($name, $min, $max) {
        $items = array();
        for ($i = $min; $i <= $max; $i++) {
            $items[$i] = $i;
        }
        self::select($name, $items);
    }

}

?>