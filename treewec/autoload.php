<?php
/*
 * This file is part of the Treewec (https://bitbucket.org/zdenekdrahos/treewec)
 * Copyright (c) 2012 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Treewec is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

function treewecAutoloadDie($className) {
    die("The file {$className}.php could not be found.");
}

function treewecAutoload($className) {
    $namespaceArray = explode('\\', $className);
    $className = array_pop($namespaceArray);
    $directories = array(
        TREEWEC_CLASS_ROOT => array(
            'exceptions', 'file-system', 'file-system/path', 'file-system/structures',
            'holders', 'html', 'mvc', 'output', 'output', 'printers', 'url', 'utils', '3rd',
            'directory-iterators', 'directory-iterators/filters', 'directory-iterators/abort',             
            'directory-iterators/order', 'directory-iterators/order/comparators',
            'directory-iterators/helpers', 'data-structures'
        ),
        TREEWEC_ADMIN_ROOT . 'classes/' => array(
            'mvc', 'directory-iterators', 'helpers', '3rd', 'editors', 'forms',
            '3rd/language-switch', 'authentication'
        ),
        TREEWEC_ADMIN_ROOT . 'modules/*/classes' => array(
            'audit', 'help', 'home', 'pages', 'plugins', 'settings', 'themes', 'file',
            'authentication'
        ),
    );
    foreach ($directories as $root => $directories) {
        foreach ($directories as $directory) {            
            if (is_int(strpos($root, '*'))) {
                $directory = str_replace('*', $directory, $root);                
                $path = "{$directory}/{$className}.php";
            } else {
                $path = "{$root}/{$directory}/{$className}.php";
            }
            if (file_exists($path)) {
                require_once($path);
                return;
            }
        }
    }    
}

// Autoload Treewec classes
spl_autoload_register('treewecAutoload');
// Autoload user classes
if (file_exists(TREEWEC_USER_FILES . 'classes/autoload.php')) {
    require_once(TREEWEC_USER_FILES . 'classes/autoload.php');
    if (function_exists('treewecAutoloadUser')) {
        spl_autoload_register('treewecAutoloadUser');
    }
}
// Autoload - die if class is not found
spl_autoload_register('treewecAutoloadDie');
?>