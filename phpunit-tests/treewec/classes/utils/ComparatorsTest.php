<?php

namespace Treewec\Utils;

/**
 * Test class for Comparators.
 * Generated by PHPUnit on 2012-07-03 at 14:13:30.
 * @group utils
 */
class ComparatorsTest extends \PHPUnit_Framework_TestCase {

    public function testNegate() {
        parent::assertEquals(Comparators::negate(0), 0);
        parent::assertEquals(Comparators::negate(1), -1);
        parent::assertEquals(Comparators::negate(-1), 1);
        // if invalid argument -> 1
        parent::assertEquals(Comparators::negate(new \Exception()), 1);
    }

    public function testcompareNumberOfSlashesInString() {
        parent::assertEquals(Comparators::compareNumberOfSlashesInString('/', '/'), 0);
        parent::assertEquals(Comparators::compareNumberOfSlashesInString('/', '//'), 1);
        parent::assertEquals(Comparators::compareNumberOfSlashesInString('//', '/'), -1);
    }

    public function testCompareNumbers() {
        parent::assertEquals(Comparators::compareNumbers(10, 10), 0);
        parent::assertEquals(Comparators::compareNumbers('10', '20'), -1);
        parent::assertEquals(Comparators::compareNumbers(10, 0), 1);
        parent::assertEquals(Comparators::compareNumbers(-10, -10.5), 1);
        // if invalid argument -> 1
        parent::assertEquals(Comparators::compareNumbers(new \Exception(), array()), 1);
    }

    public function testCompareStrings() {
        // only strings
        parent::assertEquals(Comparators::compareStrings('h', 'h'), 0);
        parent::assertEquals(Comparators::compareStrings('h', 'w'), -1);
        parent::assertEquals(Comparators::compareStrings('w', 'h'), 1);
        // string and number
        parent::assertEquals(Comparators::compareStrings('w', '55'), -1);
        parent::assertEquals(Comparators::compareStrings(55, 'w'), 1);
        // if invalid argument -> 1
        parent::assertEquals(Comparators::compareStrings(new \Exception(), array()), 1);
    }

    public function testCompareUTF8Strings() {
        parent::assertEquals(Comparators::compareStrings('č', 'č'), 0);
        parent::assertEquals(Comparators::compareStrings('č', 'ž'), -1);
        parent::assertEquals(Comparators::compareStrings('ž', 'č'), 1);
    }

}

?>
