<?php

namespace Treewec\FileSystem;

/**
 * Test class for File.
 * Generated by PHPUnit on 2012-03-25 at 16:41:27.
 * @group filesystem
 */
class FileTest extends \PHPUnit_Framework_TestCase {

    private $exceptionHelper;

    protected function setUp() {
        $this->exceptionHelper = new \Treewec\ExceptionHelper();
    }

    public function test__construct() {
        $this->assertInstance(__FILE__);
        $this->assertException(__DIR__);
        $this->assertException(array());
    }

    public function testGetPath() {
        $path = __FILE__;
        $file = new File($path);
        parent::assertEquals($path, $file->getPath());
    }

    private function assertInstance($path) {
        parent::assertInstanceOf('\Treewec\FileSystem\File', new File($path));
    }

    private function assertException($parameter) {
        $this->exceptionHelper->catchExceptionInConstructor(
                '\Treewec\Exceptions\FileSystemException', '\Treewec\FileSystem\File', $parameter
        );
    }

}

?>