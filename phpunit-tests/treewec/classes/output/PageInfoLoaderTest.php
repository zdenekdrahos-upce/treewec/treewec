<?php

namespace Treewec;

/**
 * Test class for PageInfoLoader.
 * Generated by PHPUnit on 2012-03-25 at 16:37:17.
 * @group output
 */
class PageInfoLoaderTest extends \PHPUnit_Framework_TestCase {

    /** @var ExceptionHelper */
    private $exceptionHelper;

    protected function setUp() {        
        $this->exceptionHelper = new ExceptionHelper();
    }

    public function testGetPageInfo() {
        $this->assertValidInstance('getPageInfo', '');
        $this->assertValidInstance('getPageInfo', 'teams');
        $this->assertValidInstance('getPageInfo', 'teams/makov');

        $this->assertException('getPageInfo', array());
    }
    
    public function testGetErrorPageInfo() {
        $this->assertValidInstance('getErrorPageInfo', '');
        $this->assertValidInstance('getErrorPageInfo', 'teams');
        $this->assertValidInstance('getErrorPageInfo', 'teams/makov');
                
        $valid = PageInfoLoader::getErrorPageInfo(new Holders\ArrayHolder(array('path' => 'teams')));
        parent::assertTrue($valid->header == TREEWEC_DEFAULT_ERROR);
        
        $this->assertException('getErrorPageInfo', array());
    }
    
    private function assertValidInstance($method, $pathArgument) {
        $arrayHolder = new Holders\ArrayHolder(array('path' => $pathArgument));
        parent::assertInstanceOf('\Treewec\PageInfo', PageInfoLoader::$method($arrayHolder));
    }
    
    private function assertException($method, $argument) {
        $this->exceptionHelper->catchExceptionInCallback(
                '\Treewec\Exceptions\InstanceException', 
                array('\Treewec\PageInfoLoader', $method), 
                $argument
        );
    }

}

?>
