<?php

namespace Treewec\DirectoryIterators;

/**
 * Test class for IteratorFilters.
 * Generated by PHPUnit on 2012-07-04 at 14:02:38.
 * @group directory_iterators
 */
class IteratorFiltersTest extends \PHPUnit_Framework_TestCase {

    /** @var IteratorFilters */
    private $filter;

    protected function setUp() {
        $this->filter = new IteratorFilters();
    }

    public function testAddFilter() {
        // filter must implements \Treewec\DirectoryIterators\IFilter
        $exceptionHelper = new \Treewec\ExceptionHelper();
        $exceptionHelper->catchExceptionInCallback('\Treewec\Exceptions\InstanceException', array($this->filter, 'addFilter'), null);
    }

    public function testCanBeProcessed() {
        $invalidElement = new IteratorElement(1, new \SplFileInfo(TREEWEC_VIEW_DIR . 'nav ? igation'));        
        // no filter is set -> everything is OK
        parent::assertTrue($this->filter->canBeProcessed($invalidElement));
        $this->filter->addFilter(new FilterFilename());
        parent::assertFalse($this->filter->canBeProcessed($invalidElement));
    }

}

?>
