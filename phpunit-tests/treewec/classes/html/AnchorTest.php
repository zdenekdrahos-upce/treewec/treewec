<?php

namespace Treewec\HTML;

/**
 * Test class for Anchor.
 * Generated by PHPUnit on 2012-03-25 at 16:39:02.
 * @group html
 */
class AnchorTest extends \PHPUnit_Framework_TestCase {

    public function testCreate() {
        $anchor = Anchor::create('index.php', 'INDEX');
        parent::assertTrue(is_string($anchor));
        parent::assertEquals('<a href="index.php">INDEX</a>', $anchor);
        
        parent::assertEquals('', Anchor::create(array(), 'INDEX'));
        
        $attributes = array(
            AttributeFactory::getAttributeClass('selected'),
            AttributeFactory::getAttributeTitle('Go to homepage'),
        );
        parent::assertEquals('<a href="index.php" class="selected" title="Go to homepage">INDEX</a>', Anchor::create('index.php', 'INDEX', $attributes));
    }

}

?>
