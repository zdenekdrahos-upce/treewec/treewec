<?php
namespace Treewec;

class Output {
    
    public static function getPrintedText($object, $function) {
        ob_start();
        $object->$function();
        $text = ob_get_contents();
        ob_end_clean();
        return $text;
    }
    
}

?>
