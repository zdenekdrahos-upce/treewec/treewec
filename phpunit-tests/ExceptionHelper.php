<?php

namespace Treewec;

class ExceptionHelper extends \PHPUnit_Framework_TestCase {

    private $caughtException;

    public function __construct() {
        $this->caughtException = null;
    }

    /** @return \Exception */
    public function getCaughtException() {
        if (is_null($this->caughtException)) {
            $this->failTest('exception was not caught');
        }
        return $this->caughtException;
    }
    
    public function catchExceptionInCallback($excClass, $callback, $callbackParameter = false) {
        try {
            call_user_func($callback, $callbackParameter);
            $this->failTest('exception was not thrown');
        } catch (\Exception $e) {
            $this->caughtException = $e;
            parent::assertInstanceOf($excClass, $e);
        }
    }

    public function catchExceptionInConstructor($excClass, $class, $constructorParameter) {
        try {
            new $class($constructorParameter);
            $this->failTest('exception was not thrown');
        } catch (\Exception $e) {
            $this->caughtException = $e;
            parent::assertInstanceOf($excClass, $e);
        }
    }
    
    private function failTest($message) {
        parent::fail('This test did not fail as expected - ' . $message);
    }
    
}

?>
